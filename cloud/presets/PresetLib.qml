// presetLib переменная в VlWindow.qml 
Item {
  property var byType: { return {} }
  property var byAbn: { return {} }

  function find_presets_for( obj ) {
    var t = wp.get_type( obj );

    if (t == "Deed") {
      var abn = obj.ability ? obj.ability.name : null;
      if (abn) return (byAbn[ abn ] || []).filter( function(p) { return p.feel(obj); } );
    }
    else
    {
      return (byType[ t ] || []).filter( function(p) { return p.feel(obj); } );;
    }
  }

  function add( preset ) {
    if (preset.targetAbility) {
      if (!byAbn[ preset.targetAbility ]) byAbn[ preset.targetAbility ] = [];
      byAbn[ preset.targetAbility ].push( preset );
      addtitle( preset,byAbn[ preset.targetAbility] );
    }
    else
    if (preset.targetType) {
      if (!byType[ preset.targetType ]) byType[ preset.targetType ] = [];
      byType[ preset.targetType ].push( preset );
      addtitle( preset,byType[ preset.targetType ]);
    }    
  }

  function addtitle( preset, collection ) {
    //if (!preset.title) preset.title = "№"+collection.length;
    //if (!preset.title) preset.title = preset.name;
  }

  function remove( preset ) {
    if (preset.targetAbility) {
      var i = (byAbn[ preset.targetAbility ] || []).indexOf( preset );
      if (i >= 0) byAbn[ preset.targetAbility ].splice( i,1 );
    }
    else
    if (preset.targetType) {
      var i = (byType[ preset.targetType ] || []).indexOf( preset );
      if (i >= 0) byType[ preset.targetType ].splice( i,1 );
    }
  }

}