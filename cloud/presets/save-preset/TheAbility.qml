Ability {
  title: "Сохранить шаблон"
  category: "system-deeds"
  help: "Сохраняет параметры объекта как шаблон для будущего"

  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( obj ) {
    if (obj && obj.paramsForSave && obj.paramsForSave.length > 0) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}