Deed {
    id: deed
    icon: "[сш]"

    record: false
    onRecordChanged: if (record) record=false;

    Text {
      id: info
      text: "Запишите код в файл с расширением .preset.qml\nПодключите его затем через меню 'Подключить...'"
    }

    TextEdit {
      id: results
      width: 500
      height: 200
      text: generate( deed.input );
    }

    params: [info, results]

    function generate(obj) {
      var res = "Preset {\n  title: \"\"\n  help: \"\"\n  helpurl: \"\"\n";

      var t = wp.get_type( obj );
      if (t == "Deed") 
      {
        res += "  targetAbility: \"" + obj.ability.name + "\"\n";
      }
      else
      {
        res += "  targetType: \"" + t + "\"\n";
      }

      var ip = {};
      for (var i=0; i<obj.paramsForSave.length; i++) {
        var p = obj.paramsForSave[i];
        var vv = wp.readParamValue( p );
        if (vv !== undefined)
          ip[p.guid] = vv;
      }

      if (obj.templateOnSave) res = obj.templateOnSave( res, ip );

      if (ip.code) {
        var m = ip.code.match(/function ([a-z_A-Z0-9]+)/);
        if (m[1]) {
          //res += "\n  " + ip.code.replace(/\n[^ ]+/g,"\n  ") + "\n\n";
          res += "\n  " + ip.code + "\n\n";
          ip.code = "<<" + m[1] + ".toString()>>" ;
        }
      }

      res += "  initParams: [" + JSON.stringify( ip, null,"    " ).replace("\n}","\n  }") + "]\n";

      res = res.replace( "\"<<","" ).replace( ">>\"","" );

      res += "}\n";
      
      return res;
  }
}
