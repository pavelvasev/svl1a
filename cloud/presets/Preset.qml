QtObject {
  id: preset

  property var name: "" // будем брать из файла, load-ability-qml 
  property var title: name
  property var help: ""
  property var helpurl: ""
  property var initParams: [{}]

  property var targetType: ""
  property var targetAbility: ""

  property var type: "Preset"

  function extraFeel(obj) { return true; }

  // ха.. т.е. пресеты это действия, по своему существу... хехе ;-)
  // исп find_presets_for -- больше не используется вроде как, ибо индекс построен в PresetLib.qml 
  function feel(obj) 
  {
    /*if (targetAbility)  return (obj.ability && obj.ability.name == targetAbility);
    var t = wp.get_type(obj);
    return (t == targetType);
    */
    return extraFeel(obj);
  }

  function act(obj) {
    var ip = Array.isArray( initParams ) ? initParams[0] : initParams;
    if (!ip) return;

    for (var k in ip) {
      wp.writeParam( obj, k, ip[k] );
    }
  }

  Component.onCompleted: {
    presetLib.add( preset );
  }

  Component.onDestruction: {
    presetLib.remove( preset );
  }

  function setNameFromFilename(source) {
      if (source.indexOf(".preset.qml") > 0) {
        var c = source.split("/");
        var fn = c[ c.length-1 ];
        name = c[ c.length-2 ] + "/" + fn.split(".")[0];
      }
  }
}