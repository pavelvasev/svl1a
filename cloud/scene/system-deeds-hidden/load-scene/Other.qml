        Row {
            anchors.right: parent.right
            spacing: 5
            Text {
                text: "Другие варианты"
            }

            PerformDeedButton {
                text: "Текст"
                name: "load-scene-text"
                input: qmlEngine.rootObject
                record: true
                tag: null
                parentToObject: true
                onPerformedGood: {
                    risovanie.activate( outputDeed );
                }
            }
        }