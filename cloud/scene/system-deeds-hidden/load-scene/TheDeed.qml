// todo если попользоваться текстом, то остальные отрубаются. это изза путаницы в chainloader с sourceObj

Deed {
    id: deed
    icon: "L"

    Column {
        id: coco

        TabView {
            width: 500
            height: 250

            Tab {
                title: "Облако"

                Column {
                    spacing: 5
                    Text {
                        text: " "
                    }
                    Text {
                        text: "Откройте облако, найдите сцену, и нажмите view 3d"
                    }
                    Text {
                        text: "<a href='http://www.svn.lact.ru:4567/files/svl_scenes/' target='_blank'>Облако сцен www.svn.lact.ru:4567/files/svl_scenes/</a>\n\n"
                    }
                }
            }

            Tab {
                title: "URL-адрес"

                Column {
                    spacing: 5
                    Text {
                        text: "Укажите URL сцены:"
                    }

                    TextInput {
                        id: ti
                        width: 400
                        Component.onCompleted: ti.dom.firstChild.name="qurla";
                    }

                    Button {
                        text: "Загрузить"
                        onClicked: go();
                    }
                }
            }

            Tab {
                title: "Текст"

                Column {
                    spacing: 5

                    Text {
                        text: "Возьмите содержание сцены из надежного места в формате SVL-JSON и вставьте в окно. Затем нажмите кнопку Загрузить."
                        width: 500
                        wrapMode: Text.WordWrap
                    }

                    TextEdit {
                        id: scenecode
                        width: 500
                        height: 180
                    }

                    Button {
                        text: "Загрузить!"
                        onClicked: loadfromtext();
                    }
                }
            }

            Tab {
                title: "Файл"

                Column {
                    spacing: 15

                    FileSelect {
                        id: infile
                    }

                    Button {
                        text: "Загрузить!"
                        onClicked: loadfromfile();
                    }
                }
            }

        }


        Text {
            text: " "
        }

        Text {
            id: status
        }
    }

    params: [coco]

    ChainLoader {
        id: cl
        output.historyMode: true
        output.category: "hidden"
        onReadyChanged: {
            if (cl.ready) {
                status.text = "Выполняю..";
                var out = cl.output.act();
                if (out) out.finished.connect( deed,fin);
            }
        }
        onLoadFailed: status.text = "Ошибка чтения-загрузки, см лог";
    }

    function fin() {
        status.text = "Готово";
        deed.destroy();
    }

    function go() {
        if (ti.text) {
            status.text = "Читаю файл сцены..";
            cl.file = ti.text;
        }
    }

    function loadfromfile() {
        if (infile.file) {
            status.text = "Читаю файл сцены..";
            cl.file = infile.file;
        }
    }

    function loadfromtext() {
        try {
            cl.sourceText = scenecode.text;
            status.text = "Результат: текст распарсен хорошо, загружаю.."
        } catch(err) {
            console.error( err );
            status.text = "Результат: ошибка разбора текста из окошка. См лог.";
            return;
        }

        window.svlProtectFromExit = false;
    }
}
