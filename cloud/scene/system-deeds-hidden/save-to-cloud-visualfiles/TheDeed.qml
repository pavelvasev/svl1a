Deed {
  id: deed

  property var content: ""

  Component.onCompleted: {
    export2a();
  }

        function export2a()
        {
            console.log("so export 2a...");
            var img = renderer.domElement.toDataURL("image/png");

            var output = content;

            setFileProgress( "3d export","sending to server",15);
            // как упаковываем -- http://stackoverflow.com/questions/5392344/sending-multipart-formdata-with-jquery-ajax
            var data = new FormData();
            data.append('imgbase64', img );
            data.append('svljson', output );


            var xhr = new XMLHttpRequest();
            xhr.open('POST', "http://www.svn.lact.ru:4567/update/svl_scenes?get_file_url=1");
            
            xhr.onload = function(e) {
                var res = this.responseText;
                // перейдем в каталог
                var p = res.split("/");
                p.length = p.length-1;
                res = p.join("/");
                window.open( res.indexOf("://") >= 0 ? res : "http://www.svn.lact.ru:4567" + res, '_blank');
                setFileProgress( "3d export","done",-1);
            };

            xhr.onerror = function(e) {
                setFileProgress( "3d export","server error",-1);
            }

            xhr.upload.addEventListener("progress", function(e) {
                if (e.lengthComputable) {
                    setFileProgress( "3d export","sending to server",e.loaded / e.total * 100);
                }
            }, false);

            xhr.send(data);
        }

}