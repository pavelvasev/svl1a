TextButton {
  text: "Новая сцена"
  property var tag: "top"

  onClicked: {
    window.location.hash = "";
    if (window.location.search.indexOf("used_scene") >= 0)
      window.location.search = window.location.search.replace(/used_scene/,"apply_scene");
    else
      window.location.reload();
  }
}