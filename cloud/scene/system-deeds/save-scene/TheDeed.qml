import ".."

Deed {
    id: deed
    icon: "[S]"

    property var robot: deed.input

    record: false
    onRecordChanged: if (record) record=false;

    SimpleDialog {
        title: "Сохраняем сцену"
        visible: true
        width: col.width + 50
        height: col.height + 50

        onAfterClose: deed.destroy();

        Column {
            spacing: 10
            id: col

            Text {
                text: "Сохраните текст сцены в надежное место, например в файл scena1.svl.json"
            }

            Row {
                spacing: 5

                Column {
                    spacing: 5

                    Button {
                        text: "Сохранить в URL"
                        onClicked: saveurla();
                    }

                    /*
                    Button {
                        text: "Сохранить в файл"
                    }*/

                    /*
                    PerformDeedButton {
                        width: 90
                        tag: null
                        text: "Сохранить в облако node"
                        name: "save-to-cloud"
                        options: { return { content: scenecode.text } }
                    }
                    */

                    PerformDeedButton {
                        width: 90
                        tag: null
                        text: "Сохранить в облако"
                        name: "save-to-cloud-visualfiles"
                        options: { return { content: scenecode.text } }
                    }
                }

                TextEdit {
                    id: scenecode
                    width: 500
                    height: 200
                    //text: rec.output

                    Button {
                        text: "Генерировать"
                        onClicked: rec.perform()
                        anchors.right: parent.right-10
                        anchors.top: parent.top+5
                    }
                }
            }

            Text {
                id: savenote
                width: 600
                height: 30
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignLeft
                text: "Сохранить в URL - сохраняет сцену в адрес браузера.\nСохранить в облако - сохраняет на сервер visualfiles."
            }
        }
    }

    property var fileext: ".json.svl"

    ChainSaver {
        input: qmlEngine.rootObject
        id: rec
        onOutputChanged: scenecode.text = output
    }

    ChainLoader {
        id: ldr1        
    }

    ////////////////////////////// url

    property var ho: []
    ParamUrlHashing {
        property: "ho"
        propertyWrite: null
        name: "scene-program"
        manual: true
        timeout: 0
        id: hashing
    }

    function saveurla() {
        try {
            ldr1.sourceText = scenecode.text;
            ho = ldr1.sourceObj;
            //ho = JSON.parse( scenecode.text );
            hashing.params_update_hash();            

            if (window.location.search.indexOf("apply_scene") >= 0) {
                //window.svlProtectFromExit = true;
                //window.location.search = window.location.search.replace(/apply_scene/,"used_scene");
                var newhref = window.location.href.replace(/apply_scene/,"used_scene");
                history.pushState({}, null, newhref);                
            }

            var link = "<a href='"+window.location.href+"' target='_blank'>[Ссылка]</a>";
            savenote.text = "Результат: URL страницы браузера содержит полное описание сцены. Страницу можно перезагрузить, URL можно открыть позже, или передать другу. "+link;
            
            window.svlProtectFromExit = false;
        } catch(err) {
            console.error( err );
            savenote.text = "Результат: ошибка разбора текста из окошка. См лог.";
        }
    }

}
