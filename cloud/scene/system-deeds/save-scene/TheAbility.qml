Ability {
  title: "Сохранить сцену"

  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    if (wp.get_type( target_object_x) == "Scene") return 1;
    if (!target_object_x) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}