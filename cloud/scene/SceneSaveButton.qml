import ".."
import "../../core"

PerformDeedButton {
  name: "save-scene"
  tag: "top"
  text: "Сохранить сцену"
  id: bt

  // property bool protectFromExit: false

  //////////////////////////////////////////////////////////////
  Component.onCompleted: {
    wp.deedPerformed.connect( wp,function(deed) { 

      if (deed.record) window.svlProtectFromExit = true; 
      
    });
    window.onbeforeunload = function(event) {

      if (window.svlProtectFromExit) {
        //bt.perform();
        //jQuery(bt.dom).fadeIn(500).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);        
        event.returnValue = "Сцена не сохранена. Уверены?"
        return event.returnValue;
        //return 'Dialog text here.';
      }
    };
  }
}