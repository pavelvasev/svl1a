Item {
  ParamUrlHashing {
    property: null
    propertyWrite: "historyFromUrla"
    name: "scene-program"
    id: hashing
  }

  property var historyFromUrla: [] 
  // onHistoryFromUrlaChanged: console.log("historyFromUrla changed,",historyFromUrla);

  ChainLoader {
    id: cl
    sourceObj: historyFromUrla
    output.name: "replay-history-from-hash"
    output.historyMode: true
    output.category: "hidden"
    output.robotLayer: "system"

    onReadyChanged: {
      if (cl.ready) {
        // console.log("SceneLoadFromHash.qml: chain loader ability ready, acting.");
        // todo тут можно вызвать setprogress
        setTimeout( function() { cl.output.act(); }, 500 );
      }
    }
  }
}

