import ".."

Deed {
    id: deed
    icon: "[s]"

    property var robot: deed.input

    record: false
    onRecordChanged: if (record) record=false;

    Text {
        text: "Новое имя действия:"
        id: t1
    }

    TextInput {
        id: abname
        text: robot.ability ? (robot.ability.name + "-patched") : "history"
        width: 200
    }

    TabView {
        id: tv
        width: 500
        height: contentCol.height+20
        Tab {
            title: "Содержание"
            Column {
                id: contentCol
                spacing: 10

                TextEdit {
                    id: abparams
                    width: 500
                    height: 200
                    text: rec.output
                }

                Button {
                    id: btrefresh
                    text: "Обновить"
                    onClicked: {
                        rec.perform();
                    }
                }

                Text {
                    text: "1. Сохраните текст в файл <b>" + abname.text + fileext + "</b>"
                }
                Text {
                    text: "2. Загрузите в эту или другую сцену с помощью кнопки 'Подключить...'"
                }
                Text {
                    text: "3. Появится новое действие с заданным именем."
                }
            }
        }
        Tab {
            title: "Уточнить список"

            css.maxHeight: "500px"
            css.overflowY: "scroll"
            css.overflowX: "hidden"
            css.pointerEvents: "all"

            Column {
                spacing: 10
                Text {
                    text: "Возможность уточнить, какие действия сохранять."
                }

                CheckBox {
                    id: cba
                    text: "Будем уточнять список"
                    width: 200
                }

                Row {
                    visible: cba.checked
                    spacing: 5
                    Button {
                        text: "Выбрать стандарт"
                        width: 150
                        onClicked: {
                            ch1.tablica = {}
                            ch1.in1 = []
                            refreshCh1();
                        }
                    }
                    Button {
                        text: "Выбрать все"
                        width: 150
                        onClicked: {
                            ch1.tablica = {}
                            ch1.in1 = []
                            refreshCh1( function() { return true; } );
                        }
                    }
                }
                Choser {
                    id: ch1
                    visible: cba.checked
                    onVisibleChanged: refreshCh1();
                    onChosenChanged: rec.perform();
                }
            }
        }
    }

    function refreshCh1( deftru ) {
        ch1.in1 = gatherFilterList( deftru );
    }

    Button {
        text: "Закончить сохранение и закрыть окно"
        onClicked: deed.destroy();
        width: 300
        id: bt
    }

    params: [t1,abname,tv,bt]
    property var fileext: ".ab.json"

    ChainSaver {
        input: robot
        id: rec
        onOutputChanged: abparams.text = output;

        // аспект "Выставить имя для цепочки, если родительское действие есть воспроизведение цепочки"
        defaultTitle: {
            // двойной parent нужен, тк.. у действия реальный parent это performdeed. корявка.
            if (robot.parent && robot.parent.parent && robot.parent.parent.thisIsChainDeed) {
                return robot.parent.parent.ability.title;
            }
            return robot.title + "название для человека"
        }

        function test( item, root, recorder ) {
            var cc = ch1.chosen;
            // если не выбрано уточнение - уходим на стандартные тесты
            if (!cba.checked) return rec.deftest( item, root, recorder );
            // если выбрано уточнение - проверяем, есть ли в галочках
            var res = cc.find( function(k) { return k.item === item });
            //console.log("item = ",item,"res = ",res);
            return !!res;
        }
    }

    //////////////////////////////////
    function gatherFilterList( deftru ) {
        var res = [];
        wp.qml_tree_walk( robot, function(item) {
            if (item.type !== "Deed") return true;
            if (item === deed) return true; // самое себя сохранять не будем

            var t = wp.get_object_user_name(item);
            var dt = rec.deftest( item, robot, rec );
            var ic = deftru ? deftru( item,robot,rec ) : dt;

            // if (!dt && item.ability.category.indexOf("system-deeds") >= 0) return true;
            if (!dt && item.ability.category.indexOf("hidden") >= 0) return true;

            res.push( { text: t, item: item, key: t, initialCheck: ic } )
            return true;
        } );
        return res;
    }

    property var deedItems: [] //gatherFilterList()

}
