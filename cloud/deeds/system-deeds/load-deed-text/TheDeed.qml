Deed {
    id: deed
    icon: "ld-text"

    params: [col]

    Column {
        spacing: 10
        id: col
        property var guid: "deed-code"
        property var value: code.text

        Text {
          text: "Вставьте json-код действия:"
        }

        TextEdit {
            id: code
            width: 500
            height: 200
        }

        Button {
            text: "Загрузить!"
            onClicked: loadfromtext();
        }

        Text {
            id: opnote
            width: 400
            height: 30
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignLeft
        }
     }
    
    ChainLoader {
        id: clText
        output.title: ""
    }

    function hashCode (s) {
      return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);              
    }

    function loadfromtext() {
        try {
            opnote.text = "Результат: текст распарсен хорошо."
            clText.sourceText = code.text;
            clText.output.name = ho.name || "ability-from-text-"+hashCode( code.text ); // ho.title ||
            opnote.text = "Результат: действие загружено. Имя действия " + (clText.output.title)  + (clText.output.name == clText.output.title ? "" : " ("+clText.output.name+")")
        } catch(err) {
            console.error( err );
            opnote.text = "Результат: ошибка разбора текста из окошка. См лог.";
        }
    }

    Component.onCompleted: if (code.text && code.text.length > 0) loadfromtext()
}
