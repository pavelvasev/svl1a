PerformDeedButton {
  name: "load-deed-archive"
  tag: "top"
  text: "Подключить действия"
  width: 180
  id: bt
  record: true
  input: qmlEngine.rootObject
  once: true

  onPerformedGood: {
    risovanie.activate( outputDeed );
  }

}