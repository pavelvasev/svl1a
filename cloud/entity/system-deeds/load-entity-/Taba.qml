Tab {
    width: 500
    height: examples.height

    ExclusiveGroup {
        id: ex
        onCurrentChanged: tp.value = ex.current.rec.url;
    }

    JsonLoader {
      id: ldr
      output: []
    }
    property alias file: ldr.file

    Column {
        id: examples
        Repeater {
            model: ldr.output.length
            RadioButton {
                width: 400
                text: rec.guid + " - " + rec.info 
                exclusiveGroup: ex
                property var rec: ldr.output[index] 
            }
        }
    }

}
