Deed {
    id: deed

    //
    property var names: ["starter-kit","f(x,y)"]
    property var urltable: { return {
    "starter-kit" : "https://github.com/pavelvasev/svl-starter-kit/blob/master/init.txt",
    "f(x,y)" : "https://github.com/pavelvasev/svl-function2vars/blob/master/init.txt"
    }
    }
    ////////////////////////    
    // "f(x,y)" : "http://localhost:8080/packages/function2vars/init.qml"

    params: [examples,t1,tp,btn]

    Text {
        id: t1
        text: "Укажите URL для загрузки. Допустимы: * действия ab.json * Qml-файл * сцена svl.json * сцена .vl"
    }

    TextInput {
        id: tp
        width: 450
        property alias value: tp.text
        //property var guid: "url" вроде как это действие теперь генератор, а значит не надо сохранять параметра
    }

    Button {
      text: "Подключить!"
      onClicked: updateUrlas(); 
      id: btn
    }

    function updateUrlas() { // т.е. вот сюда можно и таблицы переименований вставить будет
      urla = formatSrc( tp.text ); 
      la.perform();
    }

    property var urla: ""

    PerformDeed {
        id: la
        name: "load-ability"
        input: urla
        manual: true
        record: true
        options: [{risClosed:true}]
        // options: [{robotLayer:"system"}]
        // следующее сделано чтобы они не сразу кидались загружаться
    }

    TabView {
      width: 500
      height: 200
      id: examples

      Taba {
        title: "Библиотеки"
        file: Qt.resolvedUrl( "libs.json" )
      }
    }

    /*
    Column {
      id: examples
      Text {
        text: "Библиотеки:"
      }
      ExclusiveGroup {
        id: ex
        onCurrentChanged: tp.value = urltable[ex.current.text]; 
      }
      Repeater {
        model: names.length
        RadioButton {
          text: names[index]
          exclusiveGroup: ex
        }
      }
    }*/
}
