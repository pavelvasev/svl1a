Deed {
    id: deed
    //property bool allNewLines: true

    params: [cats,examples,t1,tp,btn,t2]

    Text {
        id: t1
        text: "Укажите URL для загрузки."
        wrapMode: Text.WordWrap
        width: 500
    }

    TextInput {
        id: tp
        width: 450
        property alias value: tp.text
        //property var guid: "url" вроде как это действие теперь генератор, а значит не надо сохранять параметра
        Component.onCompleted: tp.dom.firstChild.name = "entity-url"; // надо чтобы работала память ввода в браузере
    }

    Button {
        text: "Подключить!"
        onClicked: updateUrlas();
        id: btn
    }

    Text {
      id: t2
      text: "Допустимы:\n* подпрограмма ab.json\n* Qml-файлы\n* сцены svl.json\n* сцены .vl\n* сборник файлов init.txt\n* обычные js или css\n* шаблоны .preset.qml"
    }

    function updateUrlas() { // т.е. вот сюда можно и таблицы переименований вставить будет
        urla = formatSrc( tp.text );
        la.perform();
    }

    property var urla: ""

    PerformDeed {
        id: la
        name: "load-ability"
        input: urla
        manual: true
        record: true
        options: [{risClosed:true, risNewLine: true}]
        // options: [{robotLayer:"system"}]
        // следующее сделано чтобы они не сразу кидались загружаться
    }

    ///////////////////////////////////////

    Row {
        id: cats
        spacing: 10

        TextButton {
            text: "Библиотеки"
            property var url: Qt.resolvedUrl( "../../../libs.json" )
            onClicked: examples.file = url;
            font.underline: true
            Component.onCompleted: clicked();
        }
        TextButton {
            text: "Трюки"
            property var url: Qt.resolvedUrl( "../../../truks.json" )
            onClicked: examples.file = url;
            font.underline: true
        }
        TextButton {
            text: "Локальное"
            property var url: Qt.resolvedUrl( "../../../local.json" )
            onClicked: examples.file = url;
            font.underline: true
        }

        /* Идея а почему нельзя компонент то инлайн задать? В том плане что говорим - вот есть прототип (первый экземпляр). А второй - делай как оно. 
           Помоему удобно. Хотя конечно заморочка.. Типо можно компонент сделать в отдельном файле и все.
           Но с другой стороны. Надо проанализировать, как часто это действие совершается : создание мелких компонент.

        TextButton {
            text: "Библиотеки"
            property var url: Qt.resolvedUrl( "libs.json" )
            onClicked: examples.file = url;
            font.underline: true
            Name: Linka
        }

        Linka {
          url: ....
        }

        Linka {
          url: ....
        }

        */
    }

    Viewva {        
        id: examples
    }
}
