Column {
    id: examples

    Text {
      text: " "
    }

    ExclusiveGroup {
        id: ex
        onCurrentChanged: if (ex.current) tp.value = ex.current.rec.url;
    }

    JsonLoader {
        id: ldr
        output: []
        onFileChanged: { if (ex.current) ex.current.checked = false; ex.current = null; }
    }
    property alias file: ldr.file

    Repeater {
        model: ldr.output.length
        RadioButton {
            width: 400
            text: rec.guid + " - " + rec.info
            exclusiveGroup: ex
            property var rec: ldr.output[index]
        }
    }

    Text {
      text: " "
    }
}


