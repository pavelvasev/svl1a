PerformDeedButton {
  name: "load-entity"
  tag: "top"
  text: "Подключить.."
  width: 150
  id: bt
  record: false // пока не сохраняем, действие генератор
  input: qmlEngine.rootObject
  once: true
  parentToObject: true

  onPerformedGood: {
    risovanie.activate( outputDeed );
  }

}