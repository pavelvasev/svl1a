Deed {
  id: deed
  icon: "вынести"

  params: [tt,cb1,tt2,ti]

  Text {
    text: "Параметр для выноса"
    id: tt
  }

  property var iparams_all: deed.input ? (deed.input.params || []) : []
  property var iparams: iparams_all.filter( function(i) { return (!!i.guid) } )

  ComboBoxK {
    id: cb1
    model: ["---","все"].concat( iparams.map( function(i) { return i.guid; }) )
    property var guid: "chosenparam"
  }

  Text {
    text: "Заголовок"
    id: tt2
  }

  TextInput {
    id: ti
    property var guid: "title"
    text: ""
    property alias value: ti.text
    width: 250
  }

  property var targetParams: {
    if ( cb1.currentIndex === 0 ) return [];
    if ( cb1.currentIndex === 1 ) return iparams_all;
    var f = iparams.find( function(i) { return i.guid == cb1.value } );
    if (f) return [f];
    return [];
  }

  property bool insideDoit: false
  function doit() {
    if (insideDoit) return;
    insideDoit = true;
//    console.log("doit...");
    iparams_all.forEach( function(p) {
      //if (targetParams.indexOf(p) >= 0) return; // не двигаем то что остается
      // да пофигу, и подвигаем..
      //console.log("removing p=",p);
      if (p.paramOwnerRobot) {
        p.parent = p.paramOwnerRobot;
        p.visible = false;
      }
    } );

    var tt2=[];
    targetParams.forEach( function(p) {
      if (!p.paramOwnerRobot) {
        p.paramOwnerRobot = p.parent;
        p.oldSpaceParent = p.parent;
      }
      if (p.secondColumn) tt2.push( p); else {
        p.parent = col;
        p.visible = true;
      }
//      console.log("moved p",p,"to col=",col);
//      console.log("so p.parent=",p.parent);
    });

    tt2.forEach( function(p) {
      p.parent = col;
      p.visible = true;
    });

    insideDoit = false;
  }

  property bool firstParamVisible: targetParams.length > 0 ? targetParams[0].visible : -1
  onTargetParamsChanged: doit()
  onFirstParamVisibleChanged: doit()

  Column {
    id: allc
    property var tag: "right"

    VisibleParam {
      text: ti.value
      target: col      
      id: vp1
      width: 200

      onValueChanged: {
        doit();
      }
    }

    Column {
      id: col
      property bool iAmIfaceHolder2: true
    }
  }

  //////////////////////////////////

  Component.onCompleted: underscene.refineAll()

/*
  ComboBoxParam {
    values: ["Правая выноска","Карман 0"]
    text: "Место размещения"
    guid: "target_place"
    id: tpcb

    onValueChanged: {
      if (value == 1 && karman0) { // см scene-karman0.vl
        vp1.parent = coco;
        col.parent = coco;
        coco.parent = karman0;
      }
      else 
      {
        vp1.parent = qmlEngine.rootObject;
        col.parent = qmlEngine.rootObject;
        qmlEngine.rootObject.refineAll();
      }
    }
  }
*/
}