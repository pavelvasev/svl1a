Ability {
  title: "Добавить интерфейс"
  help: "Создает робот Интерфейса, который можно наполнить параметрами и действиями других роботов"

  function feel( obj ) {
    if (!obj) return 0;
    if (wp.get_type(obj) == "Interface") return 0;
    if (obj.ability && obj.ability.name == "create-interface-o") return 0;
    return 1;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}