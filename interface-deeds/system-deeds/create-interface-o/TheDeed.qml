// см doc/2016-06-23-interface.md и doc/2016-07-24-interface-o.md

Deed {
    id: deed
    property bool thisisinterfacedeed: true

    InterfaceO { id: iface }
    property bool interface: iface
}