Ability {
  title: "Добавить надпись"

  function feel( obj ) {
    if (wp.get_type(obj) == "Deed" && obj.ability && obj.ability.name && obj.ability.name.indexOf("to-interface")>0) return 1;
    if (wp.get_type(obj) == "Interface") return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}