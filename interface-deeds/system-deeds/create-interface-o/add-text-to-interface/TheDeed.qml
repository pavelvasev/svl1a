Deed {
    id: deed
    icon: "н"

    Text {
      text: "Укажите текст, который следует добавить в интерфейс перед выбранным объектом"
      id: tt
    }
    
    TextEdit {
      property alias value: tp.text
      id: tp
      width: 450
      height: 100
      property var guid: "text"      
    }

    Text {
      text: "(необязательно): URL адрес, в этом случае текст станет ссылкой"
      id: tt_url
    }

    TextInput {
      property alias value: tp_url.text
      id: tp_url
      width: 450
      property var guid: "url"
    }    

    params: [tt,tp, tt_url, tp_url]

    function format_url( text, url ) {
      if (!url) return text;

      if (url.indexOf("//") == -1) {
        // not absolute

        if (url[0] != "/") {
          // relative => convert
          var as = getParameterByName("apply_scene") || getParameterByName2("apply_scene");
          if (as) {
            var aa = as.replace(/\\/g,"/").split("/"); aa.pop();
            console.log("as=",as,"aa=",aa);
            url = aa.join("/") + "/" + url;
          }
        }

        if (url.match(/\.(qml|svl|vl)/))
          url = svlReloadUrl + "&apply_scene="+url;        
      }
            
      return "<a href='"+url+"' target='_blank'>" + text + "</a>";
    }

    Text {
      id: out
      text: format_url( tp.value, tp_url.value )
      visible: false
      wrapMode: Text.WordWrap
      width: 300
    }

    property bool generatedParamBeforeLast: true
    property var generatedParam: out
    onGeneratedParamChanged: {
      if (deed.input && deed.input.generatedParam) deed.input.generatedParamChanged();
      if (wp.get_type(deed.input) == "Interface" && deed.input.obhod) deed.input.obhod();
    }

    // это вызовает GeneratedParamChanged а оно вызовет обход а там увидят что параметра нет и перестроят интерфейс
    Component.onDestruction: {
      generatedParamBeforeLast = false;
      generatedParam = null;
    }

    function obhodFun( paramArray, ifaceRobot ) {
      if (generatedParam) {
        paramArray.push( generatedParam );

        if (paramArray.length >= 2) {
          var pre = paramArray[paramArray.length-2];
          paramArray[paramArray.length-2] =paramArray[paramArray.length-1];
          paramArray[paramArray.length-1] = pre;
          if (!pre.skipSc)
            paramArray[paramArray.length-2].secondColumn = pre.secondColumn; // в ту же колонку
        }        
      }
    }
}