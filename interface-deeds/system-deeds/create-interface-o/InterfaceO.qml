Robot {
  id: robot
  icon: "интерфейс"
  type: "Interface"  

  function obhod() {
    var res = [tt5];
    wp.qml_tree_walk( robot, function(item) {
      if (item.obhodFun)
        item.obhodFun( res, robot );
      return true;
    } );
    var cc = [0,-1]; res.forEach(function(f) { if (f.secondColumn) cc[1]+=1; else cc[0]+=1; } ); counters = cc;
    robot.params = res;
  }

  property var counters: [0,0]

  Text {
    id: tt5
    text: {
      return "";
      if (counters[1] == 0) return ""; // во второй колонке ничего
      var t = "Дополнительные возможности: ";
      if (counters[0] > 1) t += "\n\n";
      //if (parent.iAmIfaceHolder2) t = "\n\n" + t;
      return t;
    }  
    property bool secondColumn: true
    property bool skipSc: true
    visible: false
  }

  property bool risClosed: true
  property bool paramsSaveDisabled: true // сохраняем в объектах

  ///////////////////////////////////////////////////////

    function prepare_params() {
        var res = [];

        wp.qml_tree_walk( robot.parent.parent, function(item) {

            if (item.thisisinterfacedeed) return;
            var pp = (item.params || []).filter( function(p) { return !!p.guid; }); // те что сгуидом
            if (pp.length == 0) return true;

            //var t = (item.$class == "Deed" && item.ability ? item.ability.title : wp.get_type( item ) ); // ability имя или тип робота

            var g = wp.get_object_user_name( item );

            var t = g;
            //if (item.logicalName) t = item.logicalName + " " + t;
            if (item.logicalName) t = item.logicalName;

            pp.forEach( function(p) {
                //res.push( { text: (p.text || p.guid) + "@" + t, guid: p.guid, objref: g, param: p } );
                //var tt = (p.text || p.guid) + "@" + t;
                var tt = p.guid + "@" + t;
                //if (item.logicalName) tt = item.logicalName + " -> " + tt;

                res.push( { text: tt, guid: p.guid, objref: g, param: p } );
            });

            return true;
        });
        return res;
    }

    // рассчет списка параметров и действий
    function updatet() {
      //console.log("qq");
      var t = prepare_params();
      tWithBlank = [ { text: "-" } ].concat( t );
      var d = prepare_deed_targets();
      dWithBlank = [ { text: "-" } ].concat( d );
      return tWithBlank;
    }

    property var tWithBlank: []

    ////////////////////////////////////////////////////////
    function prepare_deed_targets() {
        var res = [];
        wp.qml_tree_walk( robot.parent.parent, function(item) {
            if (item.thisisinterfacedeed) return false;
            if (!item.is_wonder_item && !item.visual && !item.positions) { return true; };
            var q = wp.get_object_user_name(item);
            if (item.logicalName) q = item.logicalName;
            res.push( { text: q, item: item, objref: q } );
            return true;
        });
        return res;
    }

    property var dWithBlank: []
}