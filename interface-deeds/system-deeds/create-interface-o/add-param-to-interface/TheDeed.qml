Deed {
    id: deed
    icon: "п"
    // input есть InterfaceO
    details: s1.valueToLoad + " :: " + s2.valueToLoad

    Button {
      width: 200
      text: "Обновить список"
      onClicked: {
        deed.input.updatet();
        //trylocate();
      }
      id: bt
    }

    Text {
      text: "Выберите параметр"
      id: tt
    }

    ComboBox {
      model: tWithBlank.map( function(e) { return e.text; } )
      width: 200
      id: cb

      onCurrentIndexChanged: {
        deed.selected = tWithBlank[cb.currentIndex];
        /// обновим чтобы последующие trylocate работали корректно
        tl_disabled = true;
        s1.valueToLoad = deed.selected ? deed.selected.objref : undefined;
        s2.valueToLoad = deed.selected ? deed.selected.guid : undefined;
        tl_disabled = false;
      }
    }

    property var tWithBlank: deed.input ? deed.input.tWithBlank : []
    onTWithBlankChanged: trylocate();

    property var selected
    //property var selected: tWithBlank[cb.currentIndex];

    property var generatedParam: selected && selected.param ? selected.param : null
    onGeneratedParamChanged: if (deed.input) deed.input.obhod();

    Item {
      property var guid: "objref"
      function saveValue() {
        return selected ? selected.objref : null;
      }
      property var valueToLoad
      onValueToLoadChanged: trylocate()
      id: s1
    }

    Item {
      property var guid: "guid"
      id: s2
      function saveValue() {
        return selected ? selected.guid : null;
      }
      property var valueToLoad
      onValueToLoadChanged: trylocate()
    }

    property bool tl_disabled: false
    function trylocate() {
        if (tl_disabled) return;
        if (!deed.input) return;
        var objref = s1.valueToLoad;
        var guid = s2.valueToLoad;

            if (!objref) return;
            if (!guid) return;

        var tt = deed.input.tWithBlank;
        cb.currentIndex = tt.findIndex( function(item) { return item.guid == guid && item.objref == objref } );

        if (cb.currentIndex < 0) {
          tt = deed.input.updatet(); // попросим пересчитать.. чето нету..
          cb.currentIndex = tt.findIndex( function(item) { return item.guid == guid && item.objref == objref } );
          if (cb.currentIndex < 0) {
             console.error('parameter oblom',objref,guid );
             console.error( 'tWithBlank=',tt);
             cb.currentIndex = 0;
          }
          else
          {
          //  s1.valueToLoad = undefined;
          //  s2.valueToLoad = undefined;
          }
        }
        else
        {
          // s1.valueToLoad = undefined;
          // s2.valueToLoad = undefined;
        }
        //deed.input.obhod();
    }
    onInputChanged: trylocate();
    // todo можно придумать протокол что если loadValue возвращает false значит надо вызвать еще раз loadValue еще раз

    params: [bt,tt,cb,s1,s2,f1,f2]

    Component.onDestruction: generatedParam = null;

    TextParam {
      guid: "forceWidth"
      text: "Задать ширину параметра"
      id: f1
      onValueChanged: if (deed.input) deed.input.obhod();
    }

    TextParam {
      guid: "forceHeight"
      text: "Задать высоту параметра"
      id: f2
      onValueChanged: if (deed.input) deed.input.obhod();
    }

    function obhodFun( paramArray, ifaceRobot ) {
      if (generatedParam) {
        paramArray.push( generatedParam );

        if (f1.value) {
          var q = generatedParam.sizeReason || generatedParam;
          q.width = parseInt( f1.value );
        }
        if (f2.value) {
          var q = generatedParam.sizeReason || generatedParam;
          q.height = parseInt( f2.value );
        }
      }
    }
}