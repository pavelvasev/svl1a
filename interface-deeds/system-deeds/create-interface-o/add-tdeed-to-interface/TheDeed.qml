Deed {
    id: deed
    icon: "д"
    // input есть InterfaceO

    onInputChanged: deed.input.updatet();

    Button {
        width: 200
        text: "Обновить список"
        onClicked: {
            deed.input.updatet()
        }
        id: bt
    }

    Text {
        text: "Выберите объект для действия"
        id: tt
    }

    ComboBoxK {
        model: dWithBlank.map( function(e) { return e.text; } )
        valueSpace: dWithBlank.map( function(e) { return e.objref; } )
        width: 200
        id: cb
        property var guid: "objref"

        //onValueMissing: if (deed.input) deed.input.updatet()
    }

    Text {
        text: "Выберите действие"
        id: tt2
    }

    PerformDeed {
        name: "find-abilities-for-object"
        input: selectedObj ? selectedObj.item : null
        once: true
        id: finda
        record: false
        property var output2: {
            var res = [ { text: "-" } ];
            (output || []).forEach(function(f) {
                // if (f.ability.category == "system-deeds") return;
                if (f.ability.category.indexOf("hidden")>=0) return;
                res.push( { text: (f.ability.title || f.ability.name), guid: f.ability.name, abil: f.ability } );
            });
            return res;
        }
    }

    property var dWithBlank: deed.input ? deed.input.dWithBlank : []

    property var selectedObj: dWithBlank[cb.currentIndex];
    //onSelectedChanged: deed.input.childrenChanged();

    property var deeds: finda.output2 || []
    property var selectedAb: (deeds[cb2.currentIndex] || "").guid;
    property var selectedAbObj: (deeds[cb2.currentIndex] || "").abil;

    ComboBoxK {
        id: cb2

        width: 200
        model: (deeds || []).map( function(e) { return e.text; } )

        valueSpace: deeds.map( function(e) { return e.guid; } )
        property var guid: "guid"

        //onValueMissing: if (deed.input) deed.input.updatet()
    }

    Text {
        text: "Укажите другое название (необязательно)"
        id: tititle_t
    }

    TextInput {
        id: tititle
        property var guid: "title"
        property alias value: tititle.text
        width: 250
    }

    //////////////////////////////////////////

    TextButton {
        visible: false
        id: gi
        property bool secondColumn: true

        text: "<u>" + q +"</u>"
        property var q: {
            if (tititle.value) return tititle.value;
            if (selectedAbObj && selectedAbObj.title) return selectedAbObj.title;
            return selectedAb;
        }

        onClicked: {
            if (pd.outputDeed) {
                risovanie.activate( pd.outputDeed );
            }
            else {
                pd.perform();
            }
        }
        PerformDeed {
            name: selectedAb
            parentToObject: true
            record: true
            once: true
            manual: true
            id: pd
            input: selectedObj ? selectedObj.item : null
            activateDlg: true
        }
    }

    property var generatedParam: selectedObj && selectedAb ? gi : null
    onGeneratedParamChanged: if (deed.input) deed.input.obhod();

    params: [bt,tt,cb,tt2,cb2,tititle_t,tititle];

    Component.onDestruction: generatedParam = null;

    function obhodFun( paramArray, ifaceRobot ) {
      if (generatedParam)
        paramArray.push( generatedParam );
    }
}
