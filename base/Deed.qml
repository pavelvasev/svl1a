// Действие. На самом деле это нифига не действие. А уже тело программы, порожденной действием.

Robot {
  type: "Deed"
  id: deed

  icon: deed.ability ? deed.ability.icon : ""

  // главный входной объект
  property var input

  // главный выходной объект
  property var output

  // ссылка на возможность, породившую это действие
  property var ability

  property var scopeName: wp.get_object_uniq_name(deed)
  property bool scopeNameIsFinal: true // для ScopeCalculator

  // флажок для сохранятелей, что это действие надо записать. Как правило это значит что оно юзером вызвано, а не автоматом.
  property bool record: false
  property bool recordParams: false

  //////////////////////////////////////
  details: ability ? ability.help : ""
  robotLayer: ability ? ability.robotLayer : undefined

  onInputChanged: applyInitParams( initParams )

  //////////////////////////////////////
  property var performedByChainDeed // ссылко на ChainDeed, который выполнил это действие. Но конечно его может и не быть.

  property bool isProgram: true
  title: ability ? (ability.title || ability.name): "безимени"

  property bool setter: false
}