// Робот

Item {
  id: wi

  property var type
  property bool is_wonder_item: true

  property var title
  property var icon
  property var details: ""
  property var params

  // совместимость
  property alias robotDetails: wi.details //ismeta() ? "meta.." : wi.details
  property alias robotIcon: wi.icon
  property string robotLayer

  // рисование
  property bool risClosed: false
  property bool risNewLine: false

  property string logicalName // имя для глаза человека. уникальное или нет - пока не знамо.

  Component.onCompleted: hideParams()

  function hideParams() {
    (params||[]).forEach(function(p) {p.visible=false;});
  }

  ////////////////////////////////////// дальнейшее необходимо для инициализации значений параметров при создании действия или робота
  /* массив записей вида массив [
        {
          "guid": "type",
          "value": "FunctionXY"
        },
        {
          "guid": "guid2",
          "value": "val2"
        },        
      ]
    почему так сложно, я уже не помню, но на это был какой-то резон... проще: { "type" :"FunctionXY", ... }
    -- щас сделано по методу "проще", т.е. ключ-значение
    резон был в том что помимо value может быть что-то еще.. но пока оставим этот резон.
  */
  property var initParams: null

  /* модель initParams (IP). Они приходят из 
     а) цепочек, в поле initParams записи этого действия, и 
     б) надцепочек, из AssignParams
     при этом нам надо сохранять входящие значения IP, так как они
     а) могут быть присвоены только после наличия input и params, а это может произойти позже чем присваивание IP
     б) для сохранения AP. При этом AP должны сравниваться только с 1-й версией IP, а с последующими не должны.
     Хотя нет, должны. Ибо м.б. ситуация [IP(x=0),AP(x=1)], AP(x=0). Тогда создание нового AP должно сохранять x=0
     
     Мысль такая. мы должны сохранять предыдущее значение всех входящих параметров IP?
  */

  // см save-deed
  onInitParamsChanged: applyInitParams( initParams )
  onParamsChanged: applyInitParams( initParams )

  function applyInitParams( ip ) {
    if (!ip) return;
    // хм.. типо это защита от того чтобы не инициализировать до поступления input-а чтоль?.. а как же быть всяким createrobot?.. и прочим кому инпут и не нужен?..
    // if ($properties["input"] && !input) return;
    // console.log("DEED applyInitParams",ip);
    var nowHistoryMode = ip.historyMode;
    
    var pp = params;
    if (!pp) return;

    function locateParamByKey( key ) {
      return pp.find( function(p) { return p.guid == key; } );
    }

    for (var key in ip) {
      var p = locateParamByKey( key );
      //console.log( "lpbk key=",key,"p=",p);
      if (!p) continue;
      var val = ip[key];
      wp.writeParamValue( p, val );
      if (!nowHistoryMode) p.valueToCompareOnSave = val;
    }
  }

  property var paramsWithGuid: (params || []).filter( function(p) { return !!p.guid; } );
  property var paramsForSave: paramsWithGuid.filter( function(p) { return !p.saveDisabled; } );

}
