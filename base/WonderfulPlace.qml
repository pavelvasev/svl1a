// Объект "Волшебный мир". Самый главный наш объект.
// Он есть набор полезных функций и событий.
// Главная полезная функция есть perform_deed - выполнить действие.

Item {
  id: wonderfulplace

  property var wp: wonderfulplace

  /* Подразумение
     Сущности в массе своей это qml объекты (но не обязательно?..)

     Типы
     Ability
     Deed

  */

  // buildin abilities

  function perform_deed( ability_name, target_object_x, options, deed_parent ) {
  /* 1. Встроенные в объект действия
     2. Встроенные в объект а-способности
     3. Встроенные в объект т-способности
     4. Поиск по дереву.
     5. Дополнительные источники.

     Но кстати это все оптимизация и забег на будущее. Для начала можем считать что у нас все способности в wonderfulplace описаны.
  */
     
     //var all_abilities = find_abilities( underscene );  //find_abilities( wonderfulplace ); 
     //var appropriate_abilities = filter_objects_by_qml_name( all_abilities, ability_name ); // идея передавать в find_abilities функцию фильтрации, чтобы сразу искала только то что нам надо. т.е. type="Ability" and name="ConvertTriToNormals"

     var appropriate_abilities = find_abilites_of_name( ability_name );
     
     if (appropriate_abilities.length == 0) {
       console.error("############################################");
       console.error("WonderfulPlace.qml: perform_deed found 0 abilities of name=",ability_name );
       console.error("############################################");
       return undefined;
     }

     for (var i=0; i<appropriate_abilities.length; i++) {
       var ab = appropriate_abilities[i];
       //console.log("checking feel, target_object_x=",target_object_x);
       var prob = ab.feel( target_object_x );
       //console.log("okkk argability=",argability);
       if (!(prob > 0)) continue;

       // тут по идее накопление prob и последующий выбор наибольшего
       
       var deed = ab.act( target_object_x, options, deed_parent );
       // вот, получили действие. Прекрасно!
       if (deed) deedPerformed( deed );

       return deed;
     }

     console.error("WonderfulPlace.qml: perform_deed found ",appropriate_abilities.length," abilities of name=",ability_name,"but no one feels good. target_object_x=",target_object_x );
     //debugger;
     return false;
  }

  function get_object_wonderful_type( some_js_or_qml_object ) {
    if (!some_js_or_qml_object) return "[root]";
    return some_js_or_qml_object.type || some_js_or_qml_object.$class;
  }

  function get_type( obj ) {
    return get_object_wonderful_type( obj );
  }

  ///////////////////////////////////////////////////////////////////////////

  // перекрывается потом из UniqNamer.qml 
  function get_object_user_name( some_js_or_qml_object ) {
    if (!some_js_or_qml_object) return "[no object]"
    var t = "";

    if (t == "Deed") {
      t += "Способность " + some_js_or_qml_object.ability.name;
      if (some_js_or_qml_object._uniqueId) t += "#"+some_js_or_qml_object._uniqueId;
      return t;
    }

    if (some_js_or_qml_object.title && some_js_or_qml_object.title != "Scene object") {
      var parent_title = some_js_or_qml_object.parent ? some_js_or_qml_object.parent.title : null;
      //if (parent_title && some_js_or_qml_object.title)
      if (parent_title) t += parent_title + " "; // + " @> ";

      t += some_js_or_qml_object.title;
      t += " (тип " + get_object_wonderful_type( some_js_or_qml_object ) + ")";
      if (some_js_or_qml_object._uniqueId) t += " (id "+some_js_or_qml_object._uniqueId + ")";
      return t;
    }

    t += get_object_wonderful_type( some_js_or_qml_object );

    if (some_js_or_qml_object.id) t += "#"+some_js_or_qml_object.id;
    // if (some_js_or_qml_object.objectName) t += "#"+some_js_or_qml_object.objectName;
    if (some_js_or_qml_object._uniqueId) t += "#"+some_js_or_qml_object._uniqueId;
    return t;
  }  
  
  function is_wonderful_object( some_js_or_qml_object ) {
    //if (some_js_or_qml_object.type || some_js_or_qml_object.$class) return true; 
    //  || some_js_or_qml_object.is_wonder_item странно конечно на type опираться но нам надо определиться, мы вводим первоклассные чистые вещи или заскопленные?
    if (get_object_wonderful_type(some_js_or_qml_object)) return true;
    return false;
  }

  // обход qml дерева в поиске волшебных объектов и вызове callback-и.
  function qml_tree_walk( treeroot, callback ) {
    if (!treeroot) return;

    if (is_wonderful_object( treeroot )) {
      var res = callback( treeroot );
      if (!res) return false;
    }

    var cc = treeroot.children;
    for (var i=0; i<cc.length; i++) qml_tree_walk( cc[i], callback ); 
  }
  /// t-odo сделать find_all_by и find_first_by с указанием функции-критерия
  // find_all_by можно реализовать через qml_tree_walk
  // а вот find_first_by это да, надо callback результат смотреть


  function find_objects_in_qml_tree( treeroot, acc ) {
    // find_objects_in_qml_tree
    if (!acc) acc = [];

    if (is_wonderful_object( treeroot )) acc.push( treeroot );

    var cc = treeroot.children;
    
    // обходим детей
    for (var i=0; i<cc.length; i++)
      find_objects_in_qml_tree( cc[i], acc ); // check что ацц накапливается! - дык накапливается.. работает жеж
      
    return acc;
  }
  
  function filter_objects_by_type( objarray,type ) {
    var acc = [];
    for (var i=0;i<objarray.length;i++) 
      if (get_object_wonderful_type( objarray[i] ) == type) acc.push( objarray[i] ); // TODO type или $class хотя бы?
    return acc;  
  }

  function filter_objects_by_qml_name( objarray,name ) {
    var acc = [];
    for (var i=0;i<objarray.length;i++) 
      if (objarray[i].name == name) acc.push( objarray[i] );
    return acc;
  }

  function find_abilities( treeroot ) {
    // на самом деле нам tree root и не нужен
    return allAbilities.items;
  }

  function find_abilites_of_name( name ) {
    return abHash[name] || [];
  }

  property var abHash: { return {}; }

  Detector {
    id: allAbilities
    function test( obj ) {
      return (obj.type==="Ability")
    }
    onItemAdded: {
      foo(obj);
    }

    function foo(obj) {
      //console.log("############### obj.name=",obj.name);
      if (!obj.name) {
        obj.nameChanged.connect( allAbilities, function() { foo(obj); } );
      }
      else
      {
        if (!abHash[ obj.name ]) abHash[ obj.name ] = [];
        abHash[ obj.name ].push( obj );
      }

    }
    onItemRemoved: {
      var i = (abHash[ obj.name ] || []).indexOf( obj );
      if (i >= 0) abHash[ obj.name ].splice( i,1 );
    }
  }

  signal abilityLoaded( string name );
  signal deedPerformed( object deed );

  /////////////

  function writeParamValue( p, val ) {
      if (!p) return;

      if (p.loadValue)
          p.loadValue(val);
      else
      if (p.$properties["valueToLoad"])
          p["valueToLoad"] = val;
      else
          p["value"] = val;
  }

  function readParamValue( p ) {
    if (!p) return;

    return p.saveValue ? p.saveValue() : (p.valueToSave || p.value);
  }

  function writeParam( obj, paramName, val ) {
    if (obj && obj.paramsWithGuid) {
      var p = obj.paramsWithGuid.find( function(p) { return p.guid == paramName; } );
      if (p) writeParamValue( p, val );
    }
  }

  function readParam( obj, paramName ) {
    if (obj && obj.paramsWithGuid) {
      var p = obj.paramsWithGuid.find( function(p) { return p.guid == paramName; } );
      if (p)
        return readParamValue( p, val );
    }
    return undefined;
  }

  //////////////
}
