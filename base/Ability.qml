// Возможность

Robot {
  id: ability
  type: "Ability"

  // name - идентификатор действия, по нему его все именуют
  property var name: undefined

  // категория - для ориентации человека. просто строчка.
  property var category

  // название для человека
  property var text: name
  property alias title: ability.text

  // справка для человека
  property var help
  property var moreUrl
  // временное свойство, если true то справа появится кнопка вызывающая это действие
  property var auto: false

  // если вернет > 0, значит эта возможность применима к заданному объекту
  function feel( target_object_x ) 
  {
    return 0;
  }

  property var initParams; // см save-deed 

  function act( target_object_x, additional_input_params_hash, deed_parent ) {
    var init = {};
    for (var j in additional_input_params_hash) {
      init[j] = additional_input_params_hash[j];
    }
    
    init.input = target_object_x;
    if (!init.ability) init.ability = ability;
    

    /////////////////////////////////////////// initParams
    // прицепляем свои параметры, и затем те что пришли снаружи
    var kk = {};
    if (ability.initParams)
      for (var key in ability.initParams)
        kk[key] = ability.initParams[key];

    if (init.initParams)
      for (var key in init.initParams) 
        kk[key] = init.initParams[key];

    init.initParams = kk;    
    ///////////////////////////////////////////

    // if (init.ability.icon) init.icon = init.ability.icon;

    //console.log("act, additional_input_params_hash=",additional_input_params_hash,deedPath );
    var deed = gen.generate( init, deed_parent ); 
    //if (deed) performedGood( deed); else performedFail(false);
    return deed;

    //console.log("acted! ",deedPath,"r=");
    //return r;
  }

  property var deedPath

  QmlGenerator {
    id: gen
    source: deedPath
//    onLoaded: underscene.refineAll();
  }

  property bool compareWithInitParamsDuringSave: false

  // ну это уже так, для удобства.. но вроде даже пока и не надо
  // signal performedGood( object obj );
  // signal performedFail(bool pending);
}