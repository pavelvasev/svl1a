// Загружатель Ability по списку из файла abilities.txt в указанной директории.
// Вообще это старое. Сейчас действие load-ability, см base-helpers/load-ab-deeds

Item {
  id: it
  property var basedir: ""
  //property var file: basedir + "/abilities.txt"

  TextLoader {
    //file: it.file
    file: basedir + "/abilities.txt"
    id: ldr
  }
  property var files: ldr.output.split("\n")

  Repeater {
    model: files.length
    Loader {
      source: { 
        //console.log( "index=",index,"value=",files[index] );
        var f = files[index];
        if (!f || f.length == 0) return null;
        
        f = f.replace(/\\/g,"/"); // повернем все влеши на линукс-стиль
        if (f.indexOf("-/") > 0) return null; // не загружаем из каталогов, заканчивающихся на -

        return basedir + "/" + f; 
      }

      onLoaded: { 
        var c = source.split("/");
        item.name = c[ c.length-2 ];
        if (!item.category) item.category = c[ c.length -3 ];
        // возможно, тут хорошо timeout небольшой..
        wp.abilityLoaded( item.name );
        //console.log("assigned name=",item.name );
      }
    }
  }
}
