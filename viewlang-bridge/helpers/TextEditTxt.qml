    Column {
        id: te1
        property var guid: "txt"
        property var value: ""
        property var title: ""
        spacing: 4

        property var sizeReason: txt

        onValueChanged: txt.text = value;

        Text {
          text: te1.title
        }

        TextEdit {
            id: txt
            width: 500
            height: 200
            text: te1.value

            onTextChanged: {
                if (te1.value == text) return;
                btEnter.enabled = true;
            }
          }
        }

        Button {
            id: btEnter
            text: "ВВОД"
            enabled: false
            anchors.margins: 5
            onClicked: {
              te1.value = txt.text;
              enabled = false;
            }
        }
    }