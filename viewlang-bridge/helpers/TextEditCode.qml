    Column {
        id: te1
        property var guid: "func_code"
        property var value: ""
        property var title: ""
        spacing: 4

        property var sizeReason: txt

        onValueChanged: txt.text = value;        

        Text {
          text: te1.title
        }

        TextEdit {
            id: txt
            width: 500
            height: 200
            text: te1.value

            onTextChanged: {
                if (te1.value == text) return;
                btEnter.enabled = true;
            }

         /*
          Text {
            text: "<a href='javascript: window.open(\"http://viewlang.ru/superviewlang11a/experimental/test-ace/1.html\",\"svlcodeedt\");'> Редактор </a>"
            anchors.right: parent.right
            anchors.top: parent.bottom+2
          }
          */

          TextButton {
            anchors.right: parent.right
            anchors.top: parent.bottom+3
            text: "<a href='javascript:0;'>Редактор</a>"
            property var urr: Qt.resolvedUrl("../../experimental/test-ace/1.html");
            property var editorWindow
            onClicked: {
                    editorWindow = window.open( "about:blank","_blank", "width=700, height=400, top=100,left=100" );
                    editorWindow.opener = null;
                    editorWindow.document.location = urr;

                    setTimeout( function() {
                      editorWindow.postMessage( {"cmd":"init","code":txt.text},"*" );
                    }, 500 );
            }
          }
        }

        Button {
            id: btEnter
            text: "ВВОД"
            enabled: false
            anchors.margins: 5
            onClicked: {
              te1.value = txt.text;
              enabled = false;
            }
        }

    }