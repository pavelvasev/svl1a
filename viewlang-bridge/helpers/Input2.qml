// Qml-объект "Выбор второго объекта"
// используется в параметрах действий
// вход: guid, text

// выход: .value - выбранный объект
//        this - параметр для добавления в гуи
Column {
    id: it
  
    function feel( obj ) {
      return (wp.get_type( obj ) == "Array2dRobot") && obj !== it.parent.output;
    }

    Detector {
      function test(obj) {
        return it.feel( obj );
      }
      id: arrdet
    }

    property var output
    property alias result: it.output
    property alias value:  it.output

    property var valueToSave: { return { objref: wp.get_object_user_name( it.output ) } }
    property var valueToLoad: ""

    function loadValue(v) {
      if (arrdet.items.length == 0) arrdet.refresh();
      valueToLoad = v;
    }

    property var guid: "input2"

    property alias text: txt1.text

    Text {
      id: txt1
      text: "Укажите второй объект"
    }

    ComboBox {
      id: p
      width: 300  // длинные имена бывают      
      model: ["---"].concat( arrdet.items.map( function(i) { return i.logicalName ? i.logicalName : wp.get_object_user_name( i ); } ) )

      ////////////////////
      
      currentIndex: 1 + arrdet.items.findIndex( function(i) { return wp.get_object_user_name( i ) == it.valueToLoad.objref } )

      onCurrentIndexChanged: it.output = arrdet.items[ currentIndex-1 ];
    }
}