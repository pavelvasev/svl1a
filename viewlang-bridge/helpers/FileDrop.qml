// Monitors dropZone object for file drop events
// Fires `drop` event when drop occurs
// Has `files` property with dropped files

Rectangle {
  id: rect
  property var dropZone: rect
  property var dropDom: dropZone ? dropZone.dom : null

  property var files: []
  
  property var visualFeedback: true

  signal drop();

  Component.onCompleted: {
    if (!dropDom) return;
    // actual work
    dropDom.addEventListener('dragover', handleDragOver, false);
    dropDom.addEventListener('drop', handleFileSelect, false);  
    
    // visual feedback
    if (visualFeedback) {
      dropDom.addEventListener('dragenter', dragEnter  , false);
      dropDom.addEventListener('dragleave', dragLeave  , false);    
    }
  }

  function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  }  

  function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    files = evt.dataTransfer.files; // FileList object.

    drop();

    if (visualFeedback)
      event.target.style.border = prevBorder;
  }
  
   property var prevBorder: "none"

   function dragEnter(event) {
     prevBorder = event.target.style.border;
     event.target.style.border = "2px dashed green";
   }

   function dragLeave(event) {
     event.target.style.border = prevBorder;
   }

}