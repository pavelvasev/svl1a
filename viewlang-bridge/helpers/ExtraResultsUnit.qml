Item {
    id: itm

    property var mytype: "Array2dRobot"
    property var reg: "extraArray"
    property var iconprefix: "arr:"

    function extraResult( name, serialized ) 
    {
      var idx = extraResults.findIndex( function(r) { return r.name == name } );

      if (idx >= 0 && extraResults[idx].type !== mytype) {
        extraResults[idx].$delete();
        extraResults.splice( idx, 1 );
        idx = -1;
      }

      if (idx < 0) {
        var rec = {};
        rec.name = name;
        rec.type = mytype;
        rec.obj = Qt.createQmlObject( rec.type + " {}", itm.parent, "", __executionContext );
        extraResults.push( rec );
        idx = extraResults.length -1;
      }
      
      var it = extraResults[ idx ].obj;
      it.importSerialized( serialized );
      it.icon = iconprefix + name;
    }

    // property var extraResults: [] это наверх надо, если хотим общее хранилище

    Component.onCompleted: {
      itm.parent.parent[ reg ] = itm.extraResult;
    }
}