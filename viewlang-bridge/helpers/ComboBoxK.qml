/*
  ComboBox
    model
    currentIndex
    currentText
    value

  1. value V задают извне. тогда CB должен найти в себе правильный индекс
  2. если индекса этого нет, он должен перейти в режим ожидания искомого значения V
  3. в режиме ожидания отслеживается изменение модели. если в новой модели есть V, происходит выход из режима ожидания
  4. если пользователь сдвинул руками, а это при currentIndex изменении, то тоже выходим из режима ожидания

  5. также value должно отслеживать выходной результат. это либо выбранный пользователем, либо ожидаемый
*/

ComboBox {
  id: cb

  property var valueSpace: model

  property var value: cb.valueSpace[ cb.currentIndex ]

  property var valueExternal
  property bool waitingMode: !!valueExternal

  

  signal valueMissing();

  onValueChanged: {
//    console.log( "KKKK: value changed to",value);
//    if (value != "manage-object-radius" && value)
//       debugger;
    if (value != cb.valueSpace[ cb.currentIndex ]) { // задали извне.. будем отслеживать
      findIdx( value );
    }
  }

  function findIdx( v ) {
    var c = (cb.valueSpace || []).indexOf( v );

    if (model && c >= 0 && c < model.length) {
      currentIndex = c;
      valueExternal = undefined;
//      console.log(" KKKK success: v=",v);
    }
    else
    {
      valueExternal = v;
      valueMissing();
    }
  }

  checkCurrentIndex: false

  onCurrentIndexChanged: {
    // ручками пошевелили
    var t = valueSpace[ currentIndex ];
    value = t;
    valueExternal = undefined;
  }

  onValueSpaceChanged: if (valueExternal) findIdx( valueExternal );
  onModelChanged: if (valueExternal) findIdx( valueExternal );
}