// для применения в действиях "преобразовать" и "вычислить"
Item {
  property var extraResults: []

  ExtraResultsUnit {
    property var mytype: "Array2dRobot"
    property var reg: "extraArray"
    property var iconprefix: "arr:"
  }

  ExtraResultsUnit {
    property var mytype: "TextRobot"
    property var reg: "extraText"
    property var iconprefix: "txt:"
  }

}