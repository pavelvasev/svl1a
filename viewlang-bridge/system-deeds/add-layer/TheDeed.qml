Deed {
  id: deed

  params: [tt2,ti]

  Text {
    text: "Имя слоя"
    id: tt2
  }

  TextInput {
    id: ti
    property var guid: "sloyname"
    property alias value: ti.text
    text: "новый"
    width: 250
  }

  robotLayer: ti.value
}