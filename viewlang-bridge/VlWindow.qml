import ".."
import "../stdlib1"
import "../composer"
 
Scene {
  id: vlwindow
  anchors.fill: parent
  property var underscene: vlwindow
  text: ""
  help: [ ["| Справка",Qt.resolvedUrl("../doc/guide/readme.md.html")] ]

  property var svlBaseUrl: Qt.resolvedUrl("../")
  //property var vlWindowUrl: Qt.resolvedUrl("VlWindow.qml")
  property var svlReloadUrl: window.location.href.replace(/VlWindow\.qml.*$/,"VlWindow.qml");

  ///////////////////////////////////////////////////
  ColorizeAspect{}

  NameUniq {}
  //NameInline {}
  property var backHistory: BackHistoryAspect {}
  ///////////////////////////////////////////////////

  property var wonderfulplace: wpa
  property var wp: wpa
  
  WonderfulPlace {
    id: wpa
  }

  // WonderfulPlace должен быть повыше, ибо он ищет Ability с помощью Detector, а тому чтобы иниц-ся надо onCompleted, и он желательно должен быть 1м
  ////////////////////////////////
  property var presetLib: presetLibA
  PresetLib {
    id: presetLibA
  }
  ////////////////////////////////

  property var abCatalog: [ Qt.resolvedUrl("../abilities.txt") ]

  LoadAbMethods {}

  PerformDeed {
    name: "load-ability"
    input: abCatalog    
    options: [{robotLayer:"system"}]
    // следующее сделано чтобы они не сразу кидались загружаться
    enabled: false
    Component.onCompleted: setTimeout( function() { enabled=true; },100 );
  }

  Loader {
    id: loader
    anchors.fill: parent
    source: getParameterByName2("viewlangscene") || getParameterByName("viewlangscene") || Qt.resolvedUrl( "../scena.vl" )
  }   

  /////////////////////////////////////////////////////////////////////////

  // проход по возможностям, особо отмеченным, и создание кнопочки для них..
  // блин, а надо это?.. здесь?
  /*
  property var ab_auto: wonderfulplace.find_abilities( underscene ).filter( function (ab) { return ab.auto && ab.feel( null ); } );
  Row {
    spacing: 5
    Repeater {
      model: ab_auto.length
      PerformDeedButton {
        text: ab_auto[index].title
        name: ab_auto[index].name
      }
    }
  }
  */

  VisibleParam {
    source: ris1
    onCheckedChanged: underscene.refineAll();
    text: "Карта роботов"
    id: showRobotsParamP
  }
  property alias showRobotsParam: showRobotsParamP

  Risovanie {
    id: ris1
  }
  property alias risovanie: ris1  

  /////////////////////////////

  FileDrop {
    id: dro
    dropZone: vlwindow
    onDrop: {
      for (var i=0; i<dro.files.length; i++) {
        var d = genfil.generate( {file: dro.files[i]} );
      }
    }
  }

  FileDrop {
    id: dro2
    dropDom: renderer.domElement // общее окошко
    onDrop: {
      for (var i=0; i<dro2.files.length; i++) {
        var d = genfil.generate( {file: dro2.files[i]} );
      }
    }
  }

  QmlGenerator {
    id: genfil
    source: "FileRobot.qml"
    onLoaded: { object.$class="FileRobot"; }
  }

  property var pending: Pending {}

  ///////////////////////////////
  // вот он вечный вопрос, где должно определяться
  CloudControls {}

  Component.onCompleted: {
    var css = "color: green;";
    console.log("%cWelcome to Superviewlang", css);

    //http://stackoverflow.com/questions/7505623/colors-in-javascript-console
  }

  ///////////////////////////////
  property var karman1
  /*
  property var karman1: k1
  Karman1 {
   id: k1
  }
  */
  ////////////////////
  property bool iLoveParams: true
  function recompute() {}

  ///////////////////////////////

  WorldServerBro {}
}
