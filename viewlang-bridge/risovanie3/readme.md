## Схема
```
Detector::onItemsChanged --> updateTimer::start();
updateTimer::onTriggered --> computeTree : 1
computeTree ..> t
updateTimer::onTriggered --> t : 2
t --> treetop
t --> treechilds
t -> itemsLayers
itemsLayers->ComboBox
ComboBox..>computeTree
treetop --> paintTop
paintTop --> Repeater
Repeater --> PaintRobot
treechilds --> getChildsOf
PaintRobot .> getChildsOf
```
http://www.planttext.com/planttext

### Идеи
В иконке робота рисовать кружочками иконично разноцветно
* перечень параметров 
* перечень возможных действий

### Объекты могут управлять воим отображением
robotIcon - иконка для отображения в кадратиках. можно использовать li из fontawesome
risClosed - рисовать по умолчанию схлопнутым
risNewLine - рисовать с новой линии
enabled - если нет, то не будет показано
robotLayer - слой

guiHideDeeds - скрыть действия в окне робота
paramsSaveDisabled - запрет сохранеия всех параметров у этого робта

Параметрам индивидуально:
saveDisabled - запрет сохранения параметра 

