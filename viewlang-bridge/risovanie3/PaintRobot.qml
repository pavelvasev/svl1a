import "../.."

// рисует робота и его детей, рекурсивно
// наверху должна быть функция getChildsOf(robot) и еще curHiliteObj и еще activate(obj)
Rectangle {
    id: prect

    Component.onCompleted: {
      if (robot.risPainted) robot.risPainted( prect );
    }

    property bool impaintrobot: true
    property alias selfrect: selfr

    property var tabla: { return {
            "Trimesh" : "M",
            "Triangles" : "T",
            "AutoPlaneX" : "_",
            "Points" : "...",
            "CreativePoints" : "...",
            "Spheres" : "fa-circle-o",
            "Boxes" : "fa-cube",
            "Arrows" : "fa-arrows-alt",
            "Arrowstrip" : "fa-arrows-alt" ,
            "Cylinders" : "fa-toggle-off",
            "Cylinderstrip" : "fa-toggle-off",
            "Cones" : "fa-sort-up",
            "Lines" : "=",
            "Linestrip" : "fa-line-chart",
            "TextSprite": "fa-file-text-o",
            "SceneObject" : "fa-asterisk",
            "Scene": "fa-university",
            "Ability" : "A",
            "Deed" : "",
            "AxesC" : "оси",

            "NormalsFromTrimesh" :"fa-arrows-v",
            "WithSelectOne" : "fa-hand-pointer-o"
        }
    }
    

    border.color: single ? "transparent" : "grey" // || forceSingle
    //border.color: "grey"

    color: "transparent"

    width: alltogether.width + (single ? 0 : 4)
    height: alltogether.height + (single ? 0 : 4)

    property var robot
    property var robotNearestChilds: getChildsOf( robot )
    property var robotNearestChildsObjects: robotNearestChilds.filter(function(c) { return !c.isProgram; } );
    property var robotNearestChildsDeeds: robotNearestChilds.filter(function(c) { return c.isProgram; } );

    /*
    property var robotNearestChildsObjectsA: allNewLines ? [] : robotNearestChildsObjects.filter(function(c) { return !c.risNewLine; } );
    property var robotNearestChildsObjectsB: allNewLines ? robotNearestChildsObjects : robotNearestChildsObjects.filter(function(c) { return !!c.risNewLine; } );
    property bool allNewLines: (robot ? robot.allNewLines : false)
*/
    property var robotNearestChildsObjectsA: robotNearestChildsObjects.filter(function(c) { return !c.risNewLine; } );
    property var robotNearestChildsObjectsB: robotNearestChildsObjects.filter(function(c) { return !!c.risNewLine; } );

    property var robotNearestChildsDeedsA: robotNearestChildsDeeds.filter(function(c) { return !c.risNewLine && !c.setter; } );
    property var robotNearestChildsDeedsB: robotNearestChildsDeeds.filter(function(c) { return !!c.risNewLine && !c.setter; } );
    property var robotNearestChildsDeedsSetters: robotNearestChildsDeeds.filter(function(c) { return c.setter; } );

    property var single: {
        return (robotNearestChilds.length == 0);
        //|| (robotNearestChilds.length == robotNearestChildsDeeds.length); // все дети - действия
    }
    
    property var forceText

    //property bool isDeed: (robot && wp.get_type(robot) == "Deed")
    property bool isDeed: robot && robot.isProgram

    property int recursionLevel: 0

    property bool forceSingle: false
    property bool nowClosed: robot && robot.risClosed ? true : false

    function textFromRobot(r) {

        var t = tabla[ wp.get_type(r) ] || tabla[r.title] || "";
        if (r.robotIcon) t=r.robotIcon;
        if (forceText) t=forceText;

        if (t.indexOf("fa-") >= 0)
            t = '<i class="fa ' + t + '"></i>';

        if (r.logicalName) {
            if (t.indexOf("<i") >= 0)
                t = t + ":" + r.logicalName;
            else
                t = r.logicalName;
        }

        return t;
    }

    /*
    row(alltogether):
      col: setters
      col(rr):
        row(rr1): self + children
        childrenB(risNewLine)
        row: deeds
        deedsB(risNewLine)
    */

    Row {
        id: alltogether
        x: single ? 0 : 2
        y: single ? 0 : 2
        spacing: 3

        Column {
            id: setters

            spacing: 3

            // пустышка для обозначения себя-места
            Rectangle {
                width: repsetters.model > 0 ? 24 : 0
                height: width
                //visible: repsetters.model.length > 0
                color: "transparent"
            }

            Repeater {
                id: repsetters
                model: forceSingle || nowClosed ? 0 : robotNearestChildsDeedsSetters.length
                Loader {
                    //active: sublevelsEnabled
                    source: "PaintRobot"
                    id: ldr

                    onLoaded: {
                        ldr.item.robot = robotNearestChildsDeedsSetters[index];
                        ldr.item.recursionLevel = recursionLevel+1
                        //ldr.item.forceText = ldr.item.paintRobotText;
                    }
                }
            }
        }


        Column {
            id: rr
            spacing: 3


            Row {
                id: rr1
                spacing: 3

                // нарисуем себя
                Rectangle {
                    width: 24
                    height: 24
                    //color: isDeed ? "#9f56d0" : "green"
                    //color: isDeed ? "#0fb10f" : "#2892e8"
                    //color: isDeed ? "#0c9a0c" : "#0b85e8" // "#0f76dc"
                    //color: isDeed ? "#070" : "#2121d0" //"#0505bb" // "#0f76dc"
                    color: robot.forceColor ? robot.forceColor : (isDeed ? risovanie.deedColor : risovanie.robotColor)

                    //radius: isDeed ? 12 : 3
                    radius: 3
                    border.color: curHiliteObj == robot ? "red" : "transparent"
                    id: selfr

                    Text {
                        id: tx
                        //text: textFromRobot(robot) + (nowClosed && robotNearestChilds.length > 0 ? " ++" : "") + (robot.record ? "*" : "")
                        text: textFromRobot(robot) + (nowClosed ? " ++" : "") + (robot.record ? "*" : "")
                        color: '#eeeeee'
                        anchors.centerIn: parent
                        onTextChanged: if (tx.width > 24) selfr.width = tx.width+4;
                        width: implicitWidth > 0 ? implicitWidth : 12
                        height: implicitHeight > 0 ? implicitHeight : 16
                    }

                    MouseArea {
                        anchors.fill: parent
                        //onDoubleClicked: {}
                        onClicked: {

                            if (curHiliteObj == robot) {
                                if (robot.$properties && robot.$properties["risClosed"])
                                    robot.risClosed = !nowClosed;
                                else
                                    nowClosed = !nowClosed;
                            }

                            risovanie.activate( robot );
                            // нам это надо чтобы переопределять... хотя.. можем отслеживать и curHiliteObj

                            /*
                        curHiliteObj = robot;
                        robotAction.perform();
                        */
                        }
                    }

                }

                // нарисуем детей
                Repeater {
                    model: forceSingle || nowClosed ? 0 : robotNearestChildsObjectsA.length
                    Loader {
                        //active: sublevelsEnabled
                        source: "PaintRobot"
                        id: ldr
                        onLoaded: {
                            ldr.item.robot = iTargetRobot;
                            ldr.item.recursionLevel = recursionLevel+1;
                        }
                        property var iTargetRobot: robotNearestChildsObjectsA[index]
                        onITargetRobotChanged: if (ldr.item) ldr.item.robot = iTargetRobot;
                    }
                }

            } // row 1 - подобъекты

            // нарисуем детей B

            Repeater {
                model: forceSingle || nowClosed ? 0 : robotNearestChildsObjectsB.length
                Loader {
                    //active: sublevelsEnabled
                    source: "PaintRobot"
                    id: ldr
                    onLoaded: {
                        ldr.item.robot = iTargetRobot;
                        ldr.item.recursionLevel = recursionLevel+1;
                    }
                    property var iTargetRobot: robotNearestChildsObjectsB[index]
                    onITargetRobotChanged: if (ldr.item) ldr.item.robot = iTargetRobot;
                }
            }

            Row {
                id: rr2
                spacing: 3

                // нарисуем действия
                Repeater {
                    model: forceSingle || nowClosed ? 0 : robotNearestChildsDeedsA.length
                    Loader {
                        //active: sublevelsEnabled
                        source: "PaintRobot"
                        id: ldr

                        onLoaded: {
                            ldr.item.robot = robotNearestChildsDeedsA[index];
                            ldr.item.recursionLevel = recursionLevel+1
                            //ldr.item.forceText = ldr.item.paintRobotText;
                        }
                    }
                }

            } // row 2 - подобъекты-действия A

            // нарисуем действия B
            Repeater {
                model: forceSingle || nowClosed ? 0 : robotNearestChildsDeedsB.length
                Loader {
                    //active: sublevelsEnabled
                    source: "PaintRobot"
                    id: ldr

                    onLoaded: {
                        ldr.item.robot = robotNearestChildsDeedsB[index];
                        ldr.item.recursionLevel = recursionLevel+1
                        //ldr.item.forceText = ldr.item.paintRobotText;
                    }
                }
            }


        } // column

    }

    ////////////////////////////////////////

}
