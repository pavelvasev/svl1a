import "../.."

Item {
    id: ris

    FontAwesome {}

    //////////////////////////////// объекты

    property alias detector: robots
    Detector {
        id: robots
        locateExisting: false // дубликаты..
        function test( obj ) {

            // действия показываем
            if (wp.get_type( obj ) == "Deed") { 
                // return false;
                //return true;
                // у действия должны быть параметры или дети
                // такой воть трючок, а то много полулевых действий возникает..
                if (obj.robotLayer == "hidden") return;

                return !!(obj.params || (obj.children && obj.children.length > 0));
            }
            // способности не показываем, зачем нам они
            if (wp.get_type( obj ) == "Ability") return false;

            // особый случай для лампочек..
            if (obj.$properties["enabled"] && !obj.enabled) return false;
            // и для осей
            var p = obj.parent; 
            if (p && p.$properties["enabled"] && !p.enabled) return false;
            // и для первых детей скрытых действий..
            if (p && p.robotLayer == "hidden") return;

            // остальные роботы
            return !!(obj.positions || obj.visual || obj.is_wonder_item);
        }

        onItemsChanged: updateTimer.start();
        // onItemRemoved: console.log("detector item removed",obj);
    }

    // ок набрали robots.items - список понимаешь роботов системы
    function computeTree( items ) {
        // console.log( "##############",items );

        var tree = {};
        var childs = {}
        var parents = {};

        // вычисляем 
        // бо tree имяобъекта -> объект
        for (var j=0; j<items.length; j++) {
            var i = items[j];

            var p = i;
            var foundp = null;
            var name = wonderfulplace.get_object_user_name( i );            
            tree[name] = i;            

            // бо childs имяобъекта -> список объектов детей
            // бо parents имяобъекта -> имя родителя
            while (p = p.parent) {
                var pname = wonderfulplace.get_object_user_name( p );
                if (tree[pname]) { // нашли ближайшего родителя, который в наших списках
                    if (!childs[pname]) childs[pname] = [];
                    childs[pname].push( i );
                    parents[name] = pname;
                    break;
                }
            }
        }
        //console.log(layers);


        // вычисляем список treetop - объекты верхенего уровня // на основе tree и parents
        // в этом же цикле проводим вычисление слоев. почему здесь - загадка, но это и не важно
        var treetop = [];        
        var layers = {};

        for (n in tree) {
            var i = tree[n];
            if (i.robotLayer && !layers[i.robotLayer]) layers[i.robotLayer] = 1;
            if (!parents[n]) treetop.push( tree[n] );
        }
        layers = [""].concat(  Object.keys(layers) );

        // console.log("tree=",tree);
        //console.log("treechilds=",childs);
        //console.log("treetop=",treetop);

        // собственно нам что надо для рисования? 1. treetop, 2. метод "дай детей элемента"
        return [ treetop, childs, layers ];
    }

    Timer {
        id: updateTimer
        interval: 200
        repeat: false
        onTriggered: {
            // console.log("~~~~~~~~~~~~~~ computeTree timer triggered");
            t = computeTree( robots.items );
            tToItems();
            underscene.refineAll();
        }
    }

    function tToItems() {
            if (t) {
              itemsLayers = t[2];
              treechilds = t[1];
              treetop = t[0];
            }
    }

    //property var itemsLayers: t ? t[2] : []
    property var itemsLayers: []
    property var currentLayer: ""

    property var t //: computeTree( robots.items )
    property var treetop: [] // t ? t[0] : [] // верхние роботы массив. по факту там 1 сцена
    property var treechilds: [] // t ? t[1] : [] // дети - хэш имя -> объект робота

    function getChildsOf( p ) {
        var pname = wonderfulplace.get_object_user_name( p );
        return treechilds[pname] || [];
    }

    // берет treetop, и ищет в сцену, и если находит, то разворачивает ее содержимое
    property var paintTop: filterByLayer( expandSceneItems( treetop ) ) // итого, сейчас layers влияет на вершинные весчи

    // берет массив items, и если находит Scene среди них, то заменяет её на её детей.
    function expandSceneItems( items ) {
        var q = [];
        for (var j=0; j<items.length; j++) {
            if (items[j].$class == "Scene") {
                q = q.concat( getChildsOf( items[j] ) );
            }
            else
            {
                q.push( items[j] );
            }
        }
        return q;
    }

    function filterByLayer( items ) {
       var cl = (currentLayer || "");
       return items.filter( function(i) {
          return ((i.robotLayer || "") == cl);
       });
    }

    Item {
        property var tag: "left"

        id: painting
        visible: ris.visible

        width: Math.min( 20 + cc.width, 720 )
        height: cc.height

        css.maxWidth: "700px"
        css.overflowX: "scroll"
        css.overflowY: "hidden"
        css.pointerEvents: "all"
        css.paddingLeft: "20px"
        css.marginRight: "20px"

        Column {
            id: cc

            spacing: 5

            Row {
              Text {
                y:3
                text: "Слой "
              }
              ComboBox {
                model: itemsLayers.map( function (t) {
                  var slovar = { "" : "Основной", "system":"Система"};
                  return slovar[t]||t;
                 }
                )
                onCurrentIndexChanged: { treetop = []; currentLayer = itemsLayers[ currentIndex ]; tToItems(); /*updateTimer.triggered();*/ }
              }
            }

            PaintRobot {
              robot: engine.rootObject
              forceSingle: true
              forceText: " Сцена "
              recursionLevel: 1
            }

            Repeater {
                model: paintTop.length
                PaintRobot {
                    robot: paintTop[index]
                    recursionLevel: 1
                }
            }

            Text {
                text: " "
                height: 30
            }
        }
    } // item

    Component.onCompleted: underscene.refineAll();

    property var curHiliteObj

    PerformDeed {
        input: curHiliteObj
        name: "show-robot-window"
        id: robotAction
        once: true
        manual: true
    }
    
    function activate(obj) {
      curHiliteObj = obj;
      robotAction.perform();
    }

    property color deedColor: "#5959ff"
    property color robotColor: "#070"
}
