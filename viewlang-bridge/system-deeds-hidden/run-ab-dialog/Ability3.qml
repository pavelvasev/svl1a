// Старенький понимаешь диалог
// но зато проверенный

Ability {
  name: "run-ab-dialog"
  id: ability
  
  function feel( target_object_x ) {
    if (engine.rootObject == target_object_x) return 1;
    if (target_object_x) return 0;
    return 1;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}