Deed {
  id: deed
  property var atest: 14

  SimpleDialog {
    title: "Выберите действие из списка всех возможных"
    visible: true
    width: select2objs.width + select2.width+20
    height: col.height + 35 
    id: theDialog

    PerformDeed {
      name: "find-all-possible-argabilities"
      id: findall
      //onOutputChanged: console.log(">>>>>>>>>> output=",output );
    }

    property var filteredAbilities: {
      if (!findall.output) return [];
      var res = findall.output;

      var vis = cbVisual.checked;
      res = res.filter( function(rec) {
        if (!rec.input) return true;
        return !vis == !(rec.input.positions && rec.input.visual); // ето XOR
      } );

      if (vis) {
      var seen = cbVisible.checked;
      res = res.filter( function(rec) {
        if (!rec.input) return true;
        return seen == rec.input.visible;
      } );
      }

      return res;
    }

    property var abilitiesObjectsNames: {
      if (!filteredAbilities) return [];
      var h = {};
      for (var i=0; i<filteredAbilities.length; i++)
        h[ filteredAbilities[i].inputName ] = 1;
      return Object.keys(h);
    }

    function getAbilitiesOfObject( objName ) {
      if (!filteredAbilities) return [];

      var res = filteredAbilities.filter( function(rec) {
        return rec.inputName == objName;
      });

      return res;
    }

    property var abilitiesOfCurrentObject: getAbilitiesOfObject( chosenObjName )
    property var chosenObjName: abilitiesObjectsNames[ select2objs.currentIndex ] 
    property var chosenAb: abilitiesOfCurrentObject[ select2.currentIndex ] 

    property var chosenObject: {
      var rec = filteredAbilities.find( function(rec) { return rec.inputName == chosenObjName } );
      // console.log( "chosen object=",rec );
      return rec ? rec.input : null;
    }

    BoxFromTrimesh {
      source: chosenObject
      visible: theDialog.visible // && source
      color: [0,0,1]
    }

    Column {
      spacing: 5
      id: col

      Row {

      Button {
        text: "Обновить список"
        width: 150
        onClicked: findall.perform();
      }

      CheckBox {
        text: "Трехмерные"
        id: cbVisual
        checked: true
      }
      CheckBox {
        text: "Видимые"
        id: cbVisible
        checked: true
        enabled: cbVisual.checked

      }

      }

      Row {

      ComboBox {
            id: select2objs
            model: abilitiesObjectsNames
            height: 150
            width: 350
            size: 10
      }

      ComboBox {
            id: select2
            model: abilitiesOfCurrentObject.map( function(e) { return e.abilityName; } )
            height: 150
            width: 250
            size: 10
      }

      }
      
      Text {
        text: "Выбрано: " + (chosenAb ? chosenAb.humanname : "-")
      }
      Button {
        text: "Поехали!"
        onClicked: {
          if (chosenAb) {
            showp.object = chosenAb.ability.act( chosenAb.input ); 
            showp.perform();
            theDialog.close();
          }
          else alert("Выберите действие из списка!");
        }
        //visible: chosenAb
      }
   }


  }
 
  Component.onCompleted: {
    underscene.refineAll();
  }

  PerformDeed {
    name: "show-deed-params"
    id: showp
  }

}