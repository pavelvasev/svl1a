Ability {
  name: "find-abilities-for-object"
  category: "hidden"

  function feel( target_object_x ) {
    if (target_object_x) return 1;
    return 0;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}