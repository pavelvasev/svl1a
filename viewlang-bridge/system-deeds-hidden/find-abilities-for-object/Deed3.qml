Deed {
  id: deed

  function compute_object_depth( obj ) {
    var r = "";
    while (obj) {
      obj = obj.parent;
      r += ".";
    }
    return r.substr( 2 );
  }
  
  function go() {
      //console.log("------------> go for",deed.input);
      var objects = [deed.input];
      var abilities = wonderfulplace.find_abilities( underscene );
      var acc = [];

      //objects = [null].concat( objects );

      for (var i=0; i<objects.length; i++) 
      for (var j=0; j<abilities.length; j++)
      {
        var arg = abilities[j].feel( objects[i] );
        if ( !(arg > 0) ) continue;

        var rec = { input: objects[i], ability: abilities[j] };
        rec.inputName = compute_object_depth( rec.input ) + wonderfulplace.get_object_user_name( rec.input );
        rec.abilityName = rec.ability.name;
        rec.humanname = "object " + rec.inputName + " ability " + (rec.ability ? rec.ability.name : "-");

        acc.push( rec );
      }
      //console.log("--->",acc);
      
      return acc;
  }

  onInputChanged: output = go();

  Component.onCompleted: if (!output) output = go(); // не binding потому что иначе рекурсия получается

}