Ability {
  name: "show-robot-window"
  id: ability
  
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    return 1;
  }

  deedPath: Qt.resolvedUrl("TheDeed.qml")
}