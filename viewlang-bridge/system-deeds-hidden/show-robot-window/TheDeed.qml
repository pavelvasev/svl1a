/*
 заметка про дополнительно

 Идея в том чтобы не показывать лишнего, ненужного. Поэтому сейчас в диалоге действий мы скрываем действия и ставим чекбокс Действия.
 А в диалоге робота - делаем чекбокс Дополнительно и показываем "общие" действия.

 Но на самом деле это 1 механизм. Убрать лишнее, а по чекбоксу показать.

 Причем на него можно смотреть по-разному с точки зрения множеств которые он определяет.
 Например при включенном состоянии - может показывать все множество. а может только часть остатка.
 т.е. ALL = A1+A2 и варианты - I Показать A1 и показать A1+A2, II Показать A1 и показать A2 (т.е. без A1)
 Плюс разбиений может быть не 2 а больше.

 В общем это все к вопросу об интерфейсах ;-)
*/

Deed {
    id: deed
    robotLayer: "system"

    property var perevod: { return {
            "array-special" : "специальное",
            "computing" : "вычисления",
            "system-deeds" : "общие",
            "create" : "создать",
            "change" : "редактировать",
            "paint"  : "нарисовать",
            "load" : "загрузка",
            "edit" : "поменять",
            "edit-manual" : "редактировать",
            "calc" : "посчитать",
            "convert" : "преобразовать",
            "manage-visual-properties" : "визуальные свойства",
            "viewlang-tools" : "визуальные инструменты",
            "query" : "вопрос",
            "access" : "доступ к данным"
        }
    }

    function tra( tx )
    {
        return perevod[tx] || tx;
    }

    property alias robot: deed.input
    property bool isDeed: robot && robot.isProgram
    property bool deedShowDeeds: false // это текущее состояние
    property bool initHideDeeds: isDeed || (robot && robot.guiHideDeeds)

    property bool initHideCommonDeeds: !initHideDeeds && robot && robot.$class !== "Scene"
    property bool showCommonDeeds: true
    onInitHideCommonDeedsChanged: {
        //showCommonDeeds = !initHideCommonDeeds;
        cbShowCommonDeeds.checked = !initHideCommonDeeds;
    }
    onRobotChanged: cbShowCommonDeeds.checked = !initHideCommonDeeds;

    property string selectedAbilityName: ""
    property var selectedAbility: {
        var sn = selectedAbilityName;
        var q = abils.output;
        if (!sn || !q) return "";
        var ab = q.find( function(val) { return val.ability.name == sn } );
        if (!ab) return "";
        return ab.ability;
    }

    PerformDeed {
        name: "find-abilities-for-object"
        
        id: abils
        once: true
        enabled: !!abils.input
        manual: true
    }

    property var categories: {
        var q = abils.output;
        if (!q) return [];
        var h={};
        for (var i=0; i<q.length; i++)
            h[ q[i].ability.category ] = 1;
        var cats = [];
        for (var c in h)
            cats.push( c );

        return cats.sort();
    }

    function getAbilitiesOfCategory( catName )
    {
        //return (abils.output || []).filter( function(e) { return e.ability.category == catName }).map( function(e) { return e.ability } );
        return (abils.output || []).filter( function(e) { return e.ability.category == catName && !e.ability.hidden; }).map( function(e) { return e.ability } );
    }

    onInputChanged: {
        // refreshAB(); ... проблема только в том, что find abilities при нулевом входе выдают root. это правильно?...
        if (input) {
            refreshAB(); // если окошко будет тупить, вынести и в нулевой input этот вызов..
            ab.open();
        }
    }

    function refreshAB() {
        abils.input = deed.input;
        if (!abils.outputDeed) abils.perform();

        radioInputGroup.currentChanged();
        deedShowDeeds = false;
        cbDeedShowDeeds.checked = false;

        var q;
        q = cparams.children;
        while (q.length > 0) {
            var c = q[0];

            // нормальная ситуация - значит параметр удалили да и все
            //if (!c.paramOwnerRobot) { console.error("c.paramOwnerRobot is null!"); debugger; }
            if (c.paramOwnerRobot) c.parent = c.paramOwnerRobot;
            c.visible=false; // оно должно быть тутут. идет взаимосвязь с move-parametr
        }
        q = cparams2.children;
        while (q.length > 0) {
            var c = q[0];
            if (c.paramOwnerRobot) c.parent = c.paramOwnerRobot;
            c.visible=false;// оно должно быть тутут. идет взаимосвязь с move-parametr
        }

        if (robot && robot.params && robot.params.length > 0) {
            robot.params.map( function(e) {

                if (!e.paramOwnerRobot) {
                    e.paramOwnerRobot = e.parent;
                    e.oldSpaceParent = e.parent;
                }
                if (!e.parent) debugger;
                e.parent = e.secondColumn ? cparams2 : cparams;
                e.tag = null;
                e.visible = true;
            } );
            cparams.childrenChanged();
        }
        else {
        }
    }

    SimpleDialog {
        id: ab

        Text {
            id: tclose
            x: parent.width - width
            y: -15
            text: svernuto ? "    ++" : "     --"
            css.cursor: "pointer"
            z: 5
            //css.pointerEvents: "auto"
            font.pixelSize: 15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    svernuto = !svernuto;
                }
            }
        }

        property bool showExtraThings: showRobotsParam.checked

        property bool svernuto: false

        onSvernutoChanged: {
            if (svernuto) {
                //ab.x = qmlEngine.rootObject.width/2 - ab.width/2;
                var qq = qmlEngine.rootObject.rightWidgets.width;
                ab.x = qmlEngine.rootObject.width - ab.width - qq - 25;
                //ab.y = qmlEngine.rootObject.height;
                //console.log( "qmlEngine.rootObject.height = ",qmlEngine.rootObject.height);
            }
        }

        title: robot ? (isDeed ? "Программа \"" + robot.title + "\"" : (robot.title || wp.get_type( robot ))) : "/null"

        closeItem.visible: false

        visible: true
        height: svernuto ? 30 : col.realHeight+bottomarea.height+43
        //width: Math.max( cdeeds.width+25, Math.max( cparamsRow.width+25, 500 ) )
        width: 600

        z: 4999

        anchors.verticalCenter: svernuto ? qmlEngine.rootObject.height - 50 : 40 + height /2  //parent.verticalCenter - parent.height/4

        property var jurl: Qt.resolvedUrl("../../jquery-ui.js")
        Component.onCompleted: {
            //la_require( "http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" );
            la_require( jurl, function() {
                ab.titleText.css.cursor = "move";
                ab.titleText.dom.className += " handle-for-ur";
                jQuery( ab.dom ).draggable({ handle: ".handle-for-ur" });

                //jQuery( ab.dom ).resizable(); //{ handles: "se, s, e" });
            });
        }

        Row {
            y: -15
            x: parent.width - width - 30
            visible: !svernuto && robot && robot.paramsForSave && robot.paramsForSave.length > 0 && ttt.length > 0
            spacing: 3

            Text {
                y: 3
                text: "Шаблон:"
            }

            ComboBox {
              model: ["---"].concat( ttt.map( function (ti) { return ti.title || ti.name; }) )
              onCurrentIndexChanged: {
                if (currentIndex > 0) ttt[currentIndex-1].act( robot )
              }
              id: cbap
              property var ttt: presetLib.find_presets_for( robot )
              property var rr: robot
              onRrChanged: cbap.currentIndex = 0;
            }
        }

        onAfterClose: {
            cparams.children.map( function(c) {
                c.visible=false; // #move-parameter тут среагирует
            });
            cparams2.children.map( function(c) {
                c.visible=false; // #move-parameter тут среагирует
            });
        }

        onAfterOpen: svernuto = false;

        Column {
            spacing: 10
            id: col
            y: 5
            visible: !svernuto

            //css.maxHeight: "500px"
            //css.overflowY: "scroll"
            //css.overflowX: "hidden"
            css.pointerEvents: "all"
            width: 590

            function refit() {
                col.dom.style.maxHeight = maximumHeight + "px";
                //console.log("col.dom.style.maxHeight -> ",col.dom.style.maxHeight);

                if (height > maximumHeight) {
                    col.dom.style.overflowY = "scroll";
                    col.dom.style.overflowX = "hidden";

                    //console.log("turned on");
                }
                else {
                    col.dom.style.overflowY = "initial";
                    col.dom.style.overflowX = "initial";

                    //console.log("turned off");
                }
            }

            onHeightChanged: refit()

            property var maximumHeight: Math.floor( qmlEngine.rootObject.height ) - 150
            property var realHeight: Math.min( maximumHeight, height )

            onMaximumHeightChanged: {
                refit();
            }
            
            Text {
                text: robot && robot.robotDetails ? robot.robotDetails : ""
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignLeft
                width: 500
            }
            ///////////////////////////////////

            Row {
                spacing: 15
                id: cparamsRow
                Column { // сюда пойдут параметры
                    id: cparams
                    spacing: 5
                    visible: !isDeed || !deedShowDeeds
                    property var col2: cparams2
                }
                Column {
                  Text {
                    text: "Дополнительные возможности\n\n"
                    visible: cparams2.children.length > 0
                  }
                Column {
                    id: cparams2
                    spacing: 5
                }
                }
            }

            ///////////////////////////////////
            Text {
                text: "Возможные действия:"
                visible: cdeeds.visible && showAbilitiesColumn.height > 0
            }

            Row { // список возможных действий + выбранное действие
                id: cdeeds
                visible: !initHideDeeds || deedShowDeeds
                ////////////////////////////////////////////////////////////////////////////////
                Column { //  колонка показа доступных действий

                    width: 400
                    id: showAbilitiesColumn
                    spacing: 10

                    ExclusiveGroup {
                        id: radioInputGroup
                        onCurrentChanged: {
                            selectedAbilityName = current.abilityName;
                        }
                    }

                    Repeater { // категории
                        model: categories.length

                        Column { // очередная категория
                            id: cat
                            visible: {
                                if (catAbilities.length === 0) return false;
                                var cn = catName || "";
                                if (cn.indexOf("hidden") >= 0) return false;
                                if (cn === "system-deeds") return showCommonDeeds;
                                if (cn === "create-interface-o") return showCommonDeeds; // хехе трюк
                                return true;
                            }
                            //css.backgroundColor: "green"
                            //css.border: "1px solid gray"
                            //css.borderRadius: "3px"

                            Text {
                                text: tra( cat.catName ) + "......................"
                                //font.pointSize: 14
                            }

                            property var catName: categories[index]
                            property var catAbilities: getAbilitiesOfCategory(catName)

                            Grid { // действия текущей категории
                                columns: 2
                                //width: showAbilitiesColumn.width
                                Repeater {
                                    model: catAbilities.length
                                    RadioButton {
                                        width: 200 // 150
                                        text: (catAbilities[index].title || catAbilities[index].name)
                                        property var abilityName: catAbilities[index].name
                                        exclusiveGroup: radioInputGroup
                                        property var isCurrent: (radioInputGroup.current === that)
                                        id: that
                                        //font.underline: isCurrent
                                        onIsCurrentChanged: {
                                            that.dom.children[1].style.textDecoration = isCurrent ? "underline" : "none";
                                            that.dom.style.cursor = isCurrent ? "pointer" : "arrow";
                                        }
                                        onPreClicked: {
                                            if (isCurrent) {
                                                runSelectedDeed.perform();
                                            }
                                        }
                                    }
                                }
                            } // flow

                        } // категория
                    } // репитер категорий

                } // column возможных дейтсвий

                ////////////////////////////////////////////////////////////////////////////////
                Column { // выбранное действие
                    width: 200
                    Text {
                        //text: "Выбрано " + (selectedAbilityName ? selectedAbilityName : "-")
                        text: "Выбрано " + (selectedAbility ? selectedAbility.title : "-")
                        visible: showAbilitiesColumn.height > 0
                    }

                    Button {
                        visible: showAbilitiesColumn.height > 0
                        text: "Выполнить "// + runSelectedDeed.name
                        enabled: !!selectedAbilityName
                        onClicked: {
                            var d = runSelectedDeed.perform();
                        }
                    }

                    Text {
                        text: " "
                    }

                    Text {
                        text: selectedAbility.help || ""
                        width: 200
                        wrapMode: Text.WordWrap
                    }

                }

            }

        } // общая колонка

        Rectangle {
            id: bottomarea
            anchors.bottom: parent.bottom//+10
            width: parent.width
            height: Math.max( rr1.height, rr2.height )
            visible: showExtraThings && !svernuto
            //border.color: "black"

            Row {
                id: rr1
                spacing: 10

                Button {
                    text: "Готово"
                    width: 100
                    onClicked: svernuto = true; //ab.close();
                }

                BackButton {}

            }

            Row {
                id:rr2
                anchors.right: parent.right-10

                CheckBox {
                    z: 4
                    width: 100
                    visible: initHideDeeds && !svernuto && showExtraThings
                    text: "Действия | "
                    id: cbDeedShowDeeds
                    onCheckedChanged: if (deedShowDeeds != checked) deedShowDeeds = checked;
                }

                CheckBox {
                    z: 4
                    visible: initHideCommonDeeds && !svernuto && showExtraThings
                    width: 140
                    text: "Доп. действия |"
                    id: cbShowCommonDeeds
                    onCheckedChanged: if (showCommonDeeds != checked) showCommonDeeds = checked;
                }

                Text {
                    //anchors.right: parent.right
                    visible: !svernuto && showExtraThings
                    id: qq
                    z: 4
                    y: 3
                    //text: isDeed ? "Удалить действие" : "Удалить робота" //
                    text: " Удалить "
                    css.cursor: "pointer"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            deleteRobot.input = deed.input;
                            deleteRobot.perform();
                        }
                    }
                }
            }

        }



    } // dialog

    PerformDeedRec {
        name: selectedAbilityName
        object: deed.input
        id: runSelectedDeed
        manual: true
        parentToObject: true
        activateDlg: true
    }

    PerformDeedRec {
        name: "delete-robot"
        manual: true
        id: deleteRobot
    }

    // #superviewlang.chains
    PerformDeed {
        name: "save-deed"
        id: saveDeed
        object: deed.input
        manual: true
    }
}
