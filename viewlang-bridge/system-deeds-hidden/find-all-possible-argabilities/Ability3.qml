Ability {
  name: "find-all-possible-argabilities"
  category: "hidden"
  
  function feel( target_object_x ) {
    if (target_object_x) return 0;
    return 1;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}