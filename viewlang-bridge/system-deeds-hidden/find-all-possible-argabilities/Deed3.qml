import "../.."

Deed {
  id: deed

  function compute_object_depth( obj ) {
    var r = "";
    while (obj) {
      obj = obj.parent;
      r += ".";
    }
    return r.substr( 2 );
  }
  
  function go() {
      var objects = wonderfulplace.find_objects_in_qml_tree( underscene );
      var abilities = wonderfulplace.find_abilities( underscene );
      var acc = [];

      objects = [null].concat( objects );

      for (var i=0; i<objects.length; i++) 
      for (var j=0; j<abilities.length; j++)
      {
        var arg = abilities[j].feel( objects[i] );
        if ( !(arg > 0) ) continue;

        var rec = { input: objects[i], ability: abilities[j] };
        rec.inputName = compute_object_depth( rec.input ) + wonderfulplace.get_object_user_name( rec.input );
        rec.abilityName = rec.ability.name;
        rec.humanname = "object " + rec.inputName + " ability " + (rec.ability ? rec.ability.name : "-");

        acc.push( rec );
      }
      
      console.log("find-all-possible-argabilities found total = ",acc.length);
      
      //for (var k=0; k<acc.length; k++)
      //   console.log( "object ",wonderfulplace.get_object_user_name( acc[k].input ), " ability ",acc[k].ability.name );

      return acc;
  }

  Component.onCompleted: output = go(); // не binding потому что иначе рекурсия получается

}