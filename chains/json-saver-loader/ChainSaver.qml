
// вход: tree
// выход: output - текст, outputObj - объект

Robot {
  icon: "save"
  id: saver

  property var input

  params: [t,btn]
  TextEdit {
    id: t
    width: 400
    height: 300
  }
  Button {
    text: "refresh"
    onClicked: perform();
    id: btn
  }

  property var output: []
  property var outputObj: []

  onInputChanged: perform()

  property var paramValueLengthLimit

  function deftest( item, root, saver ) {
    //if (item == saver.parent) return false; а откуда у нас такой тест вообще взялся? не сохранять действие создаиня интерфейса..
    if (item.ability.name == "assign-params") return false; // не надо сохранять повторно их
    return (item.record || item == root);
  }

  function test( item, root, saver ) {
    return deftest( item, root, saver );
  }

  property string defaultTitle: ""

  function perform() {
    //console.log("ChainSaver: perform..");
    var root = input;
    var svr = saver;

    var res = {};
    res.title = defaultTitle;
    res.help = "";
    res.icon = "";

    res.chain = [];    
    //var limit = paramValueLengthLimit;

    if (root === qmlEngine.rootObject && root.cameraPos) {
      res.cameraPos = root.cameraPosReal;
      res.cameraCenter = root.cameraCenter;
      if (root.axes && root.axes.visible) {
        res.axesVisible = root.axes.visible;
      }
    }

    wp.qml_tree_walk( root, function(item) {
      if (item.type !== "Deed") { try_keep_params( res, item ); return true; }

      if (test( item, root, svr )) { // условие - что стоит флаг record или что это первое действие, выбранное для сохранения
        
        var record = {};
        record.ability = item.ability.name;
        record.input = wp.get_object_uniq_name( item.input );
        if (record.input == "/undefined" && item.input.toString) record.input = "js:"+item.input.toString();
        record.nameinchain = wp.get_object_uniq_name( item );
        if (item.robotLayer && item.robotLayer.length > 0) record.layer = item.robotLayer;

        if (!item.paramsSaveDisabled) {
          var q = recordParams( item.params, paramValueLengthLimit );
          if (q) record.initParams = q;
        }

        res.chain.push( record );
      }
      else if (item.params && item.params.length > 0) {
        try_keep_params( res, item );
      }
      return true;
    });
    // console.log("ChainSaver: res =",res);

    outputObj = res;
    output = obj2text( res );
    t.text = output;
  }

  function obj2text( obj ) {
    return JSON.stringify( obj,null,"  " );
  }

  // по заданному списку параметров формирует js-массив для записи их значений
  function recordParams( params,limit ) {
      var qq = (params || []).map( function(p) {
          if (p.saveDisabled) return null;
          var vv = wp.readParamValue( p );
          if (vv === undefined) return null;
          if (limit && vv && vv.toString) {
            var len = vv.toString().length;
            if (len > limit) {
              console.error("ChainSaver: param value length exceeds limit. p.guid=",p.guid,"len=",len,"limit=",limit);
              return null;
            }
          }
          return p.guid ? { guid: p.guid, value: vv } : null
        } ) .filter( function(r) { return !!r } );
     
     //console.log("ret qq=",qq);
     // return qq;

     if (!qq || qq.length == 0) return null;
     var simpleform = {};
     qq.forEach( function(p) { simpleform[ p.guid ] = p.value; } );
     return simpleform;
  }

  // попытаться создать действия восстановления параметров данного объекта
  // res - json куда сохраняются результаты
  // item - текущий объект
  // todo подумать, может сделать аспектное дерево аля цсс с параметрами. подумать над этим, что это по сути.
  function try_keep_params( res, item ) {
    if (!item.params || item.params.length == 0 || item.paramsSaveDisabled) return;
    // console.log( "try_keep_params item=",item );
//    console.log("item.compareWithInitParamsDuringSave=",item.compareWithInitParamsDuringSave);
//    if (!item.compareWithInitParamsDuringSave) return false;

    var record = {};
    record.ability = "assign-params";
    record.input = wp.get_object_uniq_name( item );
    record.nameinchain = wp.get_object_uniq_name( item ) + "/assign-params";    

    // выбираем параметры коии надо сохранить
    //  console.log("item.initParams=",item.initParams);
    //  console.log("item.params=",item.params);

    var pp = item.params.filter( function(p) {
      var v0 = p.valueToCompareOnSave;

      if (p.valueDiffers) {
        return p.valueDiffers( v0 );
      }

      //var vv = p.saveValue ? p.saveValue() : (p.valueToSave || p.value);
      var vv = wp.readParamValue( p );
      // тут идет сравнение параметров с тем что было раньше
      // по идее... надо наверное "глубокое" сравнение. http://stackoverflow.com/questions/13142968/deep-comparison-of-objects-arrays
      // но... пока забьем на это. ибо не охота сравнивать их методами массив из млн элементов..
      // сделали пока valueDiffers

      if (v0 != vv) return true;
      return false;
    } );   

    record.initParams = recordParams( pp,paramValueLengthLimit )
    //console.log("record.initParams=",record.initParams);

    if (!record.initParams) return false;

    //console.log("pushing record");
    //console.log("-------------------- try keep non null!", record.initParams );

    res.chain.push( record );
  }
}