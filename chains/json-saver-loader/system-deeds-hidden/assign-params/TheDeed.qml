Deed {
  id: deed
  record: false

  // а то любит ChainLoader/Deed выставлять true
  onRecordChanged: if (record) record = false;

  // переопределение функции объекта Deed
  function applyInitParams() {

    // блин.. а почему?.. 
    if (!deed.input) return;

    // передаем то что записано у нас в объект, которому предписано установить параметры
    // console.log("ASSIGN_PARAMS setting initParams=",initParams, "to robot=",deed.input,typeof(deed.input),"ok");
    deed.input.initParams = initParams;

    // AP не должен сохранять initParams в целевом роботе, т.к. это помешает созданию нового AP при сохранении
    // deed.input.applyInitParams( initParams );
    // тут может крыться маленькая засада. ибо initParams у действий и роботов у нас с памятью, и когда их список параметров или инпут меняется
    // то они переприсваюивают параметры. а теперь как бы нет, от AssignParams переприсвоения не пойдет, а зато пойдет от старой версии.
    // блин, а это уже плохо...
  }
}