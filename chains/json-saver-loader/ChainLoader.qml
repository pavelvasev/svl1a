/* Загружатель chain-действия из json-файла
  Вход: 
    * source - файл с описанием цепочки.
    * sourceText - текст с описанием цепочки.
    * sourceObj - js-объект, можно подавать вместо source но тогда надо указать еще output.name

  Если указываются sourceText или sourceObj то еще надо вручную указать output.name - имя для возможности

  Выход: 
    * output - ChainAbility, готовая выполнить цепочку

  Формат текста-файла определяется отдельно..
*/
Item {
  id: rb

  property alias file: rb.source 
  property var source: ""

  //////////////////////////////////////////////////////////////////

  JsonLoader {
    file: source
    id: jsl
    onFailed: rb.loadFailed();
    //onOutputChanged: sourceObj = output
  }

  onSourceChanged: {    
    var ff = (source.name || source);
    if (!ff.split) return;

    var c = ff.split("/");
    var fn = c[ c.length-1 ];
    desiredAbilityName = fn.split(".")[0];
  }

  ///////////////////////////////////////////////////////////

  property var sourceText: ""
  onSourceTextChanged: {
//    try {
      sourceObj = text2obj( sourceText );
//    } catch( err ) {
//      console.log( "ChainLoader::sourceText parse error: ",err );
//    }
  }

  function text2obj( text ) {
    return JSON.parse( text );
  }

  ///////////////////////////////////////////////////////////
  property var sourceObj: jsl.output

  onSourceObjChanged: {
    if (!sourceObj) return;
    var so = sourceObj;
    for (var i in so) {
      //console.log("ab[i] = so[i];. i=",i,"so[i]=",so[i]);
      if (ab.historyMode) {
        if (i === "cameraPos") {
          //console.log(">>>>>>>>>>>>>>>>>>> qmlEngine.rootObject.cameraPos = ",so[i]);
          qmlEngine.rootObject.cameraPos = so[i];
        }
        else if (i === "cameraCenter") 
        {
          //console.log(">>>>>>>>>>>>>>>>>>> qmlEngine.rootObject.cameraCenter = ",so[i]);
          qmlEngine.rootObject.cameraCenter = so[i];
        }
        else if (i === "axesVisible") 
        {
          qmlEngine.rootObject.axes.visible = so[i];
        }
      }

      ab[i] = so[i];
    }
  }

  property var desiredAbilityName
  ChainAbility {
    id: ab
    name: desiredAbilityName
    //compareWithInitParamsDuringSave: rb.compareWithInitParamsDuringSave
  }

  property bool ready: !!ab.baseab || (ab.historyMode && ab.chain.length > 0)
  property alias output: ab
  //property bool  compareWithInitParamsDuringSave: false

  signal loadFailed();  
}
