/*
  Способность вызова других способностей по цепочке.

  Вход:
    * chain - массив записей о цепочках в формате ability/input/nameinchain

  Алгоритм:
    1. Выявить первое действие в цепочке. Оно будет основным, остальные дополнительными.
    2. Использовать основное действие для feel
    3. Act вызывает воспроизведение действий в цепочке, начиная головного

*/
Ability {
  id: ab

  //////////////////////////////////////////////////////////////////
  property var basename // имя первой возможности в цепочке
  property var baseab   // первая возможность в цепочке - нужна для feel

  property var chain: [] // массив записей о событиях в цепочке

  onChainChanged: {
    //console.log("ChainAbility: chainchanged... name=",name,"chain=",chain);
    
    if (chain.length == 0) {
      ab.basename = null;
      ab.baseab = null;
    }
    else {
      ab.basename = chain[0].ability;
      loadBaseAb();
    }
  }

  function loadBaseAb() {     
     if (historyMode) {
       baseab = null;
       // вроде и не надо.. хм.. wp.abilityLoaded( ab.name );
       return;
     }

     var all_abilities = wp.find_abilities();
     var appropriate_abilities = wp.filter_objects_by_qml_name( all_abilities, ab.basename );

     if (appropriate_abilities.length == 0) {       
       console.log("ChainAbility: first ability not found, waiting to be loaded.. ab.basename=", ab.basename);
       wp.abilityLoaded.connect( ab, wpAbilityLoaded );
     }
     else {
       baseab = appropriate_abilities[0];
       //console.log("ChainAbility: first ability found and =",baseab);
       //console.log("xxx ab.name=",ab.name,"category=",category );
       if (!historyMode && !category) {
          if (!baseab.category) {
            // ситуация, когда там тоже цепочечное действие, и оно еще не загрузилось
            if (baseab.chain && !baseab.baseab) {
              baseab = null;
              wp.abilityLoaded.connect( ab, wpAbilityLoaded );
              return;
            }
          }

          //console.log(" -> setting to baseab.category=",baseab.category);
          //console.log( "... btw baseab.name=",baseab.name, baseab );
          category = baseab.category;
       }

       // важно - уведомляем мир что мы изменились. тогда те кто ждет это действие, возможно, сможет его использовать
       wp.abilityLoaded( ab.name );
     }
  }

  function wpAbilityLoaded( name ) {
    //console.log("ChainAbility: wpAbilityLoaded..",name, "ab.basename=",ab.basename);
    if (name == ab.basename) {
      wp.abilityLoaded.disconnect( ab, wpAbilityLoaded );
      //console.log("ChainAbility: ability loaded, performing..",name);
      loadBaseAb();
    }
  }

  //////////////////////////////////////////////////////////////////

  function feel( obj ) {
    //console.log("ChainAbility: feel.. name=",name,"baseab=",baseab);
    if (historyMode) return 1;

    if (!baseab) return 0;
    //if (historyMode) return (wp.get_type(obj) == "Scene");
    var baseres =  baseab.feel( obj );

    if (baseres > 0 && extraFeel) {
      return eval(extraFeel );
    }

    return baseres;
  }

  deedPath: Qt.resolvedUrl("ChainDeed.qml")

  //////////////////////////////////////////////////////////////////

  property bool historyMode: false // вляет на то, куда цеплять 1 действие цепочки в качестве дитя. Если false то к самому действию chaindeed. Если true то к объекту применения.

  // доп проверка для json-возможностей. пример --   "extraFeel" : "return obj && obj.count2 == 6 ? 1 : 0",
  property var extraFeel

  property var loadedGood: historyMode ? historyMode : !!baseab
}