// Создание цепочки действий
// Вход: ability.chain - массив с записью о действиях, которые необходимо совершить

// Выход: сигнал finished()

Deed {
  id: deed
  //property bool risClosed: true

  property bool paramsSaveDisabled: true // на случай если нам интерфейс параметров напихает

  Component.onCompleted: {
    var cloned_chain = clone( ability.chain ); // важно склонировать, чтобы править initParams имена объектов
    go( cloned_chain, 0, deed.input );
  }

  // массив переименований. его можно не обнулять, т.к. Deed создается каждый раз
  property var renamesTable: []

  property bool thisIsChainDeed: true

  // переименовывает строку str сообразно заменам renamesTable
  // это нам нужно для того, чтобы при изменении идентификатора в replay переименовать ссылки в дальнеших действиях

  /* переименование
     1 заменяем только началЫ строк
     2 но не совсем. если сделалась замена, то сдвигаем указатель и далее замененную часть уже не трогаем, а меняем то что за нею
     3 может так надо сделать несколько раз, пока делается? (но не применять повторные замены)
  */
  function rename( str ) {
//    console.log(">>>>>>>>>>> rename: input str=",str );
    var rt = renamesTable;
    var st = 0; // размер уже замененной строки. ее не трогаем. почему-то.

    for (var i=0; i<rt.length; i++) {
      // сравниваем только с начала строки
      var startchars = str.substring( st, st + rt[i][0].length );
      if (startchars == rt[i][0] ) {
        str = str.substring( 0, st ) + rt[i][1] + str.substring( st + rt[i][0].length );
        st = st + rt[i][1].length;
      }
      //str = str.replace( rt[i][0], rt[i][1] );
    }
//    console.log(">>>>>>>>>>> rename: output str=",str );
    return str;
  }

  signal finished();

  // но вообще тут нет учета что может перезапуск будет..? 
  // ну ладно пока. нам надо это все для окна показа на старте #gui-okna-nastroit 
  property bool isFinished: false
  onFinished: isFinished = true; 

  property int retryCounter:0

  property bool interfaceDetected: false

  // выполнить i-е действие
  function go(arr, i, target_object_x, nonext ) {
      if (i >= arr.length) 
      {
        var pendingDeeds = arr.filter( function(rec) { return !!rec.pendingTarget } );
        console.log("ChainDeed: chain ended.. pendingDeeds=",pendingDeeds, "arr.length=",arr.length);
        if (pendingDeeds.length > 0) {
          console.log("renamesTable=",renamesTable);
          // фиг знает почему но таймер не срабатывает
          // restartChain.newchain = pendingDeeds;
          //console.log("............starting timer.......",restartChain);
          //restartChain.start();
          retryCounter = retryCounter+1;

          var inputname = wp.get_object_user_name( deed.input );
          pending.add( deed.ability.name + " -> " + inputname,"perform");

          setTimeout( function() { go( pendingDeeds, 0 ); }, 250*retryCounter );
        }
        else {
          var inputname = wp.get_object_user_name( deed.input );
          pending.add( deed.ability.name + " -> " + inputname);

          setTimeout( function() { finished(); }, 10 );
        }
        return;
      }

      var line = arr[i];

      var name = line.ability;
      var targetName = rename( line.input );

      // условие (i == 0 && target_object_x) === если выполняется 1е действие цепочки, то надо брать входной объект, а не тот что указан в цепочке.
      var targetToChainInput = (i == 0 && target_object_x);
      var target = targetToChainInput ? target_object_x : wp.find_by_uniq_name( targetName );
      //console.log("targetName =",targetName ,"target=",target);

      if (targetToChainInput && target) {
        var myname = wp.get_object_uniq_name( target );
        if (targetName != myname) {
          var rline = [targetName, myname];
          console.log("renamesTable add line 0: ",rline);
          renamesTable.push( rline );
          targetName = myname;
        }
      }

      // а js нам нужен, ибо мы задумали неким действиям input сразу жс-объекты подавать. но пока что это ток строки
      if (!target && /^js:/.test(targetName)) target = targetName.substring(3);

      // если мы после чтения line.input прочитали null, и мы не в режиме работы с input-ом действия, то значит target это вся сцена..
      // это надо затем, что иначе объекты будут создаваться как дети непонятно чего.
      // хотя может это надо не здесь отслеживать а глубже
      if (!targetToChainInput && !target && (!targetName || targetName=="null")) target = qmlEngine.rootObject;
      
      var oldName = rename( line.nameinchain );
      //// воооот.
      //console.log("ChainDeed: perform deed ",name," on renamed targetName=",targetName,"old name=",line.input,"obj=",target);

      if (!target) {
//        console.log("ChainDeed: the target object not found. Skipping to next deed in chain. targetName=",targetName);
        // console.log("ChainDeed: renamesTable=",renamesTable);
        // console.log("ChainDeed: scheduling waitObj timer..");

        line.pendingTarget = true;
        return go( arr, i+1 );
      }

      // ищем ссылки на объекты в initParams, заодно переименовываем их
      if (processObjectRefs( line.initParams ) > 0) {
          if (retryCounter < 10) { // если много раз не получилось - забить
            line.pendingTarget = true;
            return go( arr, i+1 );
          }
      }

      line.pendingTarget = false;
      
      thisdeed.object = target;
      thisdeed.name = name;
      thisdeed.nextIndex = nonext ? 0 : i+1;
      thisdeed.outputDeed = null; // надо
      thisdeed.oldName = oldName;
      thisdeed.desiredLayer = line.layer;

      if (line.initParams && deed.ability.historyMode) line.initParams.historyMode = true; // прокинем туды этот признак
      
      thisdeed.options = { initParams: line.initParams, performedByChainDeed: deed };

      thisdeed.thechain = arr;

      //console.log("chaindeed performing ",name,"with line.initParams=",line.initParams);
      thisdeed.perform();
  }

  property var objref_processed: { return {} }

  function processObjectRefs( jsobj ) {
    var failcount = 0;

    if (jsobj && typeof(jsobj) == "object") {
      if (jsobj.objref && !objref_processed[ jsobj.objref ] && jsobj.objref !== "null") { // ну конечно это треш, хранить в общем массиве.. но может это и сработает
        var renamed = rename( jsobj.objref );
        //console.log("jsobj.objref=",jsobj.objref,"renamed=",renamed);

        var t2 = wp.find_by_uniq_name( renamed );
        if (t2) {
          // итак, объект по ссылке находится. Считаем, что все ок, и не надо будет больше переименовывать
          jsobj.objref = renamed;
          objref_processed[ jsobj.objref ] = 1;
          //console.log("resolved jsobj.objref=",jsobj.objref);
        }
        else {
          console.error("processObjectRefs failed on object: ",renamed);
          failcount = failcount + 1;
        }
      }
    }

    if (typeof(jsobj) == "object" || typeof(jsobj) == "array") {
      for (var key in jsobj) {
        var val = jsobj[key];
        failcount = failcount + processObjectRefs( val ); 
      }      
    }

    return failcount;
  }

  property var extraChildren: []

  PerformDeed {
    id: thisdeed
    
    manual: true
    parentToObject: deed.ability.historyMode
    // parentToObject: deed.ability.historyMode ? true : (nextIndex>1) // это правильно, не >= а именно > т.к. речь о последующем событии

    property var nextIndex: 0
    property var oldName
    property var thechain

    property var desiredLayer

    onPerformedGood: {
       //@ createdDeeds.add( outputDeed )

       if (!parentToObject) deed.extraChildren.push( outputDeed ); // надо для корректного поиска в удаленных мирах (когда этот мир - удаленный)

       parentToObject = true; // это для последующих действий
       //console.log("chain ab performed good!!!!!!!!!!!!!! deed.ability.name=",deed.ability.name);
       outputDeed.record = deed.ability.historyMode ? true : false;

       if (desiredLayer) outputDeed.robotLayer = desiredLayer;
       // console.log("record = ",outputDeed.record,"deed.ability.name=",deed.ability.name);
       //debugger;

       var myname = wp.get_object_uniq_name( outputDeed );
       if (oldName != myname) {
         var rline = [oldName, myname];
         console.log("renamesTable add line: ",rline);
         renamesTable.push( rline );
       }

       if (outputDeed.ability.name.indexOf("create-interface")==0 && !interfaceDetected) {
         //console.log("$$$$$$$$$$$$$$$$$$ iface add...",outputDeed.interface.params);
         interfaceDetected = true;
         deed.params = outputDeed.interface.params; // тут кстати происходит splice
         outputDeed.interface.paramsChanged.connect( deed, function(x) { 
           //console.log("$$$$$$$$$$$$$$$$$$ iface update...",deed,x);
           deed.params=x;
         });
       }

       if (nextIndex > 0)
         go( thechain, nextIndex );
    }

    retry: false
    onPerformedFail: {
       parentToObject = true;
       //console.log("chain ab performed failed!!!!!!!!!!!!!! deed.ability.name=",deed.ability.name);
       // ждем.. все условия выявлены уже до нас.. (по идее). ну или retry сделать поле в PerformDeed и выставить его в false для этого действия
       if (nextIndex > 0) {
         thechain[nextIndex-1].pendingTarget = true;
         go( thechain, nextIndex );
       }

    }
  }

  //@ property var createdDeeds: QmlItems {} // вроде как нафиг и не надо 

  ////////////////////////////////////////
  function clone(item) {
    if (!item) { return item; } // null, undefined values check

    var types = [ Number, String, Boolean ], 
        result;

    // normalizing primitives if someone did new String('aaa'), or new Number('444');
    types.forEach(function(type) {
        if (item instanceof type) {
            result = type( item );
        }
    });

    if (typeof result == "undefined") {
        if (Object.prototype.toString.call( item ) === "[object Array]") {
            result = [];
            item.forEach(function(child, index, array) { 
                result[index] = clone( child );
            });
        } else if (typeof item == "object") {
            // testing that this is DOM
            if (item.nodeType && typeof item.cloneNode == "function") {
                var result = item.cloneNode( true );    
            } else if (!item.prototype) { // check that this is a literal
                if (item instanceof Date) {
                    result = new Date(item);
                } else {
                    // it is an object literal
                    result = {};
                    for (var i in item) {
                        result[i] = clone( item[i] );
                    }
                }
            } else {
                // depending what you would like here,
                // just keep the reference, or create new object
                if (false && item.constructor) {
                    // would not advice to do that, reason? Read below
                    result = new item.constructor();
                } else {
                    result = item;
                }
            }
        } else {
            result = item;
        }
    }

    return result;
  }

}