// Вход  in1 - 1 мерный массив объектов, каждый объект содержит поля .text, .istitle, .key
// Выход chosen - массив объектов из in1, выбранных пользователем
// косвенный вход/выход - tablica бо key -> {true|false|undefined}

Column {
  id: cc
  width: 500

  /*
  css.maxHeight: "500px"
  css.overflowY: "scroll"
  css.overflowX: "hidden"
  css.pointerEvents: "all"
  */

  property var in1: []
  property var chosen: []
  property alias output: cc.chosen

  property var tablica: { return {} }

  property bool defaultChecked: false

  Repeater {
    model: in1.length
    id: rep
    Column {
      width: parent.width
    Text {
      text: in1[index].text
      visible: !!in1[index].istitle
    }
    CheckBox {
      visible: !in1[index].istitle
      id: cb
      width: parent.width
      text: cc.f( in1[index] )
      checked: {
        var v = tablica[ in1[index].key || index ];
        if (v) return true;
        if (typeof(v) === "undefined") {
          var v0 = in1[index].initialCheck;
          if (typeof(v0) === "undefined") return defaultChecked;
          return v0;
        }
        return false;
      }
      onCheckedChanged: {
        tablica[ in1[index].key || index ] = checked;
        cc.updateOutput();
      }
    }
    }
  }

  function updateOutput() {
    var m = in1.length;
    var r = [];
    in1.map( function(item,index) {
      if (tablica[ item.key || index ]) r.push(item);
    });
    chosen = r;
  }

  function f(x) {
    return x.text;
  }
}