Item {
    id: part
    property var input: []
    signal getInput();

    width: tv1.width
    height: tv1.height

    TabView {
        width: 500
        height: Math.min( 250, Math.max( c1.height, c2.height )+30 )
        id: tv1

        Tab {
            title: "Выбор"

            css.maxHeight: "350px"
            css.overflowY: "scroll"
            css.overflowX: "hidden"
            css.pointerEvents: "all"

            Choser {
              in1: part.input
              id: c1
            }
        }
        Tab {
            title: "Заголовки"
            PostroitFxTabl {
                in1: c1.output
                id: c2
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// Сохраняем-загружаем
    Item {
        id: ca
        property var guid: "chosen"
        property var valueToSave: c1.output.map( function(x,index) { return { objref: x.objref, guid: x.guid, title: c2.output[index] } })

        property var valueToLoad
        onValueToLoadChanged: {
            if (input.length == 0) getInput();

            var tabl = {};
            var tabl2 = {};
            valueToLoad.map( function(rec) {
                tabl[ rec.guid + "@" + rec.objref ] = 1;
                tabl2[ rec.guid + "@" + rec.objref ] = rec.title;
            });
            c1.tablica = tabl;
            c2.tablica = tabl2;
        }
    }
    property alias param: ca
    property alias cc1: c1
    property alias cc2: c2
}