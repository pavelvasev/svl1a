// Задает бо объект - текст
// вход in1 массив объектов с полями text, key
// выход output массив введенных текстов, соответственно объектов

Grid {
  id: cc
  width: 500
  spacing: 10
  columns: 2

  property var in1: []
  property var output: []

  property var tablica: { return {} }

  Text { text: "Выберите элемент" }
  Text { text: "Укажите значение" }

  ComboBox {
    model: in1.map( function(f) { return f.text } );
    size: 10
    height: 100
    width: 200
    onCurrentIndexChanged: refreshTi();
    id: cb
  }

  function refreshTi()
  {
    var item = in1[ cb.currentIndex ];
    if (item)
        ti1.text = ff( item );
  }

  Column {
    spacing: 5
    visible: !!in1[ cb.currentIndex ]
    TextInput {
      id: ti1
      width: 200
    }
    Button {
      text: "Ввод"
      width: 80
      onClicked: {
        tablica[ in1[ cb.currentIndex ].key ] = ti1.text;        
        cc.tablicaChanged();
      }
    }
  }

  function f(x) {
    //console.log("-----> x=",x,"tablica[ x.key ]=",tablica[ x.key ]);
    return tablica[ x.key ];
  }

  function ff(item) {
    return tablica[ item.key ] || item.title || "";
  }

  function updateOutput() {
    var m = in1.length;
    var r = [];
    in1.map( function(item,index) {
      r.push( ff(item) );
    });
    output = r;
  }

  onIn1Changed: updateOutput();
  onTablicaChanged: updateOutput();
}