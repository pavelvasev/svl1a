// см doc/2016-06-23-interface.md 
Deed {
    id: deed

    TabView {
        id: ttv
        width: 600
        //height: Math.min( 300, Math.min( part1.height, part2.height )+30 )
        height: Math.max( part1.height, part2.height )+30

        Tab {
            title: "Параметры роботов"
            Part {
                id: part1
                param.guid: "chosen"
                onGetInput: prep()
            }
        }
        Tab {
            title: "Действия"
            Part2 {
                id: part2
                param.guid: "deeds"
                onGetInput: prep()
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////// выбираем

    function prepare_params( root ) {
        var res = [];
        wp.qml_tree_walk( root, function(item) {
            if (item == deed || item == iface || item.type == "Interface") return;
            var pp = (item.params || []).filter( function(p) { return !!p.guid; });
            if (pp.length == 0) return;

            var t = (item.$class == "Deed" && item.ability ? item.ability.title : wp.get_type( item ) );
            var g = wp.get_object_user_name( item );
            res.push( { istitle: true, text: "<b>"+t+"</b>" } );

            pp.forEach( function(p) {
                res.push( { text: (p.text || p.guid), key: p.guid+"@"+g, guid: p.guid, objref: g, param: p } );
                // ссылку на параметр надо хранить чтобы ее указать в Interface
            });
        });
        return res;
    }

    function prepare_deeds( root ) {
        var res = [];
        wp.qml_tree_walk( root, function(item) {
            if (item == deed || item == iface || item.type == "Interface" || item.$class == "Deed" || item.$class == "Ability") { return; }
            if (!item.is_wonder_item && !item.visual && !item.positions) { return; };
            var q = wp.get_object_user_name(item);
            //var t = (item.$class == "Deed" && item.ability ? item.ability.title : wp.get_type( item ) );
            res.push( { text: q, item: item } );
        });
        return res;
    }

    // для Part2 /choser2
    function getsubitems( x ) {
        if (!x) return [];
        var item = x.item;

        if (pdd.outputDeed)
            pdd.outputDeed.input = item;
        else pdd.input = item;

        if (!pdd.output) return;

        var abils = pdd.output.filter(function(f) { return f.ability.category != "system-deeds" && f.ability.category.indexOf("hidden")==-1; } ); // убрали все системные
        if (abils.length == 0) { return; };

        var t = (item.$class == "Deed" && item.ability ? item.ability.title : wp.get_type( item ) );
        var g = wp.get_object_user_name( item );

        var res=[];
        abils.forEach( function(p) {
            res.push( { text: (p.ability.title || p.ability.name), key: p.ability.name+"@"+g, guid: p.ability.name, objref: g } );
        });
        return res;
    }

    PerformDeed {
        name: "find-abilities-for-object"
        id: pdd
        input: pdd
    }

    // поиск

    Component.onCompleted: if (part1.input.length == 0) prep(); // if затем, что findo может вызываться в initParams, а операция дорогая..

    function prep() {
        part1.input = prepare_params( deed.input );
        part2.input = prepare_deeds( deed.input );
    }

    Button {
        id: b1
        text: "Обновить списки роботов"
        width: 240
        onClicked: prep();
    }

    params: [ttv, part1.param, part2.param,b1]
    //details: "найдено параметров="+ito.length

    ///////////////////////////////////////////////////////////////////////////////////
    ////////////////////////// Порождаем интерфейс

    // генерируем действия
    Column {
      spacing: 5

      Text { 
        text: part2.cc1.output.length > 0 ? "Доп. возможности" : ""
      }

      Repeater {
        model: part2.cc1.output.length
        TextButton {
          text: "<u>"+part2.cc2.ff( part2.cc1.output[index] )+"</u>"
          //color: "blue"

          onClicked: {
            if (pd.outputDeed) {
              risovanie.activate( pd.outputDeed );
            }
            else {
              pd.input = wp.find_by_uniq_name( (part2.cc1.output[index] || "").objref );
              pd.perform();
            }
          }
          PerformDeed {
            name: (part2.cc1.output[index] || "").guid          
            //tag: ""
            //input: wp.find_by_uniq_name( (part2.cc1.output[index] || "").objref )
            parentToObject: true
            record: true
            once: true
            manual: true
            id: pd
            activateDlg: true
          }
        }

      }
      visible: false
      id: cdeeds
      anchors.right: parent.right

      onVisibleChanged: {
        if (visible && cdeeds.parent.col2) cdeeds.parent = cdeeds.parent.col2; // #show-robot-window 
      }
    }

    // генерируем заголовки интерфейса
    Item {
        Repeater {
            model: part1.cc2.output.length
            id: titrep
            Text {
                text: part1.cc2.output[index] ? "<b>"+part1.cc2.output[index]+"</b>" : ""
                //visible: false
            }
        }
    }


    property var chosenParams: genChosen()

    function genChosen() {
        var p = [];
        var chosen = part1.cc1.output;
        for (var v in chosen) {
            if (titrep.count > v) {
                var titl = titrep.itemAt(v);
                p.push( titl ); // заголовок
            }
            var vv = chosen[v];
            p.push( vv.param )
        };
        p.push( cdeeds );
        return p;
    }

    property var interface: iface
    Robot {
        icon: "интерфейс"
        type: "Interface"
        params: chosenParams
        id: iface
        property bool guiHideDeeds: true
        property bool saveDisabled: true // интерфейс не сохраняет параметры, они сохраняются в исходных объектах
    }
}
