import ".."

Deed {
    id: deed
    icon: "[L]"

    property var robot: deed.input

    SimpleDialog {
        title: "Загружаем сцену"
        visible: true
        width: col.width + 50
        height: col.height + 50
        id: dlg

        onAfterClose: deed.destroy();

        Column {
            spacing: 10
            id: col

            Text {
                text: "Возьмите содержание сцены из надежного места в формате SVL-JSON и вставьте в окно. Затем нажмите кнопку Загрузить."
                width: 500
                wrapMode: Text.WordWrap
            }

            TextEdit {
                id: scenecode
                width: 500
                height: 200
            }

            Button {
                text: "Загрузить!"
                onClicked: loadfromtext();
            }

            Text {
                id: opnote
                width: 400
                height: 30
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignLeft
            }


        }
    }

    ///////////////////////////////////
    ChainLoader {
        id: clText
        output.name: "load-scene-from-text"
        output.historyMode: true
        output.category: "hidden"
        //output.parent: qmlEngine.rootObject
        // парента что-ли поменять..

        onReadyChanged: {
            if (clText.ready) {
                console.log("SceneLoadFromUrl.qml: chain loader ability ready, acting.");
                //deed.finished.connect( deed, function() { dlg.close(); } );
                clText.output.act();
                // проблема если сразу закрываем диалог то у нас стоит deed.destroy() и все удаляется, а действия может еще не создались
                // сделали deed.finished() но он может быстро сработать
                // значит надо еще finishedCount делать но это уже громождение, поэтому пока просто не закрываем диалог..
                //dlg.close();
            }
        }
    }

    function loadfromtext() {
        try {
            ho = JSON.parse( scenecode.text );
            opnote.text = "Результат: текст распарсен хорошо, загружаю.."
            clText.sourceObj = ho;
            window.svlProtectFromExit = false;
        } catch(err) {
            console.error( err );
            opnote.text = "Результат: ошибка разбора JSON из окошка. См лог.";
        }
    }
}