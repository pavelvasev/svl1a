Deed {
    id: deed
    icon: "ld"

    Text {
        text: "Выберите архив:"
        id: tintro
    }

    ComboBox {
        model: ["http://localhost:3000/", "http://viewlang.ru:3000/"]
        currentIndex: (window.location.href.indexOf("file://") >= 0 ? 0 : 1)
        id: cb
        property var value: currentText
        property var guid: "acrhiveurl"
        width: 180
    }

    GetFiles {
        width: 600
        id: fsa
        noderoot: cb.value

        Column {
            anchors.top: parent.top
            anchors.right: parent.right
            spacing: 5
            Text {
                text: "Другие варианты"
            }

            PerformDeedButton {
                text: "Файлы и URL"
                name: "load-deed-url"
                input: qmlEngine.rootObject
                record: true
                tag: null
                parentToObject: true
                onPerformedGood: {
                    risovanie.activate( outputDeed );
                }
            }
            PerformDeedButton {
                text: "Текст"
                name: "load-deed-text"
                input: qmlEngine.rootObject
                record: true
                tag: null
                parentToObject: true
                onPerformedGood: {
                    risovanie.activate( outputDeed );
                }
            }
        }
    }

    params: [tintro,cb,fsa]

}
