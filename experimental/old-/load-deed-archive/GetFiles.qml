Column {
  id: co

  property var noderoot: "http://localhost:3000/"

  TextLoader {
    id: tl
    file: noderoot + "lsd?filter=\.abc\?\.json&dir="
    onOutputChanged: packs = output.split("\n").filter( function(f) { return f.length > 0 });
  }

  property var packs: []
  property var value: { return {} }
  property var guid: "checked_packs"

  Button {
    text: "Обновить"
    onClicked: { tl.enabled=false; tl.enabled=true; }
  }

  Text {
    text: " "
  }
  
  Text {
    text: "Выберите пакеты:"
  }

  Repeater {
    model: packs.length

    Column {
      width: co.width
      CheckBox {
        id: cb
        text: packs[ index ]
        checked: co.value[ packs[ index ] ]
        onCheckedChanged: co.value[ packs[ index ] ] = checked;
      }

      TextLoader {
        file: cb.checked ? (noderoot + "ls?filter=\.abc\?\.json&dir="+packs[ index ]) : null
        id: tx
      }
      property var chainsUrl: tx.output.split("\n").filter( function(e) { return e.length > 0 } )

      Repeater {
        model: chainsUrl.length
        Text {
          //text: "<a target='_blank' href='"+urla+"'>"+chainsUrl[index]+"</a>"
          text: "<a target='_blank' href='"+urla+"'>" + (cl.output.title || chainsUrl[index])+"</a>" + (cl.ready ? " ok" : " ..чего-то не хватает..")
          property var urla: noderoot + chainsUrl[index]
          ChainLoader {
            id: cl
            file: parent.urla
            output.category: cb.text
            //compareWithInitParamsDuringSave: true
          }
        }
      } // repeater for chain urls for this pack
    } // col for pack
  }
 
}