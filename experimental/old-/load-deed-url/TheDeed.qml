Deed {
  id: deed
  icon: "ld-url"

  FileParam {
    id: fs
    multiple: true
    guid: "files"
    text: "Укажите файлы действий"
    tabview.currentIndex: 1
  }

  Text {
    id: t0
    text: "Умеем загружать действия в формате: abilities.txt, ab.json, Ability.qml"
  }

/*
  Text {
    id: t1
    text: {
      var res = "Загружено действий, штук: "+fs.files.length;
      
      for (var i=0; i<rep.model; i++) {
        if (!rep.itemAt(i)) continue;
        res += " " + rep.itemAt(i).output.title;
      }
      return res;
    }
  }
*/
  params: [fs,t0]

  property var fileext: ".abc.json"

  PerformDeed {
    name: "load-ability"
    input: fs.files
    once: true
    // options: [{robotLayer:"system"}]
    // следующее сделано чтобы они не сразу кидались загружаться
  }
}