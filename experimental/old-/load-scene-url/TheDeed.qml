Deed {
    id: deed
    icon: "L"

    Column {
        id: coco
        Text {
          text: "<a href='http://www.svn.lact.ru:4567/files/svl_scenes/' target='_blank'>См. архив сцен www.svn.lact.ru:4567/files/svl_scenes/</a>\n\n"
        }

        Text {
            text: "Укажите URL сцены:"
        }

        TextInput {
            id: ti
            width: 400
            Component.onCompleted: ti.dom.firstChild.name="qurla";
        }

        Button {
            text: "Загрузить"
            onClicked: go();
        }

        Text {
          id: status
        }
    }
    params: [coco]

    ChainLoader {
        id: cl
        output.historyMode: true
        output.category: "hidden"
        onReadyChanged: {
            if (cl.ready) {
                status.text = "Выполняю..";
                var out = cl.output.act();
                if (out) out.finished.connect( deed,fin);
            }
        }
        onLoadFailed: status.text = "Ошибка чтения-загрузки, см лог";
    }

    function go() {
        if (ti.text) {
          status.text = "Читаю файл сцены..";
          cl.file = ti.text;
        }
    }

    function fin() {
      status.text = "Готово"
    }
}
