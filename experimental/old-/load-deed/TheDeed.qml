Deed {
    id: deed

    SimpleDialog {
        title: "Откуда взять действия?"
        visible: true
        width: col.width + 50
        height: col.height + 50
        anchors.verticalCenter: 40 + height /2  
        id: dlg

        onAfterClose: deed.destroy();

        Row {
            id: col
            spacing: 5

            PerformDeedButton {
                text: "Архив"

                name: "load-deed-archive"
                input: qmlEngine.rootObject
                record: true
                tag: null
                parentToObject: true
                onPerformedGood: {
                    risovanie.activate( outputDeed );
                    dlg.close();
                }
            }
            PerformDeedButton {
                text: "Файлы и URL"
                name: "load-deed-url"
                input: qmlEngine.rootObject
                record: true
                tag: null
                parentToObject: true
                onPerformedGood: {
                    risovanie.activate( outputDeed );
                    dlg.close();
                }
            }
            PerformDeedButton {
                text: "Текст"
                name: "load-deed-text"
                input: qmlEngine.rootObject
                record: true
                tag: null
                parentToObject: true
                onPerformedGood: {
                    risovanie.activate( outputDeed );
                    dlg.close();
                }
            }
        }
    }

}
