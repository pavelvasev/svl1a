// Робот "Результаты вопроса"

Robot {
  id: rr

  icon: "ответ"

  ComboBox {
    id: c1
    size: 3
    height: 150
    width: 400
    model: results
  }

  params: [c1]

  // критерий из которого породились
  property var k: []
  title: k.name

  // результаты
  property var results: []
  // доп критерии
  property var additional_ks: []

  // построенные объекты возможностей из доп критериев
  property var additional_abilities
  Repeater {
    model: additional_ks.length
    
    QuestionAbility {
      k: additional_ks[index]
      targetObj: rr
    }
  }

  property var server
}