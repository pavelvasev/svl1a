// Представитель нодовского сервера node1

ServerRobot {

  function ask( k,res ) {
    var dir = (k.value||"");
    loadFile( "http://localhost:3000/ls2?dir="+dir, function (data) {
      
      var lines = data.split("\n");
      for (var i in lines) {
        var line = lines[i];
        if (line.length == 0) continue;
        
        var cc = line.split(" ");
        if (cc[0] == "dir") {
          res( [cc[1]], [{ name: "cd "+cc[1], value: (dir + cc[1] + "/") }] );
        }
        else
        {
          res( [line] );
        }
      }
      res();
    });
  }

  k: QtObject {
    property var name: "Опросить сервер"
  }
}