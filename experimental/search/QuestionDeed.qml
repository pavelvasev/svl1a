// Действие вопроса
Deed {
  id: deed

  //icon: "вопрос"
  icon: deed.ability.name
  property var k: ability.k
  // но вообще странно. k есть параметр действия, так-то.. чего он в ability то впечатался.. оч странно..

  //property var server: k.server
  property var server: deed.input.server

  ResultRobot {
    id: rr
    k: deed.k
    server: deed.server
  }

  Component.onCompleted: {
    rr.results = [];
    rr.additional_ks = [];
    
    server.ask( k, function( r,k ) {
      if (r) { rr.results = rr.results.concat(r); }
      if (k) { rr.additional_ks = rr.additional_ks.concat(k) };

      if (!r && !k) risovanie.activate(rr); // setTimeout( function() { risovanie.activate(rr);}, 100 );
    } );
  }

  Text {
    text: "name="+k.name+" value="+k.value
    id: t1
  }
  params: [t1]
}