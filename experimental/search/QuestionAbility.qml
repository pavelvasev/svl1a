// Вопрос 
Ability {
  deedPath: Qt.resolvedUrl("QuestionDeed.qml")
  category: "query"

  function feel(obj) {
    //if (wp.get_type(obj) == "ResultRobot") return 1;
    //if (obj === parent) return 1;
    if (obj == targetObj) return 1;
    return 0;
  }

  property var k: [] // критерий, есть пара [значение, пояснение]
  name: k.name // не нужен name длинный, т.к. критерий feel все-равно объект проверит + "@" + wp.get_object_user_name( targetObj )

  property var targetObj // объект к которому применима эта Ability
}