Registrator {
  onCreated: {
    if (obj.$class == "PerformDeedButton" && obj.tag == "right") attach( obj );
  }

  function attach( obj ) {
    if (obj.wauattached) return;
    obj.wauattached = 1;
    //queue.push( obj );
    gen.generate( {}, obj );
  }
  //property var queue: []

  QmlGenerator {
    id: gen
    source: "WauSubitems.qml"
    // onLoaded: underscene.refineAll();
  }
}