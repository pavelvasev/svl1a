Item {
  id: gen
  property var names: [] // имена действий
  property var titles: []
  property var input

  Repeater {
    id: rep
    model: names.length
    PerformDeedButton {
      name: names[index]
      text: titles[index] || name[index]
      input: gen.input
      record: true
      tag: null
      visible: false
      onPerformedGood: risovanie.activate( outputDeed )
    }
  }

  property var params: gen.children.filter( function(item) { return item != rep; })
}