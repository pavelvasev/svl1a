Deed {
    id: deed

    PerformDeed {
      name: "load-entity-known"
      id: p1
      robotLayer: "hidden"
    }

    GenerateDeedButtons {
      names: ["load-entity-url","load-deed-text"]
      titles: ["Подключить по URL","Действие из текста"]
      id: gen
    }

    Row {
    spacing: 5
    PerformDeedButton {
      name: "load-entity-url"
      text: "Подключить по URL"
      record: true
      tag: null
      onPerformedGood: risovanie.activate( outputDeed )
    }
    PerformDeedButton {
      name: "load-deed-text"
      text: "Действие из текста"
      record: true
      tag: null
      onPerformedGood: risovanie.activate( outputDeed )
    }
    id: ro
    }

/*
    ComposeRow {
      input: [ gen ]

      id: c1
    }

    ComposeColumn {
      input: [ p1.outputDeed ]
      id: cr
    }
*/

    params: (p1.outputDeed ? p1.outputDeed.params : []).concat( [ro] )
}
