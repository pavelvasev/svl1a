Deed {
    id: deed

    params: [t1,tp,btn]

    Text {
        id: t1
        text: "Укажите URL для загрузки:\n* Действия json\n* Qml-файл\n* Сцена json,vl"
    }

    TextInput {
        id: tp
        width: 450
        property alias value: tp.text
        property var guid: "url"
        function loadValue(val) {
          text = val;
          updateUrlas();
        }
    }

    Button {
      text: "ВВОД"
      onClicked: updateUrlas(); 
      id: btn
    }

    function updateUrlas() {
      urlas = tp.text;
    }

    property var urlas: ""

    PerformDeed {
        name: "load-ability"
        input: urlas
        once: true
        // options: [{robotLayer:"system"}]
        // следующее сделано чтобы они не сразу кидались загружаться
    }
}
