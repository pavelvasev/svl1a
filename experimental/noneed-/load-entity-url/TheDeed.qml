Deed {
    id: deed

    params: [t1,tp,btn,t2]

    Text {
        id: t1
        text: "Укажите URL для загрузки, по 1 в строке. Умеем загружать:\n* Пакеты\n* Действия json\n* Qml-файлы\n* Сцены json,vl"
    }

    TextEdit {
        id: tp
        width: 450
        height: 150
        property alias value: tp.text
        property var guid: "urlas"
        function loadValue(val) {
          text = val;
          updateUrlas();
        }
    }

    Button {
      text: "ВВОД"
      onClicked: updateUrlas(); 
      id: btn
    }
    Text {
        id: t2
        text: "Введено для загрузки, штук: "+urlas.length
    }

    function updateUrlas() {
      urlas = tp.text.split("\n");
    }

    property var urlas: []

    PerformDeed {
        name: "load-ability"
        input: urlas
        once: true
        // options: [{robotLayer:"system"}]
        // следующее сделано чтобы они не сразу кидались загружаться
    }
}
