Ability {
  icon: "подключить-url"

  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( obj ) {
    if (!obj) return 1;
    if (wp.get_type( obj) == "Scene") return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}