Ability {
  icon: "подключить"

  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( obj ) {
    if (!obj) return 0;
    return 1;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}