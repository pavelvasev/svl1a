﻿// робот, создающий других роботов через гуи
// плохо однако то, что он не вписывается в идею записи лога действий...
// ведь каждое его создание - это действие..
// О! А пусть каждый запуск QmlGenerator это реально действие.. что такого?
// Ну да, получается, действие с параметром... вот, пришла пора подумать о параметрах действий
// а не только об их входных объектах...

// Но тогда все-таки надо PerformDeedRec... а ему надо wp.userDeedHistory синглтон Array2d..

// Да. По факту он сейчас не используется. Все ушло в create-robot .

Robot {
  icon: "gen"
  property var names: ["FileRobot","Array2dRobot","Spheres","Points","Lines","Triangles" ]
  
  params: [q]
  property var chosenName: ex.current.text

  Row {
    id: q
    //property var tag: "left"
    visible: false
    Column {
      ExclusiveGroup {
        id: ex
      }
      Repeater {
        model: names.length
        RadioButton {
          text: names[index]
          exclusiveGroup: ex
        }
      }
    }
    Button {
      text: "Создать "+chosenName
      onClicked: cre.perform()
    }
  }

  id: rg
  property bool iAmRobotGenerator: true

  PerformDeedRec {
    name: "create-robot"
    manual: true
    object: rg
    // надо будет это сделать init: [chosenName]
    id: cre
    onPerformedGood: outputDeed.source = chosenName
  }
}