// Вход  in1 - 1 мерный массив объектов с полем .text
// Вход  getsubitems - функция, которая по объекту из in1 выдает массив объектов с полем .key

// Выход output  - массив выбранных объектов в форме { key: k, text: k, title: val.title, objref: val.objref, guid: val.guid }

// косвенный вход/выход - tablica содержит выбранные объекты в виде бо key -> {objref: it.objref, guid: it.guid, title: it.text }

Column {
    id: cc
    width: 500
    spacing: 5

    property var in1: []  // входной массив объектов с полем .text

    property var output: []
    property var tablica: { return {} }

    ComboBox {
        model: in1.map( function(i) { return i.text } );
        id: cb1
        width: 250
    }

    property var subitems: getsubitems( in1[ cb1.currentIndex ] ) || []

    Row {
        Column {
            width: 200
            ComboBox { // слева все действия
                model: subitems.map( function(f) { return f.text; } )
                size: 10
                height: 170
                width: parent.width
                id: cb2
            }
            Text {
                //text: cb2.currentText || ""
                text: subitems[ cb2.currentIndex ] ? subitems[ cb2.currentIndex ].key : "-"
                width: parent.width
                wrapMode: Text.WordWrap
            }
        }
        Button {
            text: ">>"
            onClicked: {
                var it = subitems[ cb2.currentIndex ];
                tablica[ it.key ] = {objref: it.objref, guid: it.guid, title: it.text };
                cc.tablicaChanged();
            }
        }
        Column {
            width: 220
            ComboBox { // справа выбранные
                size: 10
                height: 120
                width: parent.width
                //model: tablica.forEach( function(k,v) { return k; } )
                model: output.map( function(f) { return f.text; } )
                id: cb3
            }

            Button {
                text: "удалить"
                anchors.right: parent.right
                onClicked: {
                    var it = output[ cb3.currentIndex ];
                    tablica[ it.key ] = null;
                    cc.tablicaChanged();
                }
            }

            Text {
                text: cb3.currentText|| ""
                width: parent.width
                height: 30
                wrapMode: Text.WordWrap
            }
        }
        spacing: 5
    }



    onTablicaChanged: updateOutput();

    function updateOutput() {
        var res = [];

        for (var k in tablica) {
            var val = tablica[k];
            if (val) res.push( { key: k, text: k, title: val.title, objref: val.objref, guid: val.guid } );
        }

        //console.log("----->output=",res)
        output = res;
    }

}
