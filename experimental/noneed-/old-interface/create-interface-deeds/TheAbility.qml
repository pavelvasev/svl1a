Ability {
  title: "Добавить интерфейс D"
  help: "Обходит окружающих роботов и позволяет набрать их действия в отдельный интерфейс"

  function feel( obj ) {
    if (!obj) return 0;
    if (wp.get_type(obj) == "Interface") return 0;
    if (obj.ability && obj.ability.name == "create-interface") return 0;
    return 1;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}