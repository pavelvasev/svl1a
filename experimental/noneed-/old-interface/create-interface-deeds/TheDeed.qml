// см doc/2016-06-23-interface.md 
Deed {
    id: deed
    property bool thisisinterfacedeed: true

    TabView {
        width: 500
        height: Math.min( 250, Math.max( c1.height, c2.height )+30 )
        id: tv1

        Tab {
            title: "Выбор"

            css.maxHeight: "350px"
            css.overflowY: "scroll"
            css.overflowX: "hidden"
            css.pointerEvents: "all"

            Choser2 {
              id: c1
            }
        }
        Tab {
            title: "Заголовки"
            PostroitFxTabl {
                in1: c1.output
                id: c2
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////

    function prepare_params( root ) {
        var res = [];
        wp.qml_tree_walk( root, function(item) {
            if (item.thisisinterfacedeed) return;
            if (item === deed || item === iface || item.type === "Interface" || item.$class === "Deed" || item.$class === "Ability") { return; }
            if (!item.is_wonder_item && !item.visual && !item.positions) { return; };
            var q = wp.get_object_user_name(item);
            res.push( { text: q, item: item } );
        });
        return res;
    }

    function getsubitems( x ) {
        if (!x) return [];
        var item = x.item;

        if (pdd.outputDeed) pdd.outputDeed.input = item; else pdd.input = item;
        if (!pdd.output) return;

        var abils = pdd.output.filter(function(f) { return f.ability.category != "system-deeds" && f.ability.category.indexOf("hidden")==-1; } ); // убрали все системные
        if (abils.length == 0) return;

        var t = (item.$class == "Deed" && item.ability ? item.ability.title : wp.get_type( item ) );
        var g = wp.get_object_user_name( item );

        var res=[];
        abils.forEach( function(p) {
            res.push( { text: (p.ability.title || p.ability.name), key: p.ability.name+"@"+g, guid: p.ability.name, objref: g } );
        });
        return res;
    }

    PerformDeed {
        name: "find-abilities-for-object"
        id: pdd
        input: pdd
    }


    ////////////////////////////////////////////////////////////////////

    Component.onCompleted: prep(); // if затем, что findo может вызываться в initParams, а операция дорогая..

    function prep() {
        c1.in1 = prepare_params( deed.input );
    }

    Button {
        id: b1
        text: "Обновить списки роботов"
        width: 240
        onClicked: prep();
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ////////////////////////// Порождаем интерфейс

    // генерируем действия
    Column {
      spacing: 5

      Text { 
        text: c1.output.length > 0 ? "Доп. возможности" : ""
      }

      Repeater {
        model: c1.output.length
        TextButton {
          text: "<u>"+c2.ff( c1.output[index] )+"</u>"

          onClicked: {
            if (pd.outputDeed) {
              risovanie.activate( pd.outputDeed );
            }
            else {
              pd.input = wp.find_by_uniq_name( (c1.output[index] || "").objref );
              pd.perform();
            }
          }
          PerformDeed {
            name: (c1.output[index] || "").guid          
            parentToObject: true
            record: true
            once: true
            manual: true
            id: pd
            activateDlg: true
          }
        }

      }
      visible: false
      id: cdeeds
      anchors.right: parent.right
    }

    property var interface: iface
    Robot {
        icon: "интерфейс D"
        type: "Interface"
        params: [cdeeds]
        id: iface
        property bool guiHideDeeds: true
        property bool saveDisabled: true // интерфейс не сохраняет параметры, они сохраняются в исходных объектах
    }

    ///////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// Сохраняем-загружаем
    Item {
        id: ca
        property var guid: "chosend"

        function saveValue() {
          return c1.output.map( function(x,index) { return { objref: x.objref, guid: x.guid, title: c2.ff(x) } })
        }

        function loadValue(val) {
            var tabl = {};
            var tabl2 = {};
            val.map( function(rec) {
                tabl[ rec.guid + "@" + rec.objref ] = { objref: rec.objref, guid: rec.guid };
                tabl2[ rec.guid + "@" + rec.objref ] = rec.title;
            });
            c1.tablica = tabl;
            c2.tablica = tabl2;
        }
    }
    property var sav: ca

    ////////////////////////////////////////////////////////////////////

    params: [tv1,b1,ca]
    //details: "найдено параметров="+ito.length
}
