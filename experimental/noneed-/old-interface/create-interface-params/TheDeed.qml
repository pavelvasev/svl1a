// см doc/2016-06-23-interface.md 
Deed {
    id: deed
    property bool thisisinterfacedeed: true

    TabView {
        width: 500
        height: Math.min( 250, Math.max( c1.height, c2.height )+30 )
        id: tv1

        Tab {
            title: "Выбор"

            css.maxHeight: "350px"
            css.overflowY: "scroll"
            css.overflowX: "hidden"
            css.pointerEvents: "all"

            Choser {
              id: c1
            }
        }
        Tab {
            title: "Заголовки"
            PostroitFxTabl {
                in1: c1.output
                id: c2
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////
    // обходит поддерево с корня root и набирает массив параметров в формате для Chooser

    function prepare_params( root ) {
        var res = [];
        wp.qml_tree_walk( root, function(item) {
            if (item.thisisinterfacedeed) return;
            if (item === deed || item === iface || item.type === "Interface") return;
            var pp = (item.params || []).filter( function(p) { return !!p.guid; });
            if (pp.length == 0) return;

            var t = (item.$class == "Deed" && item.ability ? item.ability.title : wp.get_type( item ) );
            var g = wp.get_object_user_name( item );
            res.push( { istitle: true, text: "<b>"+t+"</b>" } );

            pp.forEach( function(p) {
                res.push( { text: (p.text || p.guid), key: p.guid+"@"+g, guid: p.guid, objref: g, param: p } );
                // ссылку на параметр надо хранить чтобы ее указать в Interface
            });
        });
        //console.log("res=",res);
        return res;
    }

    ////////////////////////////////////////////////////////////////////

    Component.onCompleted: prep(); // if затем, что findo может вызываться в initParams, а операция дорогая..

    function prep() {
        c1.in1 = prepare_params( deed.input );
    }

    Button {
        id: b1
        text: "Обновить списки роботов"
        width: 240
        onClicked: prep();
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ////////////////////////// Порождаем интерфейс

    // генерируем заголовки интерфейса
    Item {
        Repeater {
            model: c2.output.length
            id: titrep
            Text {
                text: c2.output[index] ? "<b>"+c2.output[index]+"</b>" : ""
                visible: false
                wrapMode: Text.WordWrap
                width: 400
            }
        }
    }

    property var chosenParams: genChosen()

    function genChosen() {
        var p = [];
        var chosen = c1.output;
        for (var v in chosen) {
            if (titrep.count > v) {
                var titl = titrep.itemAt(v);
                p.push( titl ); // заголовок
            }
            var vv = chosen[v];
            p.push( vv.param )
        };
        return p;
    }

    property var interface: iface
    Robot {
        icon: "интерфейс P"
        type: "Interface"
        params: chosenParams
        id: iface
        property bool guiHideDeeds: true
        property bool saveDisabled: true // интерфейс не сохраняет параметры, они сохраняются в исходных объектах
    }

    ///////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// Сохраняем-загружаем
    Item {
        id: ca
        property var guid: "chosenp"

        function saveValue() {
          return c1.output.map( function(x,index) { return { objref: x.objref, guid: x.guid, title: c2.output[index] } });
        }

        function loadValue(val) {
            var tabl = {};
            var tabl2 = {};
            val.map( function(rec) {
                tabl[ rec.guid + "@" + rec.objref ] = 1;
                tabl2[ rec.guid + "@" + rec.objref ] = rec.title;
            });
            c1.tablica = tabl;
            c2.tablica = tabl2;
        }
    }
    property var sav: ca

    ////////////////////////////////////////////////////////////////////

    params: [tv1,b1,ca]
    //details: "найдено параметров="+ito.length
}
