// см doc/2016-06-23-interface.md 
Deed {
    id: deed
    property bool thisisinterfacedeed: true

    PerformDeed {
      name: "create-interface-params"
      input: deed.input
      once: true
      id: cp
      options: [ {initParams: deed.initParams, title: "Параметры"} ]
      robotLayer: "hidden"
    }

    PerformDeed {
      name: "create-interface-deeds"
      input: deed.input
      once: true
      id: cd
      options: [ {initParams: deed.initParams, title: "Действия"} ]
      robotLayer: "hidden"
    }

    ////////////////////////////// наше гуи
    params: [ct,i1,i2]

    ComposeTabs {
      input: { 
        var t = [cp.outputDeed,cd.outputDeed].filter( function(i) { return !!i; } )
        return t.length == 2 ? t : []; // надо сразу все ей давать, а то видимость тупит
      }
      id: ct
      //Component.onCompleted: { currentIndex = 0; }
    }

    ////////////////////////////// параметры для сохранения
    Item {
      property var guid: "chosenp"
      function saveValue() {
        return cp.outputDeed ? cp.outputDeed.sav.saveValue() : null;
      }
      id: i1
    }
    Item {
      id: i2
      property var guid: "chosend"
      function saveValue() {
        return cd.outputDeed ? cd.outputDeed.sav.saveValue() : null;
      }
    }

    ////////////////////////////// порожденный интерфейс
    property var interface: iface
    Robot {
        icon: "интерфейс"
        type: "Interface"
        params: (cp.outputDeed ? cp.outputDeed.interface.params : []).concat( cd.outputDeed ? cd.outputDeed.interface.params.map(function(i) { i.secondcolumn=true; return i }) : [] )
        //onParamsChanged: console.log(params);
        id: iface
        property bool guiHideDeeds: true
        property bool saveDisabled: true // интерфейс не сохраняет параметры, они сохраняются в исходных объектах
    }
}
