Ability {
  title: "Преобразовать 2 в 1"
  help: "Вычисление произвольной функции над 2 массивами и записью результата в новый массив"
 
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    return (wp.get_type( target_object_x ) == "Array2dRobot");
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}