Deed {
    id: deed
    robotIcon: "[преобр-2-в-1]"

    property bool iLoveParams: true

    Input2 {
      id: in2
      function feel( obj ) { return (wp.get_type( obj ) == "Array2dRobot") && obj !== resarr; }
    }

    ///////////////////////////////////////////

    function convert(arr,arr2) {
      var res = [];
      for (var i=0; i<arr.length; i++) {
        res.push( arr[i] );
        var ndist = 1000000000;
        var nj = -1;
        for (var j=0; j<arr2.length; j++) {
          var d = 0;
          for (var k=0; k<arr[i].length; k++)
            d += (arr[i][k] - arr2[j][k]) * (arr[i][k] - arr2[j][k]);
          if (d < ndist) {
            ndist = d;
            nj = j;
          }
        }

        res.push( arr2[nj] );
      }
      return res;
}

/*
function convert(arr,arr2) {

function lineeq(l1,l2) {
  if (l1.length != l2.length) return false;
  for (var i=0; i<l1.length; i++)
    if (l1[i] != l2[i]) return false;
  return true;
}
      var res = [];
      for (var i=0; i<arr.length; i++) {
        var cnt = 0;
        for (var j=0; j<arr2.length; j++) 
          if (lineeq( arr[i], arr2[j])) cnt++;
        res.push( [cnt] );
      }
      return res;
}
*/

    params: [in2.param, ex,th, p,status]

    Text {
      text: "Функция преобразования. Вход: arr и arr2 - двумерный js-массив. Выход - двумерный js-массив."
      id: th
    }

    TextEdit {
      height: 300;
      width: 400
      text: deed.convert.toString();
      property alias value: p.text
      property var guid: "code"
      id: p
    }

      Text {
        id: status
        text: "Статус выполнения: " + statusMsg
        color: text.indexOf("error") >= 0 ? "red" : "green";
      }

    Row {
      id: ex
      Button {
        text: "Пример 1"
        width: 120
        onClicked: p.value = deed.convert.toString();
      }
    }

    property var preobrCode: p.value
    property var statusMsg: "ok"
    property var inputArr: deed.input ? deed.input.arr || [] : []
    property var arr2: in2.result ? (in2.result.arr || []) : []

    function evalUserPreobr() {
      try {
        var preobrFunc = eval( "("+preobrCode+")" )
        var res = preobrFunc( inputArr,arr2 )
        statusMsg = "ok, returned array of " +res.length + " lines";
        return res;

      } catch(e) {
        statusMsg = "error: " +e.message;
        console.error(e);
        return [];
      }

    }

    Array2dRobot {
      id: resarr
      arr: evalUserPreobr()
    }
}
