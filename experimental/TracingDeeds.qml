// input - объект PerformDeed или PerformDeedButton или Ability. Главное чтобы был сигнал performedGood.
Grid {
  id: gg
  columns: 4
  spacing: 2
  property var input
  property bool bonus: true

  onInputChanged: {
    if (!input.performedGood) return;
    input.performedGood.connect( gg,newdeed );
  }

  function newdeed(obj) {
    items.push(obj);
    itemsChanged();

    obj.Component.destruction.connect(obj, function() {
        var index = items.indexOf( obj );
        if (index >= 0) items.splice( index,1 );
        itemsChanged();
    } );

    // бонус
    if (bonus) setTimeout( function() { risovanie.activate( obj ) }, 500 );
  }

  property var items: []

  Repeater {
    model: items.length
    PaintRobot {
      robot: items[index] 
      forceSingle: true
    }
  }
}