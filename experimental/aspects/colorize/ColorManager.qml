Rectangle {
  property var subjectdeed
  color: subjectdeed ? color2css( subjectdeed.params[0].color ) : "transparent"

  anchors.bottom: parent.bottom-1
  anchors.left: parent.left+2
  anchors.right: parent.right-2
  height: 4
}