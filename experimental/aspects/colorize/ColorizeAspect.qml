/*
  Идея: Нужен язык -- Класс объекта + значение свойства => действие + отменадействия
  .. 
*/
Registrator {
  id: reg

  onCreated: {
    if (obj.$properties["impaintrobot"]) {
      obj.robotChanged.connect( reg,function() { an(obj); } );
      an(obj);
    }
  }

  function an(obj) {
    if (!obj.robot) return;
    if (obj.robot.$class == "Deed" && obj.robot.ability.name == "manage-object-color") {
      //console.log("colorrrrrrr!",obj.robot);
      attach( obj );
    }
  }

  function attach( obj ) {
    if (obj.coattached) return;
    obj.coattached = 1;
    //console.log("-------------------------------- attaching color manager to obj" );
    gen.generate( {subjectdeed: obj.robot}, obj.selfrect );
    var p = obj.parent;
    while (p) {
      if (p.robot && p.robot.$class != "Deed") {
        //console.log("see pr....",obj);
        gen.generate( {subjectdeed: obj.robot}, p.selfrect );
      }
      p = p.parent;
    }
  }

  QmlGenerator {
    id: gen
    source: Qt.resolvedUrl( "ColorManager.qml" )
  }
}