//   вставлен в VlWindow.qml 
//   property var backHistory: BackHistoryAspect {}
Item {

  id: it

  property var items: QmlItems {}

  property var origactivate

  function activate2( obj ) {
    if (items.items.length > 0 && items.items[ items.items.length-1 ] === obj) 
    {
      // не добавляем, если хвост массива равен этому элементу
    }
    else {
      if (obj) items.add( obj );
    }

    origactivate( obj );
  }

  Component.onCompleted: {
    origactivate = risovanie.activate;
    risovanie.activate = it.activate2;
  }

}