// Вставлено в show-robot-window в SimpleDialog / Row

TextButton {
  text: backHistory.items.items.length > 1 ? "Назад" : ""

  onClicked: {
    backHistory.items.items.pop(); // удалили текущий элемент
    backHistory.items.itemsChanged();
    if (backHistory.items.items.length == 0) return;

    var last = backHistory.items.items[ backHistory.items.items.length-1 ]; // взяли предыдущий элемент
    risovanie.activate( last ); // и идем к нему
  }
}