Ability { 
  title: "Выдать список возможностей"

  function feel( obj ) {
    return !!obj;
  }

  function act( obj ) {

      var abilities = wonderfulplace.find_abilities( underscene );
      var res = [];

      for (var j=0; j<abilities.length; j++)
      {
        var ab = abilities[j];
        var arg = ab.feel( obj );
        if ( !(arg > 0) ) continue;

        res.push( { name: ab.name, title: ab.title, help: ab.help, category: ab.category } );
      }

    return res;
  }
}