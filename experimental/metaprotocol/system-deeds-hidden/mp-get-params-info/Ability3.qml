Ability { 
  title: "Выдать список параметров"

  function feel( obj ) {
    return !!obj;
  }

  function act( obj ) {
    var c = obj.paramsWithGuid;
    if (!c) return [];

    var res = [];
    for (var i=0; i< c.length; i++) 
      res.push( c[i].guid );

    return res;
  }
}