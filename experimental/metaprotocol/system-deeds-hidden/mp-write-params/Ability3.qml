Ability { 
  title: "Записать значения параметров"

  function feel( obj ) {
    return !!obj;
  }

  function act( obj, info ) {
    if (!info) return false;
    if (!info.initParams) return false;

    for (var k in info.initParams) {
      wp.writeParam( obj, k, info.initParams[k] );
    }

    return true;
  }
}