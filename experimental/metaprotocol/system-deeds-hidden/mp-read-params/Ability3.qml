Ability { 
  title: "Прочитать значения параметров"

  function feel( obj ) {
    return !!obj;
  }

  function act( obj, info ) {
    if (!info) return false;
    if (!info.initParams) return false;

    if (!info.initParams.guids) {
      info.initParams.guids = obj.paramsWithGuid.map( function(p) { return p.guid; } );
    }
    var guids = info.initParams.guids;

    var res = {};
    for (var i=0; i<guids.length; i++) {
      res[ guids[i] ] = wp.readParam( obj, guids[i] );
    }

    return res;
  }
}