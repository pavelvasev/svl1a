Ability { 
  title: "Выдать список детей"

  function feel( obj ) {
    return !!obj;
  }

  function act( obj ) {
    var c = obj.children;

    if (obj.extraChildren) c = c.concat( obj.extraChildren );
    console.log(" get children, c=",c);
    if (!c) return [];

    var r = risovanie.detector;
    var res = [];
    for (var i=0; i< c.length; i++) {
      console.log("testing c[i]=",c[i]);
      if (r.test( c[i] )) {
        res.push( wp.get_object_uniq_name( c[i] ) );
        console.log("passed");
      }
    }
    // todo выдавать сразу информацию по роботам?

    return res;
  }
}