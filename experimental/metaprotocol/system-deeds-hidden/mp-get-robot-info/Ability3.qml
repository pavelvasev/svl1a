Ability { 
  title: "Выдать информацию по роботу"

  function feel( obj ) {
    return !!obj;
  }

  function act( obj ) {
    var res = { type: wp.get_type( obj ), params: [], icon: obj.icon, title: obj.title, isprogram: obj.isProgram };

    var c = obj.paramsWithGuid;
    if (c) {
      for (var i=0; i< c.length; i++)  res.params.push( { guid: c[i].guid } );
    }
    if (obj.children.length == 0) res.isleaf = true;

    return res;
  }
}