Ability { 
  title: "Выдать список детей + информацию"

  function feel( obj ) {
    return !!obj;
  }

  function act( obj ) {
    var c = obj.children;

    if (obj.extraChildren) c = c.concat( obj.extraChildren );
    console.log(" get children, c=",c);
    if (!c) return [];

    var r = risovanie.detector;
    var res = [];
    var ci = [];;

    for (var i=0; i< c.length; i++) {
      console.log("testing c[i]=",c[i]);
      if (r.test( c[i] )) {
        res.push( wp.get_object_uniq_name( c[i] ) );
        ci.push( wp.perform_deed("mp-get-robot-info", c[i]) );
        //console.log("passed");
      }
    }
    // todo выдавать сразу информацию по роботам?

    return { children: res, info: ci};
  }
}