Robot {
  id: ro
  icon: "mp..."
  type: "AnotherWorldRobot"

  function risPainted( paintrect ) {
//     if (ro.parent.type != "AnotherWorldRobot")
       paintrect.color = "rgba(0, 128, 255, 0.341176)";
  }

  property var world
  property var object_id
  property alias robot_id: ro.object_id
  property alias guid: ro.object_id

  property var rinfo: null

  details: object_id + " " + JSON.stringify( rinfo,null,"  " )

  property bool ready: !!world && !!world.isConnected && !!object_id
  onReadyChanged: if (ready && !rinfo) update();

  property bool knownLeaf: false

  onRinfoChanged: {
      if (!rinfo) {
        ro.icon = "нет данных";
        ro.info = {};
        return;
      }
      ro.icon = "" + (rinfo.icon || rinfo.type);
      ro.title = rinfo.title || rinfo.type;
      ro.isProgram = rinfo.isprogram;
      if (rinfo.isleaf) ro.knownLeaf = true;
  }

  function update() {
    console.log("update called. ready=",ready, !!world,!!world.isConnected, !!object_id, "object_id=", object_id );
    if (!ready) return;

    world.getRobotInfo( object_id, function(resp) {
      rinfo = resp;
      update_children();
    });
  }

  function update_children() {
    // console.log("update_children called. ready=",ready,"loadChildren=",loadChildren);
    if (!ready) return;
    if (!loadChildren) return;

    world.getChildrenInfo( object_id, function(resp) {
      //console.log("got children",resp);
      if (!resp) return; // update();
      // ... beginPacket
      loadedChildrenInfo = resp;
      // ... endSendPacket
    } );
  }

  //property var forceColor: isDeed ? risovanie.deedColor : risovanie.robotColor

  risNewLine: true
  risClosed: knownLeaf ? false : true
  property bool loadChildren: !risClosed && !knownLeaf

  onLoadChildrenChanged: update_children()

  property var loadedChildrenInfo: []

  Repeater {
    model: loadChildren && loadedChildrenInfo.children ? loadedChildrenInfo.children.length : 0;
    AnotherWorldRobot {
      world: ro.world
      object_id: ro.loadedChildrenInfo.children[index]
      rinfo: ro.loadedChildrenInfo.info[index]
    }
  }
}