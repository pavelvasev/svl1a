WorldClientBase {
    id: client

    property var anotherWorldWindow

    property var mytoken: "WorldClientBro.qml" + client._uniqueId + Math.random()

    function receiveMessage(event)
    {
        //console.log("message rcved",event);

        if (event.data.token != mytoken) return;

        if (event.data.cmd == "response") {
            //console.log("this is repsonse!");
            //editorWindow.postMessage( { cmd: "set", value: vp.value }, "*" );

            var r = event.data;
            //console.log( "r=",r);
            if (r.responseId) {
              //console.log("have responseId=",r.responseId," pendingResponses=",pendingResponses);
              var callback = pendingResponses[ "resp" + r.responseId ];
              delete pendingResponses[ "resp" + r.responseId ];
              callback( r.responseData );
            }
        }
        
        if (event.data.cmd == "child-loaded") {
            isConnected = true;
            worldConnected();            
        }
    }

    property var pendingResponses: { return {} }
    property var reqCounter: 0

    function performAction( object_id, action_name, paramsHash, callback ) {
       var r = { cmd: "perform-action", object_id: object_id, action_name: action_name, params: paramsHash, token: mytoken };
       reqCounter = reqCounter+1;
       
       if (callback) {
         r.responseId = reqCounter;
         pendingResponses[ "resp" + r.responseId ] = callback;
       }

       console.log("posting performAction action_name=",action_name," to anotherWorldWindow=",anotherWorldWindow);
       anotherWorldWindow.postMessage( r, "*" );
    }

    /////////////////////////////////////////////////////////////

    Component.onCompleted: {
        window.addEventListener("message", receiveMessage, false);
    }

    Component.onDestruction: {
        window.removeEventListener("message", receiveMessage, false);
    }

    /////////////////////////////////////////////////////////////

    function openSvlWindow(args) {
      var urla = vlwindow.svlReloadUrl + "?rtp_token=" + mytoken + (args ? "&" + args : "");
      //anotherWorldWindow = window.open( urla, "_blank","width=900, height=500, top=100,left=100" );
      anotherWorldWindow = window.open( urla, "_blank" );

//      editorWindow.document.location = urrt;
//      editorWindow = window.open( "about:blank","_blank", "width=900, height=500, top=100,left=100" );      
    }
}