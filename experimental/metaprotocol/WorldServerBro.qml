Item {
    id: server

    Component.onCompleted: {
        window.addEventListener("message", receiveMessage, false);

        var token = getParameterByName("rtp_token");
        if (token && window.opener) {
          var re = { cmd: "child-loaded", token: token }
          window.opener.postMessage( re, "*" );
        }
    }

    Component.onDestruction: {
        window.removeEventListener("message", receiveMessage, false);
    }

    function receiveMessage(event)
    {
      if (event.data.cmd == "perform-action") {        
        // var r = { cmd: "perform-action", object_id: object_id, action_name: action_name, params: paramsHash };

        var r = event.data;
        console.log( "perform-action msg recv, r=",r);

        var obj = wp.find_by_uniq_name( r.object_id );
        if (!obj) {
          sendResponse( event, undefined );
          return;
        }

        enact( obj, event, true );
      }
    }

    function enact( obj, event, waitAbilityLoaded ) {
        var r = event.data;

        if (waitAbilityLoaded) {
          var ab = wp.find_abilites_of_name( r.action_name );
          if (ab.length == 0) {
          var f = function(aname) {
            if (aname == r.action_name) {
              //console.log("!!!!!!!!!!!############## ",aname);
              clearTimeout( tid );
              wp.abilityLoaded.disconnect( server,f );
              enact( obj, event, false );
            }
          };

          wp.abilityLoaded.connect( server,f );

          // и ждем макс 30 секунд загрузки етой штуки
          var tid = setTimeout( function() { 
            wp.abilityLoaded.disconnect( server,f );
            enact( obj, event, false );
          }, 30*1000 );

          return;

          } // ab.length == 0
        }

        
        var deed = wp.perform_deed( r.action_name, obj, { initParams: r.params }, obj );
        // deed мб объект действия, мб какие-то прямо данные, а мб null

        // console.log("peform deed action_name=",r.action_name,"returned",deed );

        if (deed && deed.$class) 
          deed = wp.get_object_uniq_name( deed );

        sendResponse( event, deed );
    }

    function sendResponse( event, responseData ) {
      var r = event.data;
      console.log("sendResponse called! r=",r, "responseData=",responseData);
      if (r.responseId)  {
        var re = { cmd: "response", responseId: r.responseId, responseData: responseData, token: r.token }        
        console.log("doing actual post, re=",re);
        event.source.postMessage( re, "*" );
        return;
      }
    }
}