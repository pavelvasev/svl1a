WorldClientBase {
    id: client

    property var endpoint

    onEndpointChanged: { 
      ping();
      //isConnected = true;
      //worldConnected();
    }

    function ping() {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', endpoint );
        var data = js2form( { cmd: "ping" } );
        xhr.onload = function(e) {
            var res = this.responseText;
            console.log("got ping resp e=",e,"res=",res,"this=",this)
            if (res.indexOf("samping")>=0) {
              console.log(">>>>>> mp ajax world connected, endpoint=", endpoint)
              isConnected = true;
              worldConnected();
            }
            else
              isConnected = false;
        };
        xhr.onerror = function(e) {
            console.error("mp ping error, endpoint=",endpoint,"e=",e)
            isConnected = false;
        }
        xhr.send(data);
    }

    function performAction( object_id, action_name, paramsHash, callback ) {
        // var r = { cmd: "perform-action", object_id: object_id, action_name: action_name, params: paramsHash };

        console.log("ajax: posting performAction action_name=",action_name," to endpoint=",endpoint, "object_id=",object_id);

        var data = null;
        if (!paramsHash || (Object.keys(paramsHash).length === 0 && paramsHash.constructor === Object)) {} 
           else data = js2form( { params: paramsHash } );

        var xhr = new XMLHttpRequest();
        // todo ? or & ?
        // todo object_id array?
        action_name = action_name.replace(/-/g,"_");
        xhr.open('POST', endpoint + "?cmd=perform-action&action_name="+action_name+"&object_id="+object_id );

        xhr.onload = function(e) {
            console.log("got resp e=",e);
            var res;
            if (this.status == 200) 
              res = JSON.parse( this.responseText );
            // https://mathiasbynens.be/notes/xhr-responsetype-json
            
            //,"res=",res)
            if (callback) callback( res );
        };

        xhr.onerror = function(e) {
            //setFileProgress( "3d export","server error",-1);
            console.error("mp error, endpoint=",endpoint,"e=",e)
            if (callback) callback( undefined );
        }

        data ? xhr.send(data) : xhr.send();
    }

    //////////////////////--------------------------
    function js2query (obj, prefix) {
        var str = [];
        for(var p in obj) {
            if (obj.hasOwnProperty(p)) {
                var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
                str.push(typeof v == "object" ? js2query(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
            }
        }
        return str.join("&");
    }

    function js2form( obj,prefix, form_data ) {
        if (!form_data) form_data = new FormData();
        for ( var p in obj ) {
           var k = prefix ? prefix + "[" + p + "]" : p
           var v = obj[p];
           if (typeof v == "object")
             js2form( v,k,form_data );
           else
             form_data.append(k, v );
        }
        return form_data;
    }

/*
    function js2form( obj ) {
        var form_data = new FormData();
        for ( var p in obj ) {
            form_data.append(p, obj[p]);
        }
        return form_data;
    }
*/

}
