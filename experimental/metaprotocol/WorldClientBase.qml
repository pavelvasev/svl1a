Item {
    signal worldConnected();
    property bool isConnected: false

    // выдает массив с guid объектов детей
    function getChildren( object_id, callback ) {
       return performAction( object_id, "mp-get-children", {}, callback );
    }

    // хеш children -> getChildren, info -> [getRobotInfo]
    function getChildrenInfo( object_id, callback ) {
       return performAction( object_id, "mp-get-children-info", {}, callback );
    }

    // выдает массив возможностей по роботу в формате {name, title, help}
    function getAbilities( object_id, callback ) {
       return performAction( object_id, "mp-get-abilities", {}, callback );
    }

    // params: ключ-значение для обновления
    function writeParams( object_id, params, callback ) {
      return performAction( object_id, "mp-write-params", params, callback );
    }

    function readParams( object_id, guids_array, callback ) {
      return performAction( object_id, "mp-read-params", { guids: guids }, callback );
    }

//    function getParamsInfo( object_id, callback ) {
//      return performAction( object_id, "mp-get-params-info", {}, callback );
//    }

    // выдает хеш с ключами по роботу, включая params
    function getRobotInfo( object_id, callback ) {
      return performAction( object_id, "mp-get-robot-info", {}, callback );
    }
}