Ability {  
  title: "Выполнить действие во внешнем мире"

  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( obj ) {
    if (obj && obj.type == "AnotherWorldRobot") return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}