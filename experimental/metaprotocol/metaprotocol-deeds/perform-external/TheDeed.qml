Deed {
    id: deed
    icon: "<@>"

    params: [b1,cb1,b2]

    function update() {
      deed.input.world.getAbilities( deed.input.guid, function(resp) {
        console.log("got resp ab:",resp);
        abs = resp;
      } );
    }

    property var abs: []

    property var abs2: {
      var sorted = abs.sort( function(a,b) { return a.category.localeCompare( b.category ); } );
      var res = [];
      var res2 = [];
      var cc;

      for (var i=0; i<sorted.length; i++) {
        if (sorted[i].category != cc) {
          cc = sorted[i].category;
          res.push( "....... " +cc+ " ....... " );
          res2.push("");
        }
        res.push( sorted[i].title || sorted[i].name );
        res2.push( sorted[i].name );
      }
      abs2name = res2;
      return res;
    }
    property var abs2name: []

    Button {
      text: "Обновить список действий"
      width: 200
      id: b1
      onClicked: update()
    }

    ComboBox {
      id: cb1
      width: 200
      //model: abs.map( function(ab) { return ab.title || ab.name; } );
      //property var value: abs[ currentIndex ] ? abs[ currentIndex ].name : ""
      model: abs2
      property var value: abs2name[ currentIndex ]
    }

    Button {
      id: b2
      text: "Выполнить"
      onClicked: {
        deed.input.world.performAction( deed.input.guid, cb1.value, {}, function(resp) {
          console.log("perform-external response:",resp);
          // перерисуем детей
          deed.input.risClosed = true;
          deed.input.risClosed = false;
        } );
      }
    }

    Component.onCompleted: update();
}
