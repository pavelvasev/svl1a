Deed {
  id: deed
  icon: "в карман"

  params: [ti]

  TextInput {
    id: ti
    property var guid: "title"
    text: "Заголовок"
    property alias value: ti.text
    width: 250
  }

  Flow {
    id: flo
    //title: ti.value
    anchors.fill: parent
    z: 4
    flow: Flow.TopToBottom
    property var title: ti.value
    spacing: 5
  }

  onInputChanged: doit();

  // child 0 =-> changed

  property var inparams: input ? input.params : []
  onInparamsChanged: doit();

  property bool firstParamVisible: inparams.length > 0 ? inparams[0].visible : -1
  onFirstParamVisibleChanged: doit();

  function doit() {
    console.log("doit, inparams=",inparams);
    inparams.forEach( function(p) {
      if (!p.paramOwnerRobot) {
        p.paramOwnerRobot = p.parent;
        p.oldSpaceParent = p.parent;
      }
      p.parent = flo;
      p.visible = true;
    });
  }

  Component.onCompleted: {
    console.log("K init!!!!!!!!!!!!!!!!!!!!!!");
    doit();
    karman1.add( flo );                
  }

}