Rectangle {
    id: r1
    css.pointerEvents: "auto"

    border.color: "grey"
    property bool refineDisabled: true

    anchors.bottom: vlwindow.bottom - 50
    anchors.left: vlwindow.left+30

    height: 150
    width: 1000

    function add( thing ) {
      thing.parent = tv.contentItem;
      console.log("~~~~~~ tv.contentItem.children=",tv.contentItem.children);
      tv.contentItem.childrenChanged();
    }

    TabView {
      id: tv
      anchors.fill: parent
      anchors.margins: 5
      //visible:false
    }

    property var jurl: Qt.resolvedUrl("../../viewlang-bridge/jquery-ui.js")

    Component.onCompleted: {
      la_require( "http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" );

      la_require( jurl, function() {
        r1.dom.className += " handle-for-rs";
        jQuery( r1.dom ).resizable({ handles: "se, n, e, ne" });
        //jQuery( r1.dom ).resizable({ handles: "n, e, ne" });

        jQuery( r1.dom ).on( "resize", function( event, ui ) {
          r1.width = ui.size.width;
          r1.height = ui.size.height;
        } );
      } );
    }
}