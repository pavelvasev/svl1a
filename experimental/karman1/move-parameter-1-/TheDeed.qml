Deed {
  id: deed
  icon: "вынести"

  params: [tt,cb1,tt2,ti, tpcb]

  Text {
    text: "Параметр для выноса"
    id: tt
  }

  property var iparams: (deed.input.params || []).filter( function(i) { return (!!i.guid) } )

  ComboBox {
    id: cb1
    model: iparams.map( function(i) { return i.guid; })
    property var guid: "chosenparam"
    property alias value: cb1.currentText    
  }

  Text {
    text: "Заголовок"
    id: tt2
  }

  TextInput {
    id: ti
    property var guid: "title"
    text: ""
    property alias value: ti.text
    width: 250
  }

  property var targetParam: iparams.filter( function(i) { return i.guid == cb1.value } ) [0]
  property var targetParamParent: targetParam ? targetParam.parent : null
  property var targetParamVisible: targetParam ? targetParam.visible : true

  onTargetParamChanged: {
    if (!targetParam) return;
    //console.log("-->",targetParam);
    targetParam.parent = col; // qmlEngine.rootObject.rightWidgets;    
    targetParam.visible = true;
  }

  onTargetParamVisibleChanged: {
    if (!targetParamVisible) { // тупой признак
      //console.log("qqqqqq");
      targetParam.parent = col; // qmlEngine.rootObject.rightWidgets;    
      targetParam.visible = true;
      qmlEngine.rootObject.refineSelf();
    }
  }

  Column {
    id: coco

    VisibleParam {
      width: 100
      text: ti.value
      target: col
      property var tag: "right"
      id: vp1

      onValueChanged: {
        targetParam.parent = col;
        //underscene.refineAll();
      }
    }

    Column {
      id: col
      property var tag: "right"
    }

    Row {
      id: ro
    }

  }

  //////////////////////////////////

  ComboBoxParam {
    values: ["Правая выноска","Карман 0"]
    text: "Место размещения"
    guid: "target_place"
    id: tpcb

    onValueChanged: {
/*      if (value == 1 && karman0) { // см scene-karman0.vl
        vp1.parent = coco;
        col.parent = coco;
        coco.parent = karman0;
      }
      else 
*/
      {
        vp1.parent = qmlEngine.rootObject;
        col.parent = qmlEngine.rootObject;
        qmlEngine.rootObject.refineAll();
      }
    }
  }
}