Ability {
  title: "Вынести параметр"
  help: "Выносит указанный параметр в другую область пользовательского интерфейса"

  // Также у каждого действия есть программное имя, оно берется из имени каталога, где размещено действие
  // Еще у действия есть категория - оно берется из имени верхнего каталога.
  // Другие свойства для действий см. `systema/core/base`

  // условие проверки, может ли это действие сработать на объекте obj
  function feel( obj ) {
    if (!obj) return 0;
    if (obj.params && obj.params.length > 0) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}