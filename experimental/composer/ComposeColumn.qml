Column {
  property var input: [] // массив объектов с полем .params каждый
  id: col
  spacing: 5

  property var params: [col]
  property alias output: params

  function refresh() {
    var out = [];
//    console.log("input=",input);
    input.forEach( function(item) {
      if (!item) return;
      ( item.params || [] ).forEach( function(p) {
        p.parent = col;
        if (!p.paramOwnerRobot) p.paramOwnerRobot = item;
        p.tag = null;
        p.visible = true;        
      } );
    } );
  }

  onInputChanged: refresh();  
  onVisibleChanged: if (visible) refresh();
}