TabView {
    id: ta
    property var input: [] // массив объектов с полем .params каждый
    width: 600
    height: getMaxOfArray( heights ) 
    property var heights: []
    //onHeightsChanged: console.log(heights);

    function getMaxOfArray(numArray) {
      return Math.max.apply(null, numArray);
    }
    onInputChanged: heights = heights.slice( 0, input.length );

    Repeater {
      model: input.length
      id: rep
      Tab {
        id: tabb
        title: (ta.input[index] || "").title || ((ta.input[index] || "").ability ? ta.input[index].ability.title : "")
        ComposeColumn {
          id: cc
          input: [ ta.input[index] ]
          visible: ta.visible
          onHeightChanged: { heights[index] = height+20; ta.heightsChanged(); }
        }
      }
    }

  property var params: [ta]
  property alias output: params
}