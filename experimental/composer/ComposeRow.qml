Row {
  property var input: [] // массив объектов с полем .params каждый

  spacing: 10
  Repeater {
    model: input.length
    id: rep

    ComposeColumn {
      input: [ ro.input[index] ]
      visible: ro.visible
    }
  }
  id: ro
  onVisibleChanged: refresh();

  property var params: [ro]
  property alias output: params

  function refresh() {
  }
}