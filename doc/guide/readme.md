# Superviewlang

![](program-intro/images/window-sm.png)

Программа предназначена для создания визуальных сцен и других программ. 
Программа работает в браузерах Chrome и Firefox последних версий. WebGL должен быть включен (он обычно включен).

### Запуск программы

Откройте ссылку: http://viewlang.ru/super?apply_scene=/svl1a/scena-starter.svl

### Справка

* [Термины](intro2/readme.md)
* [Введение в программу](program-intro/readme.md)

### Пошаговые примеры

* [Нарисовать прямоугольник и сферу по центру](example-rect/readme.md)
* [Нарисовать функцию Z(x,y) и построить на ней нормали](example-func/readme.md)
* [Нарисовать точки из файла и построить по ним поверхность](example-pts/readme.md)

### Примеры графических программ

* [Рисователь точек и поверхности](http://viewlang.ru/super?apply_scene=/svl1a/apps/lebedev/init.txt)
* [Рисователь графов](http://viewlang.ru/super?apply_scene=/svl1a/apps/grigoriev/2/init.txt)
* [Пример обработки изображений](http://viewlang.ru/super?apply_scene=/svl1a/apps/sinea/init.txt)

### Примеры других программ

* [Генератор документов на публикацию статьи](http://viewlang.ru/super?apply_scene=/svl1a/apps/papers/init.txt)
* [Преобразователь переносов](http://viewlang.ru/super?apply_scene=/svl1a/apps/perenosy/init.txt)
* [Преобразователь JSON-YAML](http://viewlang.ru/super?apply_scene=/svl1a/apps/json2yaml/init.txt)
