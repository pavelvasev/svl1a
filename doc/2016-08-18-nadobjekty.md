# Надобъекты

Можно создать описание объекта для модели 0-0 в духе
```
object New {

subobject Q1 #q1
subobject Q2 #q2 #trim
subobject Q2 #q3 #trim

port int #alfa
port #beta

zigota #ups { // это типа как функция, но описана удобнее, чтобы разворачивать в схему. что-то такое.
  zigota sum #s1
  zigota sum2 #s2
  link s1.output -> s2.input
  link input -> s1.input
  send 15 -> s2.input2
//  zigota console
  link s2.output -> console
}

link alfa -> q1.input
link alfa -> trim.in2
link q2.sosed -> q3

link q3.output -> ups
link ups -> beta

}
```

ХОрошо бы этому описанию быть совместимым с PlantUml

Имея такое описание, мы можем:
1. Создавать объекты по этому описанию
(можно даже без зигот, ибо пока не ясно, что это такое воообще)

2. Генерировать действия вида "сохдать Neo, имея только Q2". Если есть объект типа Q2, то можно создать действие, которое породит эту схему, надобъект Neo,
и в котором какое-то Q2 будет заменено на имеющийся на сцене.

Ожидаемые плюсы.
1. Возможность описывать объекты (хотя, мы и щас можем, на Qml).
Пояснение. Нам надо уметь создавать новые сущности. ЭТо принципиально. Такая возможность должна быть.
Чтобы описывать новые явления. В т.ч. более комплексные, состоящие из существующих.

2. Автоматическая генерация действий. По сути, действие это, создать объект Neo, но при этом вместо подобъекта поставить имеющийся; а также спросить 
и про другие, может их тоже можно поставить.

Например, можно будет:
Диалог создания треугольника из точки
Укажите точку 1 - существующая p
Укажите точку 2 - новая
Укажите точку 3 - новая
Ок, нажали, получили треугольник.

Для чего это вообще все появилось. Дублирующиеся действия: 
- создать из имеющегося файла изображение
- загрузить имеющееся изображение из файла
Можно было бы сделать схему: File -> Image, и тогда оно будет подставлять недостающие компоненты и будет щастье.

Аналогично "загрузить массив из файла" и "прочитать файл в массив".
И аналогично - compute и transform - превращаяется только в transform и над ним две схемы: A -> transform -> A' и A -> transform -> A

----

А можно ли описать схему преобразования изображения в массив и обратно? Image -> Array -> Image ? Но блин, это же Image -> transform
Кстати а какого фига transform у нас порождает объект того же типа? ЭТо же тупо. Это же именно что transform.... вычисление.. породить может
что угодно. Конечно есть extraArray, extraText, но это все неявная фигня. По идее трансформ должен спросить тип результата и в какой параметр писать.
Блин как все сложно.

Image -> T1 -> Array -> T2 -> Image

T1 и T2 всяко писать нужно. Ну напишем. И что даст эта схема? Да ничего.. ее на две надо разбить
Image -> T1 -> Array
Array -> T2 -> Image
тогда имея любой из этих компонент.. мы типа можем делать вещи:
"Преобразовать изображение в массив".
"Загрузить этот массив изображением".
"Преобразовать этот массив в изображение"
"Загрузить это изображение данными из массива".

Вопрос. А не слишком ли много возможностей?.. Нужны ли они все?... Это же вопрос юзабилити. Может что-то редко используется, а пространство информации заполним.

Может.. "Преобразовать массив в .. " -> выбор (файл, текст, изображение).
"Заполнить массив данными из..." -> выбор (файла, текста, изображения).

Но блин, еще хотелось бы, что бы все-таки действия не явно прописывались, а то все стрелочки придется прорисовывать. А чтобы оно как-то догадалось,
что вот это - преобразование из A в B, а это - из B в C, значит, есть действие A в C. 

Но может это можно сделать на мета-уровне, на возможностях. Дать им описание, что вот это - convert A в B и  т.д. 
И везде где идет одинаковое имя возможности, можем строить цепочку сразу.
Как вариант, хм.


В общем, исходнО, я вижу проблему в том, что при преобразовании одного во второе, по сути, идет 1 алгоритм. А на него мы должны давать 2 действия
(когда есть то или другое). И это немного странно.

--------------------------------

Создавать надобъекты не очень интересно. Ну есть у меня в фильме точки, и что, я из любых точек теперь буду создавать объект фильма?

А вот создавать новые процессы - интересно. И тут идет идея В.Л. про бульен. В мире накидали всяких штук, оно бульон. И система видит, что ребята,
у меня есть ингредиенты для создания такойто новой сущности. И такой-то другой. И эта сущность если это процесс, то это интересно. Динамика появляется,
в том числе неожиданная.

А еще идея Митрича про "расположить кубики по кругу". Но там немного другое.. Ну есть у нас круг, ок. И есть процесс расположения чего-либо
по кругу. Но кстати, это чего-либо он может смело копировать. Ну тогда ок. Есть схема "расположить по кругу", и в ней описано
*/
object PlaceByCircle {
  subobject Circle #c
  subobject #second

  zigota #rasp {
    берем координаты, берем second, клонируем second кучу раз, и ставим центр ему тудысько.
  }
}

А кстати клонирование может быть хитрым. Мы можем клонировать threejs объект. А исходный оставить один. Итогда он вроде как будет перегенерирвоать может даже клонов.
А может и нет. 
Надо понять что это с тз модели 0-0.
*/
И тогда если в сцене есть круг, и что-нибудь еще (а может и сам круг), то доступно действие "расположить по кругу".

Ну ладно. Это вроде бы как получается, все к вопросу об автоматической генерации действий. Это интересно, но это уже луковичный слой модель 2 или 3.

А пока ладно уж.. по старинке поработаем. Мне еще надо сделать интерфейсы и вызов действия меж миров браузера. И доделать изображения.