## Действие не показывается в карте

Добавить 
```
params: []
```


## Подключенный js-компонент не реагирует на мышку

Выставить контейнеру
```
css.pointerEvents: "all"
```

## Надо прокручивать содержание

```
            css.maxHeight: "500px"
            css.overflowY: "scroll"
            css.overflowX: "hidden"
            css.pointerEvents: "all"
```