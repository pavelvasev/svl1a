Robot {
  property var url: ""
  property var name: {
    var c = url.split("/");
    if (c.length < 2) return "-";
    return c[ c.length-2 ];
  }
  details: url
  //icon: "fa-folder"
  icon: name
  property bool risNewLine: true
  property bool risClosed: true

  function url2dir( url ) {
    // .replace("//","/")
    return url.replace(/\/[^\/]+\/?$/g, "/");
  }
}