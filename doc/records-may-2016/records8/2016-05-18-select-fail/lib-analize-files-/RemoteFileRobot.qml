Robot {
  property var url: ""
  property var name: url.split("/").pop()
  details: "url="+url
  icon: "fa-file-o"
}