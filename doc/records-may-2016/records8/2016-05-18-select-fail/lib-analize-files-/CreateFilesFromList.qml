Robot {
  id: thisitem

  //property var basedir: "" // базовый каталог
  property var urlarr: []  // массив урлей файлов

  onUrlarrChanged: generate( urlarr );

  function url2dir( url ) {
    // .replace("//","/")
    return url.replace(/\/[^\/]+\/?$/g, "/");
  }

  function generate( arr ) {
    console.log( "CreateFilesFromList generate called..");
    //console.log(arr);
    for (var i in arr) 
      processFile( arr[i] );
    console.log( "CreateFilesFromList generate finished.");
  }

  function processFile( url ) {
    //console.log("processFile url=",url );
    if ( !url || url.length == 0 || url[ url.length-1 ] == "/") return;

    var cc = url.split("/");
    var filename = cc.pop();
    var dirurl = url2dir( url );
    //cc.join("/") + "/";

    
    var dr = findOrCreateDirRobot( dirurl );
    var fr = fileGen.generate( { url: url }, dr );

    return fr;
  }

  function findOrCreateDirRobot( url ) {
    //console.log( "findOrCreateDirRobot, url=",url );
    var e = dirsHash[url]; if (e) return e;

    // если уперлись в http:// то все, директорий больше нет
    //if (url.search(/:\/.+/) < 0) return thisitem;
    if (url.replace(/^.+:\/\/[^\/]*\/?/,"") .length == 0) return thisitem;

    return dirGen.generate( { url: url } );
  }


  QmlGenerator {
    id: fileGen
    source: "RemoteFileRobot.qml"
    onLoaded: {
      object.$class = "RemoteFileRobot";
    }
  }

  QmlGenerator {
    id: dirGen
    source: "RemoteDirRobot.qml"
    onLoaded: {
      object.$class = "RemoteDirRobot";
      if (!dirsHash || dirsHash === "") dirsHash = {};
      dirsHash[ object.url ] = object;

      var parentDirUrl = url2dir( object.url );
      if (parentDirUrl == object.url) 
        debugger;
      else
        object.parent = findOrCreateDirRobot( parentDirUrl );
    }
  }

  property bool dirsHash: ""

/*
  Detector {
    id: dirs
    locateExisting: false
    function test( obj ) {
       if (wp.get_type( obj ) == "RemoteDirRobot") return true;
      return false;
    }
  }
  function findDirRobot( url ) {
    return dirsHash[url];
  }
*/
}