Deed {
  id: deed

  FileRobot {
    id: fl
    file: deed.input 
  }

  TextLoader {
    id: ldr
    file: deed.meta ? "" : fl.file
    onOutputChanged: ar.arr = ar.text2arr( ldr.output )
  }

  Array2dRobot {
    id: ar
    numeric: true
  }

  output: ar
}