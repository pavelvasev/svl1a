Ability {
  analizer: true

  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    if (!target_object_x || !target_object_x.search) return 0;

    return (target_object_x.search( /\.csv$/i ) > 0);
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}