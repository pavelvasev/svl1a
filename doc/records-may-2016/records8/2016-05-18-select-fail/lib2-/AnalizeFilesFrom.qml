Robot {
  property var url

  FileRobot {
   id: f
   file: "http://localhost:3000/ls"
  }

  TextLoader {
    file: f.file
    onOutputChanged: af.files = output.split("\n").map( function(f) { return "http://localhost:3000/"+f });
  }

  AnalizeFiles {
    id: af
  }
}