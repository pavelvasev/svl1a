Robot {
  id: af

  property var files: [] // массив файлов
  property int detected: 0

  details: "вижу файлов: "+files.length + " обнаружено полезных: " + detected

  onFilesChanged: {
    detected = go( files );
  }

  function go( arr ) {
    //console.log("AnalizeFiles go...",arr);
    var possible_abilities = wonderfulplace.find_abilities( underscene ).filter( function (ab) { return ab.analizer; } );
    var cnt = 0;

    for (var i in arr ) {
      var record = arr[i];
      for (var j in possible_abilities) {
        var ab = possible_abilities[j];
        if (ab.feel( record, arr ) > 0) {
          var r = ab.act( record, {meta: true} );
          if (r) { 
            r.parent = af;
            cnt = cnt + 1;
          }
        }
      }
    }
    return cnt;
  }

}