
Deed {
    id: deed

    icon: "txt-процессор"
    details: "кол-во байт текста: " + (vp.value || "").length

    //////////////////////////////////////////////
    property var indata : deed.input.txt || ""
    property bool hasUserEnteredValue: false

    property bool noWriteBack: false
    onIndataChanged: {
        if (noWriteBack) return;
        hasUserEnteredValue = false;

        vp.value = indata;

        if (editorWindow)
          editorWindow.postMessage( { cmd: "set", value: vp.value }, "*" );
    }

    Item {
        property var guid: "value"
        id: vp
        property var value

        function saveValue() {
            if (!hasUserEnteredValue) return undefined;
            return value;
        }

        function loadValue(v) {
            if (!deed.input) return;

            registerValue( v );
        }
    }

    function registerValue(v) {
        noWriteBack = true;
        deed.input.txt = v;
        vp.value = indata; // это важно, что не v. ибо там сработает update.. на этапе инициализации.. бред блин
        noWriteBack = false;
        hasUserEnteredValue = true;
    }

    //////////////////////////////////////////////////////////////////////

    property var urr: Qt.resolvedUrl("../../../../experimental/tinymce-app/1.html");
    property var urrt: urr + "?token="+mytoken
    property var editorWindow

    Row {
        spacing: 5
        property var guid: "controls"
        Button {
            text: "Запустить проце́ссор"
            width: 300
            onClicked: {
                editorWindow = window.open( "about:blank","_blank", "width=900, height=500, top=100,left=100" );
                //editorWindow.opener = null;
                editorWindow.document.location = urrt;
            }

        }
        TextButton {
            text: " | Просмотр"
            onClicked: {
              var vv = window.open( "about:blank","_blank", "width=900, height=500, top=100,left=100" );
              vv.opener = null;
              vv.document.write( vp.value );
            }
        }
        id: ro
    }

    params: [vp,ro]

    property var mytoken: deed.$class + deed._uniqueId + Math.random()

    function receiveMessage(event)
    {
        console.log("message rcved",event);

        if (event.data.token != mytoken) return;
        // event.source is popup
        // event.data is "hi there yourself!  the secret response is: rheeeeet!"
        if (event.data.cmd == "setme") {
            //console.log("sending set");
            editorWindow.postMessage( { cmd: "set", value: vp.value }, "*" );
        }
        if (event.data.cmd == "save") {
            registerValue( event.data.value );
        }
    }

    Component.onCompleted: {
        window.addEventListener("message", receiveMessage, false);
    }

    Component.onDestruction: {
        window.removeEventListener("message", receiveMessage, false);
    }
}
