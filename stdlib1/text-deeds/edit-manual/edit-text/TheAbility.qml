Ability {
  title: "Простой текстовый редактор"
  help: "Запускает редактор текста"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    return (wp.get_type( target_object_x ) == "TextRobot");
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}