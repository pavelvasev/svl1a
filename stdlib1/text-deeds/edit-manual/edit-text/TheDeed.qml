
Deed {
    id: deed

    icon: "txt-редактор"
    details: ""

    params: [co]
    setter: true

    property var inpos : input.txt || ""
    
    property bool hasUserEnteredValue: false

    // todo не делать преобразований если параметр невидим. а то это очень дорого.
    property bool noWriteBack: false

    onInposChanged: {
      if (noWriteBack) return;
      hasUserEnteredValue = false;

      if (inpos.length > 50000)
        txt.text = "... слишком много символов ("+inpos.length+">50000), редактор отключен.. ";
      else
        txt.text = inpos;
    }

    Column {
        id: co
        width: 500
        spacing: 8
        visible: false
        property alias value: txt.text
        property var guid: "txt"

        property alias sizeReason: txt

        function loadValue(v) {
          if (!deed.input) return;

          noWriteBack = true;
          deed.input.txt = v;
          noWriteBack = false;          

          txt.text = inpos; // binding update!
          hasUserEnteredValue = true;
        }

        function saveValue() {
          if (!hasUserEnteredValue) return undefined;
          return txt.text
        }

        TextEdit {
            height: 300
            width: parent.width
            id: txt
            onTextChanged: b1.visible = true
        }

        Button {
            id: b1
            text: "ВВОД"
            width: 150
            visible: false
            onClicked: {
              noWriteBack = true;
              deed.input.txt = txt.text;
              noWriteBack = false;
              hasUserEnteredValue = true;
              b1.visible = false
            }
        }

        Text {
          text: "  "
          visible: !b1.visible
          height: b1.height
        }
    }
}
