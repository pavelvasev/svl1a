Deed {
    id: deed
    icon: "загрузить"

    TextLoader {
      id: ldr
      file: deed.input.file
    }

    setter: true

    params: fil.params.concat([])
    property bool paramsSaveDisabled: true

    FileRobot {
      id: fil
      onFileChanged: f()
    }

    TextLoader {
      id: ldr
      file: fil.file
      onOutputChanged: deed.input.txt = output
    }
}
