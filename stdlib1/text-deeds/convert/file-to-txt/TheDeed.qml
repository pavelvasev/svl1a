Deed {
    id: deed

    TextRobot {
      txt: ldr.output
    }

    TextLoader {
      id: ldr
      file: deed.input.file
    }

}
