Robot {
  id: robot
  icon: "txt"
  details: "Длина текста: "+txtA.length + " символа"

  property var txt: ""
  property var txtA: txt || ""

  Text {
    text: "Предпросмотр:"
    id: pt
  }

  TextEdit {
    id: te
    width: 400
    height: 100
    property var guid: "preview"
    property bool saveDisabled: true

    Component.onCompleted: {
      te.dom.firstChild.readOnly = true;
    }
    text: txtA.length > 50000 ? txtA.substring( 0,50000)+".....есть еще, но мы стоп." : txtA

    TextButton {
      anchors.right: parent.right
      anchors.top: parent.bottom+3
      text: "<a href='javascript:0;'>Просмотр в окне</a>"
      onClicked: {
        var vv = window.open( "about:blank","_blank", "width=900, height=500, top=100,left=100" );
        vv.opener = null;
        vv.document.write( "<pre>" + robot.txt + "</pre>" );
      }
   }

  }

  params: [par,pt,te]

  function importSerialized( a ) {
    txt = a;
  }

  //////////////////////////////////////
    Item {
      property var guid: "txt"
      property alias value: robot.txt
      property bool saveDisabled: true
      id: par

      function loadValue( v ) {
        robot.txt = v;
      }
    }

}