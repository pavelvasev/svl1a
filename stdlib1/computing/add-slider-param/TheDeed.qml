// todo быстрый рендеринг может сделать за счет укрупнения сетки.. 
Deed {
  id: deed

  logicalName: tt.value

  title: "Параметр-слайдер"
  isProgram: false

  Param {
    id: sl
    guid: "par"
    text: "Значение "+tt.value
    onValueChanged: updat();
    min: ranges[0]
    max: ranges[1]
    step: ranges[2]
  }

  TextParam {
    id: tt
    guid: "varname"
    text: "Имя переменной для использования в коде"
    value: ""
    onValueChanged: updat();
  }

  TextParam {
    id: tdi
    guid: "range"
    text: "Диапазон: мин макс шаг (три числа через пробел)"
    value: ranges.join(" ")
    onValueChanged: {
      var r = value.split(/\s+/).map(function(f) { return parseFloat(f); });
      if (r.length == 0) r = [0,100,1];
      if (r.length == 1) r = r.concat( [100,1] );
      if (r.length == 2) r.push( 1 );
      if (r[2] <= 0) r[2] = 1;
      ranges = r;
    }
  }
  property var ranges: [0,100,1]

  params: [sl,tt,tdi]

  function updat() {
    if (!deed.input) return;
    if (tt.value.length == 0) return;

    deed.input[ tt.value ] = sl.value;
    deed.input.recompute();
  }

  onInputChanged: updat()
}