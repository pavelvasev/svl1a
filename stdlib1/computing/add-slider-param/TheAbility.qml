Ability {
  title: "Добавить параметр-слайдер"
  icon: "п-слайдер"

  // Также у каждого действия есть программное имя, оно берется из имени каталога, где размещено действие
  // Еще у действия есть категория - оно берется из имени верхнего каталога.
  // Другие свойства для действий см. `systema/core/base`

  // условие проверки, может ли это действие сработать на объекте obj
  function feel( obj ) {
    if (obj && obj.iLoveParams) return 1;
    /*
    if (wp.get_type(obj)=="Deed") {
       if (obj.ability.name == "preobrazovat") return 1;
       if (obj.ability.name == "compute-array") return 1;       
    }
    if (wp.get_type(obj)=="Function2vars") return 1;
    */
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}