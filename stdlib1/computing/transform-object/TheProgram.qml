Program {
    id: prg
    robotIcon: "[преобр.]"

    property var qq: (prg.input.params || []).filter( function(q) { return !!q.guid; });

    property bool iLoveParams: true

    // это для параметра-массив, чтобы не зацикливало .. add-array-param 
    property var antyCycleChecks: [prg.input]

    // функция пример
    function generate(inp) {
      return inp;
}

    Text {
      text: "Укажите параметр аргумент"
      id: t1
    }

    ComboBox {
      id: cb1
      model: ["--"].concat( qq.map(function(e) { return e.guid; }) );
      
      onCurrentIndexChanged: refind();
      onModelChanged: {
        if (valueToLoad) refind();
        else if (currentIndex == 0 && model.length == 2) currentIndex = 1; // сразу выбираем 1й параметр, если всего 1 параметр
      }
      property var guid: "param-name"

      property var value: model[ currentIndex ]
      
      property var valueToLoad

      onValueToLoadChanged: refind();

      function refind() {
        if (!valueToLoad) return;

        var ff = model.indexOf( valueToLoad );
        if (ff >= 0) {
          currentIndex = ff;
          valueToLoad = undefined;
        }
        recompute();
      }
    }    

    TextEditCode {
      title: "Функция. Вход и выход: значение параметра."
      value: prg.generate.toString(); /// todo : choseparam.desiredGenerateCode
      id: p
      guid: "code"

      Text {
        id: status
        text: "Статус выполнения: " + statusMsg
        color: text.indexOf("error") >= 0 ? "red" : "green";
      }
    }

    Button {
      text: "Пересчитать"
      property var guid: "recomputebt"
      onClicked: recompute()
      id: btRe
    }

    property var preobrCode: p.value
    property var statusMsg: "ok"

    onPreobrCodeChanged: recompute()

    property var tparam: qq[ cb1.currentIndex -1 ];
    property var arg0: wp.readParamValue( tparam );
    onArg0Changed: recompute();

    function evalUserPreobr() {
      try {
        var arg = wp.readParamValue( tparam ); // на всяк случай ко перечитаем
        var preobrFunc = eval( "("+preobrCode+")" )
        var res = preobrFunc(arg);
        
        statusMsg = "хорошо";
        return res;
      } catch(e) {
        statusMsg = "ошипка: " +e.message;
        console.error(e);
        return null;
      }
    }

    function recompute() {
      if (!ldr.item) { statusMsg = "не вижу объекта-цели"; return ;}
      if (!ldr.item.params) { statusMsg = "у объекта-цели нет params. объект выведен в консоль"; console.log(ldr.item); return ;}

      var newv = evalUserPreobr();
      var vv = cb1.value;
      var par = ldr.item.params.find( function(p) { return p.guid == vv } );
      wp.writeParamValue( par, newv );

      covalue.value = newv;
    }

    Loader {
      //sourceComponent: prg.input.Component
      source: wp.get_type( prg.input )
      id: ldr
      onItemChanged: {
        item.type = wp.get_type( prg.input );
        pln.valueChanged();
        recompute();
      }
    }

    ////////////////////////////////////////////
    ExtraResults { }


  ////////////////////////////////////////////
  TextParam {
    id: pln
    guid: "logicalName"
    text: "Логическое имя результата (необязательно)"

    onValueChanged: if (ldr.item) ldr.item.logicalName = pln.value;
  }

  Item {
    property var guid: "computed-value"
    property bool saveDisabled: true
    id: covalue
    property var value: ""
  }

  params: [t1, cb1, p,btRe, pln, covalue]

  ///////////////////////////////////////////////

  function templateOnSave( str, ip ) {
    str += "\n  "+extraFeel.toString().replace( "CLACLA", wp.get_type( prg.input ) )+ "\n";
    return str;
  }

  function extraFeel(obj) {
    return obj.input && wp.get_type(obj.input) == "CLACLA";
  }
}
