Ability {
  title: "Преобразовать"
  help: "Берет параметр, преобразует, и записывает в новый объект такого же типа"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( obj ) {
    if (!obj) return 0;
    if (obj.input) return 0; // типо программы и прочее пока не обрабатываем
    if (wp.get_type(obj) === "Interface") return 0;

    if (!obj.params) return 0;
    var qq = obj.params.filter( function(q) { return !!q.guid; });
    return qq.length > 0 ? 1 : 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheProgram.qml")
}