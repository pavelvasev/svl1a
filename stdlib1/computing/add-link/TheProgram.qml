Program {
    id: prg
    icon: "[ссылка]"

    params: [ins2, t1, cb1, tt ]

    Input2all {
      id: ins2
    }

    property var inobj: ins2.value
    property var qq: inobj && inobj.params ? inobj.params.filter( function(q) { return !!q.guid; }) : [];

    property bool iLoveParams: true

    // функция пример
    function generate() {
      return 123;
}

    Text {
      text: "Выберите параметр"
      id: t1
    }

    ComboBox {
      id: cb1
      model: ["self"].concat( qq.map(function(e) { return e.guid; }) );

      //currentIndex: 1 // чтобы с self не начинать
      onCurrentIndexChanged: refind();
      onModelChanged: refind();
      property var guid: "param-name"

      property var value: model[ currentIndex ]
      property var valueToLoad
      onValueToLoadChanged: refind();

      function refind() {
        var ff = model.indexOf( valueToLoad );
        if (ff >= 0) currentIndex = ff;
        recompute();
      }
    }

   TextParam {
     id: tt
     guid: "varname"
     text: "Имя переменной для использования в коде"
     value: ""
     onValueChanged: updat();
   }

  property var inparam: inobj && inobj.params ? inobj.params.find( function(q) { return q.guid == cb1.value; }) : null;

  property var sourceData: {
    if (cb1.value === "self") return inobj;

    var p = inparam;
    if (!p) return undefined;
    var vv = p.saveValue ? p.saveValue() : (p.valueToSave || p.value);
    return vv;
  }
  onSourceDataChanged: updat();
  // важно отслеживать изменение массива, который будем передавать.

  function updat() {
    if (!prg.input) return;
    if (tt.value.length == 0) return;
    if (sourceData === undefined) return;

    prg.input[ tt.value ] = sourceData;
    prg.input.recompute();
  }

  onInputChanged: updat()
}
