// Qml-объект "Выбор второго объекта"
// используется в параметрах действий
// вход: guid, text

// выход: .value - выбранный объект
//        this - параметр для добавления в гуи
Column {
    id: it

    property var arrdet: risovanie.detector

    property var output
    property alias result: it.output
    property alias value:  it.output

    property var valueToSave: { return { objref: wp.get_object_user_name( it.output ) } }
    property var valueToLoad: ""

    function loadValue(v) {
      if (arrdet.items.length == 0) arrdet.refresh();
      valueToLoad = v;
    }

    property var guid: "input2a"

    property alias text: txt1.text

    Text {
      id: txt1
      text: "Укажите объект"
    }

    property var anitems: {
      //var res = [];
      return arrdet.items.filter( function(i) { return i.ability ? i.ability.name != "load-ability" : true } );
    }

    ComboBox {
      id: p
      width: 300  // длинные имена бывают      
      model: ["---"].concat( anitems.map( function(i) { return i.logicalName ? i.logicalName : wp.get_object_user_name( i ); } ) )

      ////////////////////
      
      currentIndex: 1 + anitems.findIndex( function(i) { return wp.get_object_user_name( i ) == it.valueToLoad.objref } )

      onCurrentIndexChanged: it.output = anitems[ currentIndex-1 ];
    }
}