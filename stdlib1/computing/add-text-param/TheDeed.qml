Deed {
  id: deed

  logicalName: tt.value
  // risNewLine: true

  TextParam {
    id: sl
    guid: "par"
    text: "Значение "+tt.value
    onValueChanged: updat();
    width: 500
  }

  TextParam {
    id: tt
    guid: "varname"
    text: "Имя переменной для использования в коде"
    value: ""
    onValueChanged: updat();    
  }

  params: [sl,tt]

  function updat() {
    if (!deed.input) return;
    if (tt.value.length == 0) return;

    deed.input[ tt.value ] = sl.value;
    deed.input.recompute();
  }

  onInputChanged: updat()
}