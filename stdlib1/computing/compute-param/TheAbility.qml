Ability {
  title: "Вычислить из функции"
  help: "Проводит вычисление и записывает результат в указанный параметр данного объекта"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( obj ) {
    if (!obj) return 0;
    if (wp.get_type(obj) === "Interface") return 0;
    if (!obj.params) return 0;
    var qq = obj.params.filter( function(q) { return !!q.guid; });
    return qq.length > 0 ? 1 : 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheProgram.qml")
}