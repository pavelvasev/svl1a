Program {
    id: prg
    robotIcon: "[выч.]"

    setter: true

    property var qq: (prg.input.params || []).filter( function(q) { return !!q.guid; });

    property bool iLoveParams: true

    // это для параметра-массив, чтобы не зацикливало .. add-array-param 
    property var antyCycleChecks: [prg.input]

    // функция пример
    function generate() {
      return 123;
}

    Text {
      text: "Укажите параметр для записи результата"
      id: t1
    }

    ComboBox {
      id: cb1
      model: ["--"].concat( qq.map(function(e) { return e.guid; }) );
      
      onCurrentIndexChanged: refind();
      onModelChanged: {
        if (valueToLoad) refind();
        else if (currentIndex == 0 && model.length == 2) currentIndex = 1; // сразу выбираем 1й параметр, если всего 1 параметр
      }
      property var guid: "param-name"

      function saveValue() {
        return model[ currentIndex ];
      }
      
      property var valueToLoad

      onValueToLoadChanged: refind();

      function refind() {
        if (!valueToLoad) return;

        var ff = model.indexOf( valueToLoad );
        if (ff >= 0) {
          currentIndex = ff;
          valueToLoad = undefined;
        }
        recompute();
      }
    }

    params: [t1, cb1, p,btRe]

    TextEditCode {
      title: "Функция. Выход: значение, который Вы присваиваете в выбранный параметр."
      value: prg.generate.toString(); /// todo : choseparam.desiredGenerateCode
      id: p
      guid: "code"

      Text {
        id: status
        text: "Статус выполнения: " + statusMsg
        color: text.indexOf("error") >= 0 ? "red" : "green";
      }
    }

    Button {
      text: "Пересчитать"
      property var guid: "recomputebt"
      onClicked: recompute()
      id: btRe
    }

    property var preobrCode: p.value
    property var statusMsg: "ok"

    onPreobrCodeChanged: recompute()

    function evalUserPreobr() {
      try {
        //console.log("preobrCode=",preobrCode);
        var preobrFunc = eval( "("+preobrCode+")" )
        //console.log( "preobrFunc=",preobrFunc );
        var res = preobrFunc();
        
        statusMsg = "хорошо";
        return res;
      } catch(e) {
        statusMsg = "ошипка: " +e.message;
        console.error(e);
        return null;
      }
    }

    function recompute() {
      var newv = evalUserPreobr();
      
      var tparam = qq[ cb1.currentIndex -1 ];
      wp.writeParamValue( tparam, newv );
    }

    ////////////////////////////////////////////
    ExtraResults { }

  ///////////////////////////////////////////////

  function templateOnSave( str, ip ) {
    str += "\n  "+extraFeel.toString().replace( "CLACLA", wp.get_type( prg.input ) )+ "\n";
    return str;
  }

  function extraFeel(obj) {
    return obj.input && wp.get_type(obj.input) == "CLACLA";
  }
}
