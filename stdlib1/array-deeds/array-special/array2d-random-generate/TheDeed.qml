
Deed {
  id: deed

  params: [cnt,cnt2] //,btn]
  icon: "случайное"

  Param {
    min: 0
    max: 10*1000
    id: cnt
    value: 100
    text: "Число строк"
    onValueChanged: if (inited) go();
  }

  Param {
    min: 1
    max: 1000
    id: cnt2
    value: 3
    text: "Число столбцов"
    onValueChanged: if (inited) go();
  }

  /*
  Button {
    id: btn
    text: "Поехали!"
    onClicked: go();
  }*/

  function go() {
      var q = [];
      var n = cnt.value;
      var n2 = cnt2.value;
      var r = 100;
      for (var i=0; i<n; i++) {
        var line = [];
        for (var j=0; j<n2; j++)
          line.push( Math.random()*r );
        q.push(line);
      }
      deed.input.arr = q;
  }

  property bool inited: false

  Component.onCompleted: {
    go();
    inited=true;
  }
}