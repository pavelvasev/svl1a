
Deed {
    id: deed
    robotIcon: "[соединить]"

    Input2 {
      id: in2
      //function feel( obj ) { return (wp.get_type( obj ) == "Array2dRobot") && obj !== resarr; }
    }
    params: [in2]
    //property var arr2: in2.result ? (in2.result.arr || []) : []

    Array2dRobot {
      arr: deed.input.arr.concat( in2.result ? (in2.result.arr || []) : [] )
      id: resarr
    }

    output: resarr
}
