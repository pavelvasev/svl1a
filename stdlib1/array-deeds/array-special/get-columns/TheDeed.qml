
Deed {
    id: deed
    robotIcon: "выбр-колонки"

    params: [p]

    TextParam {
      text: "Номера колонок через пробел (нумерация с 0)"
      value: "0 1 2"
      guid: "columns"
      id: p
    }

    property var result: {
      var a = deed.input;
      var arr = a.arr;
      var c1 = a.count;
      if (c1 == 0) return [];
      var r = [];

      var nums = p.value.split(/\s+/).map( function(it) { return parseInt(it); } );

      for (var i=0; i<c1; i++) {
        var subr = [];
        for (var j=0; j<nums.length; j++) subr.push( arr[i][ nums[j] ] );
        r.push( subr );
      }

      return r;
    }

    Array2dRobot {
      arr: result
    }
}
