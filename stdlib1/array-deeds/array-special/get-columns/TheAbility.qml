Ability {
  title: "Выбрать колонки"
  help: "Выдает массив, в котором только выбранные колонки исходного массива"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    return (wp.get_type( target_object_x ) == "Array2dRobot");
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}