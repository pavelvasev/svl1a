
Deed {
    id: deed
    robotIcon: "[загрузить]"

    Input2 {
      id: in2
      function feel( obj ) { return (wp.get_type( obj ) == "Array2dRobot") && obj !== deed.input; }
    }
    params: [in2]
    property var arr2: in2.result ? (in2.result.arr || []) : []

    onArr2Changed: deed.input.arr = arr2;
}
