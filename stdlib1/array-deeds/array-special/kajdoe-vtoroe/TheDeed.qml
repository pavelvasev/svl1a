
Deed {
    id: deed
    robotIcon: "[в]"

    params: [p]

    Param {
      text: "Брать каждую N-ю строку массива"
      
      min: 1
      max: 100
      value: 2
      id: p
    }

    property var result: {
      var a = deed.input;
      var arr = a.arr;
      var c1 = a.count;
      if (c1 == 0) return [];
      var q = p.value;
      var r = [];

      for (var i=0; i<c1; i+= q)
        r.push( arr[i].slice(0) );

      return r;
    }

    Array2dRobot {
      arr: result
    }
}
