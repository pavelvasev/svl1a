Ability {  
  title: "Загрузить массив из файла"
  help: "Считывает файл как csv в указанный массив"

  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    return (wp.get_type( target_object_x ) == "Array2dRobot");
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}