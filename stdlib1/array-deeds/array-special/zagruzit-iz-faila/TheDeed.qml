Deed {
    id: deed
    icon: "загрузить"

    setter: true

    params: fil.params.concat([cb])
    CheckBoxParam {
      id: cb
      text: "Преобразовать в числа"
      checked: true      
      width: 200
    }

    FileRobot {
      id: fil
    }

    TextLoader {
      id: ldr
      file: fil.file
      onOutputChanged: if (deed.input) deed.input.arr = deed.input.text2arr( ldr.output )
    }

    onInputChanged: ldr.outputChanged();
}
