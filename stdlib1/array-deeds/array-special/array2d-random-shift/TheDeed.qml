
Deed {
  id: deed

  params: [rparam,cnt,btn]
  icon: "сл. сдвиг"

  Param {
    min: 0
    max: 10
    step: 0.1
    id: rparam
    value: 3
    text: "Радиус сдвига"
  }

  Param {
    min: 0
    max: 10*1000
    id: cnt
    value: 100
    text: "Счетчик сдвига"
    onValueChanged: go();
  }

  Button {
    id: btn
    text: "Поехали!"
    onClicked: go();
  }

  function go() {
      if (!deed.input) return;
      var arr = deed.input.arr;
      var n = deed.input.count;
      var n2 = deed.input.count2;
      var r = rparam.value;
      for (var i=0; i<n; i++) {
        for (var j=0; j<n2; j++)
          arr[i][j] += (Math.random() - 0.5)*r;
      }
      deed.input.arr = deed.input.arr;
  }

  Component.onCompleted: go();

}