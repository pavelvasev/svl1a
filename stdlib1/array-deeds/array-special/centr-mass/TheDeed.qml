
Deed {
    id: deed
    robotIcon: "[ц]"

    property var medium: {
      var a = deed.input;
      var arr = a.arr;
      var c1 = a.count;
      if (c1 == 0) return [];

      var c2 = a.count2;
      var centr = a.arr[0].slice(0);

      for (var i=1; i<c1; i++) 
        for (var j=0; j<c2; j++) 
          centr[j] += arr[i][j];

      for (var j=0; j<c2; j++) 
        centr[j] /= c1;

      return centr;
    }

    Array2dRobot {
      arr: [medium]
    }
}
