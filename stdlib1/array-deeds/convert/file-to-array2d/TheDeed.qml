Deed {
    id: deed

    params: [cb]
    CheckBoxParam {
      id: cb
      text: "Преобразовать в числа"
      checked: true      
    }

    TextLoader {
      id: ldr
      file: deed.input.file
      //onFileChanged: console.log(333, file, deed.input );
      onOutputChanged: ar.arr = ar.text2arr( ldr.output )
    }

    Array2dRobot {
      id: ar
      numeric: cb.checked
      onNumericChanged: ar.arr = ar.text2arr( ldr.output )
    }

    output: ar
}
