Deed {
    id: deed

    details: "Размерность массива: "+input.count + "x" + input.count2

    setter: true
    icon: "редактор"

    params: [co]

    property var inpos : input.arr || []
    
    property bool hasUserEnteredValue: false

    // todo не делать преобразований если параметр невидим. а то это очень дорого.
    property bool noWriteBack: false
    onInposChanged: {
      if (noWriteBack) return;
      hasUserEnteredValue = false;

      if (inpos.length > 5000)
        txt.text = "... слишком много чисел ("+inpos.length+">5000), редактор отключен.. ";
      else
        txt.text = input.arr2text( inpos );
    }

    Column {
        id: co
        width: 300
        spacing: 8
        visible: false
        property alias value: txt.text
        property var guid: "text"        

        function loadValue(v) {
          if (!deed.input) return;

          noWriteBack = true;
          deed.input.arr = deed.input.text2arr( v )
          var q = inpos; // надо вызвать, шобы биндинга сработала
          noWriteBack = false;          

          txt.text = v;
          hasUserEnteredValue = true;
        }

        function saveValue() {
          if (!hasUserEnteredValue) return undefined;
          return txt.text;
        }

        Text {
          text: deed.input.numeric ? "Числа через пробел" : "Данные через пробел"
        }

        TextEdit {
            height: 300
            width: parent.width
            id: txt
        }

        Button {
            text: "ВВОД"
            width: 150
            onClicked: {
              noWriteBack = true;
              deed.input.arr = deed.input.text2arr( txt.text );
              noWriteBack = false;
              hasUserEnteredValue = true;
            }
        }
    }
}
