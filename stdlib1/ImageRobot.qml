/* doc/2016-08-17-image1.md
*/

Robot {
    id: rb

    icon: "fa-picture-o"
    details: "размеры "+imageWidth + "x" + imageHeight

    Image {
        id: imga
        width: 500

        onWidthChanged: adjustHeight( imga );

        property var guid: "qmlimage"
        property bool saveDisabled: true

        Text {
            text: "<a href='" + imga.source + "' target='_blank'>Открыть в окне</a>"
            anchors.right: parent.right
            anchors.top: parent.bottom+2
        }

    }

    property var imageDom: imga.dom.firstChild
    property var imageWidth: imga.sourceSize.width
    property var imageHeight: imga.sourceSize.height
    property var source

    onSourceChanged: {
        if (source && !source.indexOf) { // значит blob или file прислали
            try {
              source = window.URL.createObjectURL( source );
            } catch(er) {
              console.error(er);
            }
            return;
        }
        imga.source = source;
    }

    onImageHeightChanged: {
        // console.log("imageHeight changed to",imageHeight);
        adjustHeight( imga )
    }

    property bool addis: false // флаг рекурсии для adjustHeight()
    function adjustHeight(qmlimg) {

        if (addis) return;

        var img = qmlimg.dom.firstChild;

        var screenW = imga.width;

        // var natW = img.naturalWidth; // исходное
        var natW = qmlimg.sourceSize.width;
        var ratio = screenW / natW; // соотношение экран/исходное
        var hh = qmlimg.sourceSize.height * ratio; // naturalH will be updated upon image load

        if (hh > 400) {
            var r2 = hh / 400;
            addis = true;
            qmlimg.width = screenW / r2;
            qmlimg.height = hh / r2;
            addis = false;
        }
        else {
            qmlimg.height = hh;
        }
    }


    function getctx() {
        if (!rb.icanvas) {
            rb.icanvas = document.createElement('canvas');
            rb.icontext = icanvas.getContext('2d');
        }
        return rb.icontext;
    }

    /////////////////////////////////
    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Pixel_manipulation_with_canvas
    Item {
        id: imgdatap

        property var guid: "imagedata"
        property var saveDisabled: true

        property int changeCounter: 0

        function loadValue(v) {
            if (!v || !v.width || !v.height) return;

            try {
                //        console.log("putting data, v.width=",v.width,"v.height=", v.height);

                var ctx = getctx();
                //        var canvas = document.createElement('canvas');
                //        var ctx = canvas.getContext('2d');

                ctx.canvas.width = v.width;
                ctx.canvas.height = v.height;

                imageDom.naturalWidth = v.width;
                imageDom.naturalHeight = v.height;
                imageDom.width = v.width;
                imageDom.height = v.height;

                ctx.putImageData( v,0,0 ); // , 0,0, v.width, v.height );

                var dataURL = ctx.canvas.toDataURL();

                rb.source = dataURL;
                changeCounter = changeCounter+1;
            }
            catch(er) {
                console.error("ImageRobot loadValue:",er);
            }
        }

        function saveValue() {
            if (changeCounter < 0) return;
            if (imageWidth == 0 || imageHeight == 0) return { width: 0, height: 0, data: []};

            //var canvas = document.createElement('canvas');
            //var ctx = canvas.getContext('2d');

            var ctx = getctx();
            ctx.canvas.width = imageWidth;
            ctx.canvas.height = imageHeight;
            ctx.drawImage( imageDom, 0, 0 );

            try {
                var myData = ctx.getImageData(0, 0, imageWidth, imageHeight );
                return myData;
            }
            catch(er) {
                console.error("ImageRobot saveValue:",er);
                return { width: 0, height: 0, data: []};
            }
        }
    }

    Item {
        id: arrport

        property var guid: "arr4"
        property var saveDisabled: true

        function saveValue() {
            var vv = imgdatap.saveValue();
            var res = [];
            for (var yy=0; yy<vv.height; yy++)
              res.push( vv.data.slice( yy * vv.width * 4, (yy+1) * vv.width * 4) );
            return res;
        }

        function loadValue(v) {
            var ctx = getctx();
            if (v.length == 0 || !v || v[0].length == 0) {
                console.log("ImageRobot arr4 loadValue; input value is bad, making 0-image",v)

                imageDom.naturalWidth = 0;
                imageDom.naturalHeight = 0;
                imageDom.width = 0;
                imageDom.height = 0;
                return;
            }

            var ww = v[0].length /4;
            var hh = v.length;
            var im = ctx.createImageData( ww, hh );
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Typed_arrays

            for (var yy=0; yy<hh; yy++)
                im.data.set( v[yy], yy*ww*4);

            imgdatap.loadValue(im);
            im = null;
        }
    }

    params: [imga,imgdatap, arrport]
}
