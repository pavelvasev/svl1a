Deed {
    id: deed
    icon: "загрузить"

    setter: true

    params: fil.params.concat([])
    property bool paramsSaveDisabled: true

    FileRobot {
      id: fil
      onFileChanged: f()
    }

    function f() {
      if (!deed.input) return;
      deed.input.source = fil.file;
    }

    onInputChanged: f()
}
