Preset {
  title: "Прозрачность сверху-вниз"
  help: ""
  helpurl: ""
  targetAbility: "transform-object"

  function extraFeel(obj) {
    return obj.input && wp.get_type(obj.input) == "ImageRobot";
  }

  function generate(inp) {
  for (var i=0; i<inp.width; i++) 
  for (var j=0; j<inp.height; j++) {
    inp.data[(j*inp.width + i)*4 +3] = (255 - 255 * j / inp.height);
  }
  return inp;
}

  initParams: [{
    "param-name": "imagedata",
    "code": generate.toString(),
    "logicalName": ""
  }]
}
