Preset {
  title: "Переносы \\n"
  help: ""
  helpurl: ""
  targetAbility: "transform-object"

  function extraFeel(obj) {
    return obj.input && wp.get_type(obj.input) == "TextRobot";
  }

  function generate(inp) {
  return inp.replace(/(\\n)(?=(?:[^\"]|\"[^\"]*\")*$)/g, "\n");
}

  initParams: [{
    "param-name": "txt",
    "code": generate.toString(),
    "logicalName": ""
  }]
}
