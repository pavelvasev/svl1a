// Робот "Двумерный массив"

Robot {
    id: mas

    icon: "мас."
    details: "Размерность массива: "+count + "x" + count2 + " Формат: " + (numeric ? "числовой" : "текст")
    //type: "Array2dRobot"

    // двумерный массив
    property var arr: []

    // первая размерность - типа кол-ва точек, строк
    property var count: arr ? arr.length : 0
    // вторая размерность - кол-во колонок, чисел в одной точке
    property var count2: arr && arr.length > 0 ? arr[0].length : 0

    property bool numeric: true

    // сервисные функции
    property bool imarr2d: true

    // одномерный массив выдает из себя
    function toflatarray() {
      return arr2to1( arr );
    }

    function parseFloatOrCommaFloat(str) {
      return parseFloat( str.replace(",",".") );
    }

    // тест в массив
    function text2arr(t) {
        if (typeof t === "string" || t instanceof String) {
        }
        else
        {
          if (t.toString) t = t.toString();
          else
          return;
        }
        var tn = t.split("\n");
        var res = [];
        var nn = numeric;
        //console.log("nn=",nn);
        for (var i=0; i<tn.length; i++) {
            var nums = tn[i].trim().split(/\s+/);
            if (nums.length == 0) continue;
            if (nums.length == 1 && nums[0].length == 0) continue;

            res.push( nn ? nums.map( parseFloatOrCommaFloat ) : nums );
        }
        //console.log("----",res);
        return res;
    }

    // массив в текст
    function arr2text(a) {
        var res = "";
        for (var i=0; i<a.length; i++)
        {
            res += a[i].join(" ");
            res += "\n";
        }
        return res;
    }

    // одномерный массив a в двумерный массив. считается что в а идет nums чисел в строке
    function arr1to2( a, num ) {
      //if (!a) debugger;
      if (!a) return [];
      var res = [];
      for (var i=0; i<a.length; i+= num) {
        var line = [];
        for (var j=i; j<i+num; j++) line.push( a[j] );
        res.push( line );
      }
      return res;
    }

    // двумерный массив в одномерный, упаковывая все элементы подряд
    function arr2to1( a ) {
      //if (!a) return [];
      var res = [];
      for (var i=0; i<a.length; i++) {
        for (var j=0; j<a[i].length; j++)
          res.push( a[i][j] );
      }
      return res;
    }

    // преобразует нечто массиво-подобное в двумерный массив
    // если a = просто массив, то выдаст двумерный массив, составленный из 1 строки, которая равна a
    function arr2arr2( a ) {
      if (!a) return [];
      if (!Array.isArray(a)) return [[a]];
      if (Array.isArray(a[0])) return a;
      return [a];
      //return arr1to2( a, 1 );
    }

    function importArr( a ) {
      arr = arr2arr2( a );
    }

    function importSerialized( a ) {
      arr = arr2arr2( a );
    }

    Item {
      property var guid: "arr"
      property alias value: mas.arr
      property bool saveDisabled: true
      id: par

      function loadValue( v ) {
        arr = arr2arr2( v );
      }
    }
    params: [par]

}
