// Робот "Файл"

Robot {
  icon: "file"
  params: [param]
  property bool visual: false

  property alias file: param.file
  property alias files: param.files
  FileParam {
    id: param
    tag: ""
    visible: false
    guid: "file"
  }
}