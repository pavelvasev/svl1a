Deed {
  id: deed
  icon: "bb"

  BoxFromTrimesh {
    source: deed.input
    color: [0,0,1]
    add: deed.input.radius ? [2*deed.input.radius, 2*deed.input.radius, 2*deed.input.radius] : [0,0,0]
    id: box
  }

  Text {
    text: "Вычисленный размер:" + box.sizes.toString();
    id: t
  }
  params: [t]
}