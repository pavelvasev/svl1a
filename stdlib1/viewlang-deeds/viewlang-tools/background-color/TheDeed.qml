Deed {
  id: deed

  ColorParam {
    target: deed
    id: c1
  }

  property var color

  onColorChanged: {
    var c = color;
    var cc = new THREE.Color( c[0], c[1], c[2] );
    var mixOpacity = 1;
    threejs.renderer.setClearColor( cc, mixOpacity);
  }

  params: [c1]
  robotIcon: "фон"
}