Ability {
  id: ability
  title: "Цвет фона"
  
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    if (wp.get_type(target_object_x) == "Scene") return 1;
    return 0;
  }

  deedPath: Qt.resolvedUrl("TheDeed.qml")
}