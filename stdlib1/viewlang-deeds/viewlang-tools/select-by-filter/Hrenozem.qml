/* Решается задача создания подмножества визуального объекта.

   source - входной объект
   numbers - номера примитивов, которые надо скопировать.

   Объекты бывают безиндексные и на индексах.

   Объекты содержат поля
     positions
     colors
     indices?
     uvs?

     radius
     color

   Что еще надо учитывать? positionItemSize
   и еще strip или не strip объект
*/

Item {
  id: so
  property var source
  property var numbers: []

  Loader {
    id: loader
    source: so.source ? so.source.$class : null
    onLoaded: item.$class = so.source.$class;
  }

  property var output: loader.item

  Binding {
    target: output
    property: "positions"
    value: extractField( "positions", numbers, source ? source.positionItemSize || 3 : 3 );
    when: source
  }

  Binding {
    target: output
    property: "colors"
    value: extractField( "colors", numbers, 3 );
    when: source && source.colors && source.colors.length > 0
  }

  // индексы все копируем..?
  Binding {
    target: output
    property: "indices"
    value: extractField( "indices", numbers, Math.floor( ( source.positionItemSize || 3 ) / 3.0 ) );
    when: indexMode
  }

  property bool indexMode: source && source.incides && source.incides.length > 0

  Binding {
    target: output
    property: "radius"
    value: source ? source.radius : 1
    when: source && source.radius > 0
  }

  Binding {
    target: output
    property: "color"
    value: source ? source.color : [1,1,1]
    when: source && source.color
  }

  //////////////////////////////////////////////// 
  // name = positions, colors, ....
  function extractField( name, numbers, itemSize ) {
    if (!source) return [];
    /*
    var sdata = source.$properties[ name ];
    if (!sdata) return [];
    sdata = sdata.val;
    */
    var sdata = source[ name ];
    if (!sdata) return [];
    if (sdata.length == 0) return [];

    // в индексном режиме мы тупо все копируем. А индексы уже будем фильтровать.
    if (indexMode && name != "indices") return sdata;

    var res = [];
    for (var i=0; i<numbers.length; i++) {
      var j = numbers[i];

      if (itemSize * (j+1) <= sdata.length)
      for (var k=0; k<itemSize; k++)
        res.push( sdata[ itemSize * j + k] );
    }
    // console.log("hrenozem field=",name,"res=",res, "sdata=",sdata);

    return res;
  }

}