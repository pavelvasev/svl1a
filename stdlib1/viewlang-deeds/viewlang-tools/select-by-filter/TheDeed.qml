Deed {
  id: deed

  details: "Укажите в массиве номера примитивов, которые следует оставить."

  Hrenozem {
    source: deed.input
    numbers: numbersArr.toflatarray();
    id: hr
  }

  Array2dRobot {
    id: numbersArr
    risNewLine: true
    property var inputObj: deed.input
    arr: [[0,1,2]]
  }  

  PerformDeed {
    input: numbersArr
    name: "array2d-edit"
    parentToObject: true    
  }

  PerformDeed {
    input: deed.input
    name: "manage-object-opacity"
    parentToObject: true
  }

  /*
  OpacityParam {
    guid: "source_opacity"
    target: deed.input
    value: 60
    text: "Прозрачность исходного объекта"
    id: o1
  }
  params: [o1]
  */

/*
  PerformDeed {
    input: numbersArr
    name: "compute-array"
    parentToObject: true    
    options: [ { inputObj: deed.input, initParams: {"code" : f1.toString()} } ]
  }
*/

/*
  function f1() {
    var count = inputObj.positions.length / 3;
    var res = [];
    for (var i=0; i<count; i+=2) {
     res.push( i );
    }
    return res;      
  }
  */

  /*
  Text {
    text: "Укажите номера функции в блоке вычислений для выдачи номеров примитивов, которые следует оставить. При вычислени доступена переменная inputObj - входной визуальный объект.\n\nЛибо установите значения номеров в массиве вручную."
    wrapMode: Text.WordWrap
    width: 400
    id: txt1
  }

  params: [txt1]
  */

  robotIcon: "фильтр"
}