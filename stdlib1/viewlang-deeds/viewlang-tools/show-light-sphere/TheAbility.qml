Ability {
  title: "Показать источник света"
  help: "Создает сферу по координатам источника света"
 
  function feel( target_object_x ) {
    if (wp.get_type(target_object_x) == "PointLight") return 1;
    return 0;
  }

  deedPath: Qt.resolvedUrl("TheDeed.qml")
}