Deed {
  id: deed

  Spheres {
    nx: 4
    ny: 4
    wire: true
    opacity: 0.5
    color: deed.input.color
    positions: deed.input.position
    radius: 2

    Spheres {
      color: deed.input.color
      positions: deed.input.position
      radius: 1
    }
  }

}