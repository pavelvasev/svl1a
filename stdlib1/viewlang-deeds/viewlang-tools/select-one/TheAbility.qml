Ability {
  title: "Выбор 1"
  help: "Позволяет указать номер примитива и высвечивает его отдельным цветом"

  id: ability
  
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    if (! (target_object_x.positions && target_object_x.positions.length && target_object_x.positions.length > 0)) return 0;

    return 1;
  }

  deedPath: Qt.resolvedUrl("TheDeed.qml")
}