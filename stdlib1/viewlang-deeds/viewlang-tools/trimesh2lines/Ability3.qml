Ability {
  id: ability

  title: "Треугольники в отрезки"
  help: "Показывает отрезками стороны треугольников"
  
  // открытый вопрос что на вход - один объект или набор. пока для упрощения один
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    var t = wonderfulplace.get_object_wonderful_type( target_object_x );
    if (t != "Trimesh" && t != "Triangles") 
      return 0;

    return 1;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}