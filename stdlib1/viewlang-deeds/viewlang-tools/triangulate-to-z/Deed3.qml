// uses https://github.com/pavelvasev/vl_triang

Deed {
  id: deed
  icon: "z"

  Triangulate {
    input: deed.input.positions
    id: tri
  }

  Trimesh {
    positions: deed.input.positions
    indices: tri.output
    color: [0,0.5,0]
  }

}