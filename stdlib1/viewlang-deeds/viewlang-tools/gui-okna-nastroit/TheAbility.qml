Ability {
  title: "Настроить польз. интерфейс"
  help: "Настраивает элементы пользовательского интерфейса"
 
  function feel( target_object_x ) {
    if (wp.get_type(target_object_x) == "Scene") return 1;
    return 0;
  }

  deedPath: Qt.resolvedUrl("TheDeed.qml")
}