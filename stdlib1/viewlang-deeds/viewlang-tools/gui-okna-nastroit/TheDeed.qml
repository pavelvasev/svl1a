Deed {
  id: deed
  icon: "настр ПИ"

  params: [cb1,cb2,cb3,cb4,in2,sctitle]

  CheckBox {
    property var guid: "robots"
    property alias value: cb1.checked

    text: "Карта роботов"
    
    checked: showRobotsParam.checked
    onCheckedChanged: showRobotsParam.checked = checked;
    id: cb1
    width: 250
  }

  CheckBox {
    property var guid: "bottomw"
    property alias value: cb2.checked

    text: "Показать нижний ряд"
    
    checked: qmlEngine.rootObject.bottomWidgets.visible
    onCheckedChanged: qmlEngine.rootObject.bottomWidgets.visible = checked;
    id: cb2
    width: 250
  }

  CheckBox {
    property var guid: "toolbarw"
    property alias value: cb3.checked

    text: "Показать верхний ряд"
    
    checked: qmlEngine.rootObject.toolbarWidgets.visible
    onCheckedChanged: qmlEngine.rootObject.toolbarWidgets.visible = checked;
    id: cb3
    width: 250
  }  

  CheckBox {
    property var guid: "toplinks"
    property alias value: cb4.checked

    text: "Показать верхние ссылки"
    
    checked: true
    onCheckedChanged: {
      var els = jQuery("#viewlanglink, #togglelink, #infoDark");     
      checked ? els.show() : els.hide();
    }
    id: cb4
    width: 250
  }  

  /*
  ColorParam {
    text: "Цвет роботов"
    color: hex2tri( risovanie.robotColor )
    onColorChanged: risovanie.robotColor = tri2hex( color )
    id: col1
    target: null
  } 
  */
  

/////////////////////////////////////
  Input2 {
    id: in2
    text: "Показать окно на старте программы:"
    guid: "show_window_on_start"

    onResultChanged: console.log(" XXXXXXXXXXXXXXX in2.result=",in2.result);

    function feel( obj ) { 
      if (obj && obj.params && obj.params.length > 0) return true;
      if (obj && wp.get_type(obj) === "Interface") return true;
      return false;
    }
  }

  TextParam {
    guid: "scenetitle"
    text: "Заголовок сцены"
    id: sctitle
    onValueChanged: qmlEngine.rootObject.text = value
  }

  Component.onCompleted: {
    if (in2.result) {
      if (deed.performedByChainDeed) 
      {
        if (deed.performedByChainDeed.isFinished) showin(); else deed.performedByChainDeed.finished.connect( deed, showin )
      }
      else {
        console.error("gui-okna-nastroit: performedByChainDeed что-то не установлен.. как же так? юзаю таймер теперь..")
        setTimeout( showin, 1000 );
      } 
    }
  }

  function showin() {
    if (in2.result) risovanie.activate( in2.result );
  }

}