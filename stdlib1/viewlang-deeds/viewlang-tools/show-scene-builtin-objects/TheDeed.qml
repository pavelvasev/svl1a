Deed {
  id: deed
  icon: "встроенные"

  Text {
    id: t1
  }
  Button {
    text: "log"
    onClicked: console.log(deed);
    id: b1
  }
  params:[t1,b1]

  Component.onCompleted: {
    add( qmlEngine.rootObject.light0 );
    add( qmlEngine.rootObject.light1 );
    add( qmlEngine.rootObject.light2 );

    qmlEngine.rootObject.axes.visual = true;
    qmlEngine.rootObject.axes.visible = true;
    add( qmlEngine.rootObject.axes );

    for (var c=0; c<qmlEngine.rootObject.axes.children.length; c++) {
      reg( qmlEngine.rootObject.axes.children[c] );
    }
    
    deed.childrenChanged();
  }

  function add(item) {
    item.parent = deed;
    reg(item);
  }

  function reg(item) {
    var event = new CustomEvent('componentOnCompleted', { 'detail': item });
    window.dispatchEvent(event);
  }

}