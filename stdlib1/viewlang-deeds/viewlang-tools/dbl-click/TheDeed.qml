// Идеи. У тримеша показывать треугольник. И еще показывать всю трассут точек встречи. Но тогда надо и все объекты показать...
/*
  Разобраться с режимами. Что нам и когда надо. А щас понатыкано все как-то абы как.
*/

Deed {
    id: deed
    icon: "дв. клик"

    params: [cb0,spPosParam, cb1,cb2, cb11it]

    Spheres {
      id: sp
      opacity: 0.75
      radius: 0.5
      //positions: r1.curIntersect ? pt2arr( r1.curIntersect.point ) : []
      //color: r1.curIntersect ? r1.curIntersect.object.qmlParent.color : [1,0,0]
      color: [1,1,0]
    }

    Item {
      property var guid: "sp-coord"
      property alias value: sp.positions
      id: spPosParam
    }

    Spheres {
      id: sp2
      opacity: 1
      radius: 0.25
      color: [1,0,0]
      positions: sp.positions      
      visible: sp.visible
    }

    CheckBoxParam {
      guid: "showai"
      checked: true
      text: "Показывать надпись в левой колонке"
      width: 300
      id: cb0
    }

    CheckBoxParam {
      guid: "showsp"
      onCheckedChanged: sp.visible = checked;
      checked: sp.visible
      text: "Подсвечивать места кликов сферой"
      width: 300
      id: cb1
    }

    CheckBoxParam {
      guid: "showinfo"
      checked: true
      text: "Показывать доп. информацию"
      width: 300
      id: cb2
    }    

    Component.onCompleted: {
        renderer.domElement.addEventListener( 'dblclick', sceneDblClick2, false );
        qmlEngine.rootObject.refineAll();
    }

    Component.onDestruction: {
        renderer.domElement.removeEventListener( 'dblclick', sceneDblClick2, false );
    }

    function sceneDblClick2( event ) {
        //  console.log("ddd ",sceneMouse,event);
        //if (!cb11.checked) return false;

        raycaster.setFromCamera( sceneMouse, camera );

        raycaster.params.Points.threshold = 0.25;
        
        raycaster.params.Sprite.threshold = 0.25;
        raycaster.linePrecision = 0.25;

        // calculate objects intersecting the picking ray
        var intersects = raycaster.intersectObjects( scene.children,true );

        // убираем клики на сферы выбора
        if (intersects.length > 0 && intersects[0].object && intersects[0].object.qmlParent)
          if (intersects[0].object.qmlParent.parent === sp || intersects[0].object.qmlParent.parent === sp2)
            intersects.shift();
        if (intersects.length > 0 && intersects[0].object && intersects[0].object.qmlParent)
          if (intersects[0].object.qmlParent.parent === sp || intersects[0].object.qmlParent.parent === sp2)
            intersects.shift();

        // убираем назад спрайты, а то они большие по клику
        if (intersects.length > 0 && intersects[0].object && intersects[0].object.qmlParent)
           if (intersects[0].object.qmlParent.$class == "TextSprite") moveon( intersects );

        var intersect = intersects[0];
        //console.log(intersect.object, intersect.indices );

        if (intersect && intersect.object && intersect.object.qmlParent) {
            //if (cb11.checked)
            //  risovanie.activate( objOrNest( intersect.object.qmlParent ) );
            //else 
            {
              //risovanie.curHiliteObj = objOrNest( intersect.object.qmlParent ); 
               sp.positions = [ intersect.point.x, intersect.point.y, intersect.point.z ];
               sp.color = intersect.object.qmlParent.color || [1,1,0];
            }
        }
        else
        {
            //sp.positions = [];
        }

        var res = [];
        var res2 = [];
        for ( var i=0; i< intersects.length; i++ ) {
          var intersect =intersects[i];
            //intersect.object.material.color = new THREE.Color( 0xff0000 );          
          if (intersect.object && intersect.object.qmlParent) {
            var oo = objOrNest( intersect.object.qmlParent );
            if (res.indexOf( oo ) < 0) {
              res.push( oo );
              res2.push( intersect );
            }
          }
        }
        r1.items = res;
        r1.items2 = res2;
    }

    CheckBoxParam {
      text: "Двойной клик - выбор объекта"
      id: cb11
      tag: "left"
      width: 250
      //onCheckedChanged: if (!checked) tclear.clicked();
      checked: true
      //visible: cb0.checked
      visible: false
    }

    Item {
      property var guid: "click-activate"
      property alias value: cb11.checked
      id: cb11it
    }

    Column {
      id: c0
      visible: cb2.checked
      property var tag: "left"

      Text {
        text: "\nПересечение:"
        visible: r1.items.length > 0
      }
    
    Row {
      id: r1
      property var items: []
      property var items2: []
      property var curItemIndex: { var i = items.indexOf( risovanie.curHiliteObj ); if (i >=0) return i; return r1.items.length > 0 ? 0 : -1; }
      property var curIntersect: items2[ curItemIndex ];

      onCurIntersectChanged: {
        console.log( "Current intersect = ",curIntersect );
        sp.positions = r1.curIntersect ? pt2arr( r1.curIntersect.point ) : [];
        sp.color = r1.curIntersect ? r1.curIntersect.object.qmlParent.color : [1,0,0];
      }
      spacing: 7

      Repeater {
        model: r1.items.length

        PaintRobot {
          robot: r1.items[index]
          forceSingle: true
        }
     }

    }

    Column {
      id: t1
      visible: !!r1.curIntersect

      Text {
        text: r1.curIntersect ? "Точка встречи "+pt2str( r1.curIntersect.point ) : (sp.positions || []).join(" ")
      //text: sp.positions && sp.positions.length > 0 ? "Пересечение: " + sp.positions.join(" ") : ""
      }
      Text {
        visible: r1.curIntersect && (typeof(r1.curIntersect.index) !== "undefined")
        text: visible ? "Номер примитива " + r1.curIntersect.index : ""
      }
      Text {
        visible: r1.curIntersect && (typeof(r1.curIntersect.faceIndex) !== "undefined" && r1.curIntersect.faceIndex != null)
        text: visible ? "Номер грани " + r1.curIntersect.faceIndex : ""
      }
      Text {
        visible: r1.curIntersect && r1.curIntersect.face
        text: visible ? "Номера вершин " + r1.curIntersect.face.a + " " + r1.curIntersect.face.b + " " + r1.curIntersect.face.c : ""
      }
    }

/*
    TextButton {
      id: tclear
      text: r1.items.length > 0 ? "Очистить" : " "
      onClicked: {
        r1.items = [];
        r1.items2 = [];
      }
    }
*/
    
    }

    function pt2arr(pt) {
      return [ pt.x, pt.y, pt.z];
    }

    function pt2str(pt) {
      return [ pt.x.toFixed(7), pt.y.toFixed(7), pt.z.toFixed(7)].join(" ");
    }

    function moveon(arr) {
      var it = arr.shift();
      arr.push( it );
    }

    function objOrNest(obj) {
      if (obj && obj.nesting) return obj.parent;
      return obj;
    }

}
