Ability {
  title: "Двойной клик"
  help: "Двойной клик по 3д сцене выбирает объект"
 
  function feel( target_object_x ) {
    if (!target_object_x) return 1;
    if (wp.get_type(target_object_x) == "Scene") return 1;
    return 0;
  }

  deedPath: Qt.resolvedUrl("TheDeed.qml")

  robotLayer: "system"
}