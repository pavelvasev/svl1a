Deed {
  id: deed

  robotIcon: "радиус"

  RadiusParam {
    //text: wonderfulplace.get_object_user_name( target ) + " radius"
    text: "Radius"
    target: deed.input
    property var tag: "left"
    //parent: underscene
    step: 0.1
    id: r1
    bigCase: true // для текста
    //textEnabled: true
  }
  params: [r1]

  property alias radius: r1.value
  //onRadiusChanged: console.log(">>>>>>>>>>>>>>>>>>>>>> radius=",radius);
}