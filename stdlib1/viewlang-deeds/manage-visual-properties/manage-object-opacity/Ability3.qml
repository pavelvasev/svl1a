Ability {
  title: "Задать прозрачность"
  
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    if (typeof(target_object_x.opacity) == "undefined") return 0;
    if (!target_object_x.positions) return 0;
    if (!target_object_x.visual) return 0;

    return 1;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}