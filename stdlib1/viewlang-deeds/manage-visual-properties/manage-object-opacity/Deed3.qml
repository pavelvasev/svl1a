Deed {
  id: deed
  property var atest: 14

  robotIcon: "прозрачность"
  params: [p1]
  OpacityParam {
    text: wonderfulplace.get_object_user_name( deed.input ) + " opacity"
    target: deed.input
    property var tag: "left"
    parent: underscene
    guid: "opacity"
    id: p1
  }
}