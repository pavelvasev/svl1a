Deed {
  id: deed

  robotIcon: "xyz"
  params: [p1,p2,p3]
  

  onInputChanged: {
    p1.value = deed.input.center[0];
    p2.value = deed.input.center[1];
    p3.value = deed.input.center[2];
  }

  function up( index, val )
  {
     if (!deed.input) return;
     var q = [deed.input.center[0], deed.input.center[1], deed.input.center[2] ];
     q[index] = val;
     deed.input.center = q;
  }

  property var un: wonderfulplace.get_object_user_name( deed.input )

  Param {
    text: un+" X"
    max: 100
    min: -100
    onValueChanged: up( 0, p1.value ) 
    id: p1
    bigCase: true
    guid: "x"
  }
  
  Param {
    text: un+" Y"
    max: 100
    min: -100
    onValueChanged: up( 1, p2.value )
    id: p2
    bigCase: true
    guid: "y"
  }

  Param {
    text: un+" Z"
    max: 100
    min: -100
    onValueChanged: up( 2, p3.value ) 
    id: p3
    bigCase: true
    guid: "z"
  }
 
}