Ability {
  title: "Задать систему координат"
  
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    var t = wp.get_type(target_object_x);
    if (t == "Scene") return 0;
    return target_object_x.$properties && target_object_x.$properties["center"] ? 1 : 0;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}