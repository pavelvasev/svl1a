Deed {
  id: deed

  robotIcon: "масштаб"
  params: [p1]
  Param {
    text: wonderfulplace.get_object_user_name( deed.input ) + " scale"
    guid: "scale"
    id: p1
    max: 10
    min: 0.1
    step: 0.05
    value: 1
    //bigCase: true
    onValueChanged: move(); 
  }

  onInputChanged: move();

  function move() {
    var v = p1.value;
    deed.input.scale = v;
  }
}