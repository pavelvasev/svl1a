Ability {
  title: "Задать масштаб"

  function feel( obj ) {
    if (!obj) return 0;
    if (obj.$class == "Scene") return 0;
    if (obj.visual && obj.$properties["scale"] && obj.$properties["color"]) return 1;
    return 0;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}