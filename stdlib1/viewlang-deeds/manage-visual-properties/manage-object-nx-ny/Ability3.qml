Ability {
  title: "Задать nx ny"
  
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    if (! (target_object_x.nx >= 0)) return 0;
    
    return 1;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}