import "../.."

Deed {
  id: deed
  property var atest: 14

  robotIcon: "nx ny"
  params: [p1,p2]

  Param {
    text: wonderfulplace.get_object_user_name( deed.input ) + " NX"
//    parent: underscene
    max: 20
    value: deed.input.nx
    id: p1
  }

  Param {
    //visible: deed.input.ny > 0
    text: wonderfulplace.get_object_user_name( deed.input ) + " NY"
//    parent: underscene
    max: 20
    value: deed.input.ny ? deed.input.ny : ""
    id: p2
  }

  Binding {
    target: deed.input
    property: "nx"
    value: p1.value
  }

  Binding {
    target: deed.input
    property: "ny"
    value: p2.value
    when: p2.visible
  }
 
  Component.onCompleted: {

    underscene.refineAll();
  }
 
}