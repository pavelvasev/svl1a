Ability {
  title: "Задать видимость"
  
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    if (target_object_x.$class == "Scene") return 0;
    if (target_object_x && target_object_x.$properties && target_object_x.$properties["visible"] && target_object_x.visual && target_object_x.$properties["color"]) return 1;
    
    return 0;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}