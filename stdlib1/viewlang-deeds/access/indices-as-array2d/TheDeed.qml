Deed {
    id: deed

    CheckBoxParam {
      id: cb
      text: "При изменении массива передавать значения обратно"
      guid: "writeback"
      width: 300
      checked: true
    }
    params: [cb]

    Array2dRobot {
      id: ar
      onArrChanged: {
        if (!stopgo && cb.checked) {
          stopgo = true;
          deed.input.indices = ar.arr2to1( ar.arr );
          stopgo = false;
        }
      }
    }

    property var stopgo: false
    property var indices: deed.input.indices

    onPositionsChanged: {
      if (!stopgo) {
        stopgo = true;
        ar.arr = ar.arr1to2( indices, deed.input.positionItemSize || 3 );
        stopgo = false;
      }
    }
}
