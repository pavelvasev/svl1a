Deed {
    id: deed

    CheckBoxParam {
      id: cb
      text: "При изменении массива передавать значения обратно"
      checked: true
      guid: "writeback"
      width: 300
    }
    params: [cb]

    Array2dRobot {
      id: ar
      onArrChanged: {
        if (!stopgo && cb.checked) {
          stopgo = true;
          deed.input.radiuses = ar.arr2to1( ar.arr );
          stopgo = false;
        }
      }
    }

    property var stopgo: false
    property var radiuses: deed.input.radiuses

    onPositionsChanged: {
      if (!stopgo) {
        stopgo = true;
        ar.arr = ar.arr1to2( radiuses ) ////, (deed.input.positionItemSize || 3)/3 );
        stopgo = false;
      }
    }
}
