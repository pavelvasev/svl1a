Ability {
  title: "Массив радиусов"
  help: "Предоставляет доступ к радиусам как к массиву чисел"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    return target_object_x && target_object_x.$properties && target_object_x.$properties["radiuses"] ? 1 : 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}