Ability {
  title: "Массив координат"
  help: "Предоставляет доступ к координатам как к массиву чисел"
  icon: "мк"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    return target_object_x && target_object_x.$properties && target_object_x.$properties["positions"] ? 1 : 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}