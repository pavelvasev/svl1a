Deed {
    id: deed
    //details: "Кол-во примитивов: "+input.positions.length
    // positionItemSize

    setter: cb.checked

    CheckBoxParam {
      id: cb
      text: "При изменении массива передавать значения обратно в positions"
      checked: true
      guid: "writeback"
      width: 300
    }
    params: [cb]

    Array2dRobot {
      id: ar
      onArrChanged: {
        if (!stopgo && cb.checked) {
          stopgo = true;
          deed.input.positions = ar.arr2to1( ar.arr );
          stopgo = false;
        }
      }
    }

    property var stopgo: false
    property var positions: deed.input.positions

    onPositionsChanged: {
      if (!stopgo) {
        stopgo = true;
        ar.arr = ar.arr1to2( positions, (deed.input.positionItemSize || 3) );
        stopgo = false;
      }
    }
}
