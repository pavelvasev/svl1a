Ability {
  title: "СК как массив"
  help: "Предоставляет доступ к системе координат робота как к массиву чисел"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    if (!target_object_x) return 0;
    if (target_object_x.$class == "Scene") return 0;
    return target_object_x && target_object_x.$properties && target_object_x.$properties["center"] ? 1 : 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}