Deed {
    id: deed

    details: "в массиве 3 числа - x y z задающие систему координат. Если кому-то надо, можно сделать и повороты."

    CheckBoxParam {
      id: cb
      text: "При изменении массива передавать значения обратно"
      checked: true
      guid: "writeback"
      width: 300
    }
    params: [cb]

    Array2dRobot {
      id: ar
      onArrChanged: {
        if (!stopgo && cb.checked) {
          stopgo = true;
          console.log("sending to center ar.arr[0]=",ar.arr[0]);
          deed.input.center = ar.arr[0];
          stopgo = false;
        }
      }
    }

    property var stopgo: false
    property var center: deed.input.center

    onCenterChanged: {
      if (!stopgo) {
        stopgo = true;
        ar.arr = [center];
        stopgo = false;
      }
    }
}
