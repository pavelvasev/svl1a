
Deed {
    id: deed

    CheckBoxParam {
      guid: "zamknut"
      id: cb1
      text: "Соединить первую и последние точки"
      width: 300
    }
    params: [cb1]

    Linestrip {
      positions: {
        var flat = deed.input.toflatarray();

        if (cb1.checked)
        {
          if (flat.length > 3)
            return flat.concat( [flat[0], flat[1], flat[2]] );
        }

        return flat;
      }
    }
}
