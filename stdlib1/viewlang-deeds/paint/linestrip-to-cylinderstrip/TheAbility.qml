Ability {
  title: "Нарисовать цилиндрами"
  
  // условие проверки, может ли это действие сработать на объекте target_object_x
  function feel( target_object_x ) {
    return (wp.get_type( target_object_x ) == "Linestrip");
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}