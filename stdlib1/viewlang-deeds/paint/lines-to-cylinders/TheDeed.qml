
Deed {
    id: deed

    Cylinders {
      positions: deed.input.positions
      radius: 0.3
      colors: deed.input.colors
      color: deed.input.color
    }
}
