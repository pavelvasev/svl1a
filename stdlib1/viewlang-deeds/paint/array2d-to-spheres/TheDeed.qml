
Deed {
    id: deed

    Spheres {
      id: sp
    }

    property var q: deed.input.arr
    onQChanged: sp.positions = deed.input.toflatarray();

    output: sp
}
