
Deed {
    id: deed

    Arrows {
      positions: deed.input.positions;
      //radius: deed.input.radius > 0 ? deed.input.radius: 0.3
      radius: 0.3
      colors: deed.input.colors
      color: deed.input.color
    }
}
