// Кнопочка, вызывающая действие

Button {
    id: btn
    width: 150
    text: name

    property var tag: "right"    
    property var name: "waiting.."

    property alias object: rundlg.object
    property alias input: rundlg.object
    property alias options: rundlg.options
    property alias once: rundlg.once
    property alias activateDlg: rundlg.activateDlg
    property alias parentToObject: rundlg.parentToObject

    onClicked: rundlg.perform();

    signal performedGood( object obj );
    property alias outputDeed: rundlg.outputDeed

    property var record: false

    PerformDeed {
      name: parent.name
      object: btn.object
      id: rundlg
      manual: true
      record: btn.record
      onPerformedGood: btn.performedGood(obj);
    }

    Component.onCompleted: vlwindow.refineAll();
    // м.б переделать - PerformDeed и в нем уже Button
}
