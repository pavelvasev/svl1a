// Вызывает сигнал completed при создании каждого объекта в сцене

Item {
  property bool locateExisting: true

  signal created( object obj );

  Component.onCompleted: {
   window.addEventListener('componentOnCompleted', function (e) {
     created( e.detail );
   }, false);

   // на этапе инициализации если мы - то completed у всех вызовут. нам нужен только режим running
   // хотя блин не факт... наш completed может сработать после того как другие completed вызовут.. ладно разберемся.
   // console.log( "qmlEngine.operationState=",qmlEngine.operationState);
   if (qmlEngine.operationState !== QMLOperationState.Init && qmlEngine.operationState !== QMLOperationState.Idle) {
     if (locateExisting) {
       refresh();
     }
   }
  }

  function refresh() { // это, однако, насилие
    wp.qml_tree_walk( qmlEngine.rootObject, function (o) { created( o ); return true; } );
  }
}