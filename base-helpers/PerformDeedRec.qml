// Как PerformDeed, только с указанием что это действие надо будет запоминать.
// Конечно в core PerformDeedRec не место, но он пораньше нужен

PerformDeed {
  id: deed

  record: (name != "delete-robot")

/*  onPerformedGood: {
    if (name != "delete-robot") {
      outputDeed.record = true;
    }
  }
*/
}