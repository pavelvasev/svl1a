Deed {
  id: deed
  details: "LoadAbilityQml " + deed.input + "\nloaded = "+(!!ldr.item)

  Loader {
    id: ldr
    source: deed.input
    onLoaded: {
      if (ldr.item.setNameFromFilename) {
        ldr.item.setNameFromFilename( source );
      }
      else
      {
        var c = source.split("/");
        item.name = c[ c.length-2 ];
        if (!item.category) item.category = c[ c.length -3 ];
        wp.abilityLoaded( item.name );
      }
    }
  }
}