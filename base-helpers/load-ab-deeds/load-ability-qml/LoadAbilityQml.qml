Ability {
  name: "load-ability"
  robotIcon: "qml"
  // robotLayer : "system"

  function feel( obj ) {
    if (typeof(obj) == "string" && /\.(qml|vl)$/.test(obj)) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}