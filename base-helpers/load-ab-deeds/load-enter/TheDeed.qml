Deed {
  id: deed

  details: "LoadAbilityEnter "+deed.input

  PerformDeed {
    name: "load-ability"
    input: urla
  }

  property var urla: (deed.input || "").replace(/\.enter$/,"")

  risClosed: true
}