Ability {
  name: "load-ability"
  robotIcon: "enter"
  // robotLayer : "system"

  function feel( obj ) {
    if (typeof(obj) == "string" && /\.enter$/.test(obj)) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}