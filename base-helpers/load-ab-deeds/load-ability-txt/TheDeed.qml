Deed {
  id: deed
  details: "LoadAbilityTxt "+deed.input

  TextLoader {
    file: deed.input
    id: ldr
  }

  property var files: ldr.output.split("\n").map( function(f) { return f.replace(/\\/g,"/").trim(); } ) // повернули слеши, убрали концевые пробелы
  property var goodfiles: files.filter( function(f) { return f.length > 0 && f.indexOf("-/") < 0; } )
  property var goodfiles2: goodfiles.filter( function(f) { return !/(core\/base\/Ability.qml)|(load-ab)/.test(f); } )
  
  property var urls: goodfiles2.map( function(f) { return f.indexOf("://") > 0 ? f : basedir + "/" + f } );
  property var basedir: { var s = deed.input.replace(/\\/g,"/").split("/"); s.pop(); return s.join("/"); }
  //onUrlsChanged: console.log("LoadAbilityTxt.qml ",wp.get_object_user_name(deed),"computed urls:",urls);

  Repeater {
    model: urls.length
    PerformDeed {
      name: "load-ability"
      input: urls[index]
    }
  }

  output: urls

  property bool risClosed: true
}