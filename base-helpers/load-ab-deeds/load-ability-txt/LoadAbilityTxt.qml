Ability {
  name: "load-ability"
  robotIcon: "la(txt)"
  // robotLayer : "system"

  function feel( obj ) {
    if (typeof(obj) == "string" && /(abilities|init)\.txt$/.test(obj)) return 1;
    if (typeof(obj) == "string" && /\.enter$/.test(obj)) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}