Deed {
  id: deed
  details: "LoadAbilityJson "+deed.input
  
  Text {
    text: "ability name: "+cl.output.name + "\nability category: "+cl.output.category + "\n loaded good: " + cl.output.loadedGood;
    id: t1 
  }

  params: [t1]

  ChainLoader {
    id: cl
    source: deed.input 

    output.historyMode: /\.(svl)\.json$/.test( deed.input )
    /* иногда мы хотим категорию из каталога, а иногда из первого действия. и как быть?..
    output.category: {
      var dirs = (cl.source || "").split("/");
      return dirs[ dirs.length - 2];
    }
    */
  }
}