Ability {
  name: "load-ability"
  robotIcon: "json"
  // robotLayer : "system"

  function feel( obj ) {
    if (typeof(obj) == "string" && /\.(ab)\.json$/.test(obj)) return 1;
    // добавить проверку файлов
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}