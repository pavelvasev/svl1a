Ability {
  name: "load-ability"
  robotIcon: "la(a)"
  robotLayer : "system"

  function feel( obj ) {
    if (Array.isArray(obj)) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}