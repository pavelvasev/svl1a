Deed {
  id: deed
  details: "LoadAbilityJson "+deed.input + "\n"+status
  property var status : ""

  ChainLoader {
    source: deed.input
    output.historyMode: true
    output.category: "hidden"    

    id: cl
    onReadyChanged: {
      if (cl.ready) {
          var out = cl.output.act();
          if (out) {
            status= "Загружено";
            //out.destroy();
          }
      }
    }
    onLoadFailed: { status="Ошибка чтения-загрузки, см лог"; }
   } 
}