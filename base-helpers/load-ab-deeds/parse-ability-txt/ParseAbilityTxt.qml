Ability {
  name: "parse-ability-txt"

  function feel( obj ) {
    if (typeof(obj) == "string" && /abilities\.txt$/.test(obj)) return 1;
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}