Ability {
  name: "load-ability"
  robotIcon: "js-css"

  function feel( obj ) {
    if (typeof(obj) == "string" && /\.(js|css)$/.test(obj)) return 1;
    // добавить проверку файлов
    return 0;
  }

  // путь к файлу дейтсвия
  deedPath: Qt.resolvedUrl("TheDeed.qml")
}