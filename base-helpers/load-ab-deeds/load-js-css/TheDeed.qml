Deed {
  id: deed
  details: "LoadAbilityJsCss "+deed.input + "\n"+status
  property var status : ""

  icon: deed.ability.icon

  property var urla: deed.input

  onUrlaChanged: {
    // la_require - метод viewlang. оно у нас и цсс грузит
    la_require( urla, function() { 
      // готово
      status = "загружено"
      deed.icon = /\.js$/.test(urla) ? "js" : "css";
      wp.abilityLoaded( urla );
    } );
  }

  params: [] // хитро блин... это чтобы оно показывалось в рисовании.. час наверное потерял ---- надо какое-то явный признак ввести

  Component.onDestruction: {
    la_unrequire( urla );
  }
}