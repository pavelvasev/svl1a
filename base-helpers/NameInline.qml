Item {
  id: un

  Component.onCompleted: {
    init();
  }

  function init() {
    un.orig_get_name = wp.get_object_user_name;
    un.orig_find = wp.find_by_uniq_name;

    wp.get_object_user_name = un.get_object_user_name;
    wp.get_object_uniq_name = un.get_object_uniq_name;
    wp.find_by_uniq_name = un.find_by_uniq_name;
  }

  function get_object_user_name( some_js_or_qml_object ) {
    return get_object_uniq_name( some_js_or_qml_object );
  }

  function get_object_uniq_name( obj ) {
    if (obj && !obj.$properties && obj.toString) {
      return "inline:"+obj.toString();
    }
    return un.orig_get_name( obj );
  }

  function find_by_uniq_name( name, root ) {
    if (name && /^inline:/.test(name)) 
      return name.substring(7);
    return un.orig_find( name, root);
  }
}