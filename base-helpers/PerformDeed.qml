// Qml-объект "Выполни действие"
// Режимы работы. 
// 1. Автоматический, "выполни и забудь".
// 2. Ручной (флаг manual и вручную вызывать метод perform)
// A1. Однократный. При повторных вызовах perform ничего не делается.
// блин а вообще надо разобраться тут получше, что же понаделано
 
Item {
  id: pd

  property var name
  property var object
  property alias input: pd.object
  property alias ability: pd.name

  property var options: [{}]
  //property var initParams

  property bool manual: false // ручной режим - действие не будет выполняться автоматически

  //property var deed: perform( true ); // аналогично, не можем юзать биндинг, так как может возникнуть зависимость много от чего, в т.ч. от children кого-то
  property var outputDeed

  property var output: outputDeed ? outputDeed.output : undefined

  property bool once: false   // действие выполнить 1 раз, при дальнейших вызовах perform ничего не делать
  property bool enabled: true // действие разрешено

  property bool performed: !!outputDeed

  property bool parentToObject: false

  property bool record: false

  property bool retry: true // если выполнение действия не удалось, попытаться выполнить его снова, когда действие изменится 

  property string robotLayer

  onInputChanged: {
    if (!inited) return;
    if (!outputDeed) perform( true ); // вход изменился, а у нас действие не сделанное? -> пытаемся снова
    if (outputDeed && once) outputDeed.input = input; // вход изменился, а у нас действие однократное - меняем ему вход тогда
  }
  onEnabledChanged: if (enabled) perform( true );

  function perform(automaticCall) {
    if (automaticCall && manual) return undefined;
    if (!enabled) return undefined;

    if (once && outputDeed) {
      //console.log( "setting outputDeed.input to",pd.input);
      outputDeed.input = null; 
      outputDeed.input = pd.input;
      performedGood( outputDeed );
      return outputDeed;
    }

    var optHash = pd.options && (pd.options instanceof Array) ? pd.options[0] : pd.options;
    if (!optHash) optHash = {};
    optHash.record = pd.record;
    if (pd.robotLayer) optHash.robotLayer = pd.robotLayer;
    //if (!optHash.initParams && pd.initParams) optHash.initParams = Array.isArray( pd.initParams ) ? pd.initParams[0] : pd.initParams;

    //console.log(">>>>>>>>>>>>>>>>>>>>> optHash=",optHash,"pd=",pd);
    
    // такая вот у нас зацикленность.. deed зависит сам от себя ;-)
    var pdd = parentToObject && object ? object : pd; 

    // object.$properties есть проверка что этот объект есть qml-единица
    if (!pdd.$properties) pdd = qmlEngine.rootObject;

    outputDeed = wonderfulplace.perform_deed( name, object, optHash, pdd );
    
    if (outputDeed) {
      // нам важно 2 варианта - цеплять родителем к себе или к объекту.
      // console.log("outputdeed abilityname =",outputDeed.ability.name, "parentToObject=",parentToObject);
      //var pdd = parentToObject && object ? object : pd;
      //outputDeed.parent = pdd;
      //pdd.childrenChanged();
      // мейлкий qmlweb-овский хак - чтобы при удалении этого PerformDeed удалялись порожденные им действия
      /* вроде уже не надо, мы parent-а сразу выставляем --- нет надо!.. */
      if (pdd && pdd.$tidyupList) {
        //console.log("assigned tidy pdd=",pdd );
        pdd.$tidyupList.push( outputDeed );
      }

      if (once) { // дык всем же полезно -- полезно то полезно, но см коммент в deedDeleted
        outputDeed.Component.destruction.connect( pd, deedDeleted );
      }

      performedGood( outputDeed );
    }
    else {
      // #wodner performdeed::perform -> wp::abilityLoaded // connnect 
      // а вообще связь это объект. и открытый вопрос - что мы стрелочками обозначаем? передачу управления? а создание объектов?
      // #wonder wp::abilityLoaded -> performdeed::wpAbilityLoaded

      if (outputDeed === undefined) { // но оно еще может быть false - это значит действие не сработало..
        
        if (retry) {
          console.log("waiting ability to load.. name=",pd.name);
          wp.abilityLoaded.connect( pd, wpAbilityLoaded );
        }
        performedFail( true );
      }
      else
      if (outputDeed === false && object) { // но оно еще может быть false - это значит действие не сработало..
        
        if (retry) {
          console.log("waiting ability to change.. name=",pd.name);
          wp.abilityLoaded.connect( pd, wpAbilityLoaded );
        }

        performedFail( true );
      }
      else performedFail( false );
    }
    // вроде и не ошибка - ну подумаешь, не сработало.. else console.error("PerformDeed: perform_deed returned null, ability name=",name);
    //console.log("perform_deed name=",name,"object=",object,"result=",outputDeed);
    
    return outputDeed;
  }

  property var machine // в каком мире делать, урль

  property bool inited: false
  Component.onCompleted: {
    inited = true;
    if (!outputDeed) perform( true );
  }

  signal performedGood( object obj );
  signal performedFail(bool pending);

  //////////////////////////////
  function wpAbilityLoaded( name ) {
    if (name == pd.name) {
      // #wonder performdeed::wpAbilityLoaded -> performdeed::perform
      console.log("disconnecting ab name=",name);
      wp.abilityLoaded.disconnect( pd, wpAbilityLoaded );
      // важно сначала отконнектиться а потом делать действие, т.к. после после действия вызывают peformedGood а там могут опять вызвать действие но другое, и опять законнектиться
      if (!performed) {
        console.log("PerformDeed: ability loaded, performing..",name);
        //debugger;
        setTimeout( function() { perform(); }, 100 );
      }
    }
  }

  property bool activateDlg: false
  // #viewlang-window risovanie
  onPerformedGood: if (activateDlg && typeof(risovanie) !== "undefined") setTimeout( function() { risovanie.activate( outputDeed ) }, 250 ); // задержка такая вот..

  signal deedDeleted(); //  object deed 
  onDeedDeleted: {
    //console.log(">>>>>>>>>>>deee deleted!");
    ///if (outputDeed == deed) 
    // как блин узнать то кто вызвал?...
    outputDeed = null;
  }
}