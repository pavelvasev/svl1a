Deed {
  id: deed

  Text {
    id: t1
    text: wp.get_object_user_name( deed.input )
  }
  Text {
    id: t2
    text: "Содержание робота выведено в консоль разработчика. Нажмите Ctrl+Shift+J для ее просмотра."
  }
  params: [t1,t2]
  
  onInputChanged: {
    console.log("************ print-to-console-log");
    console.log(deed.input);
    console.log("************");
  }

}