// Создает робота 1 штуку с указаным именем `source`

Deed {
  id: deed
  //icon: q.item ? q.item.icon : ""
  //icon: ""

  property var source
  // имя робота или урля к qml файлу

  property var extraChildren: q.item ? [q.item] : []

  params: [p,examples,pln]
  TextParam {
    id: p
    visible: false
    text: "Тип объекта или адрес файла qml"
    guid: "type"
    value: source
    onValueChanged: {
      //console.log(">>>>>>>>>>>>>>>>>>>>>>> p.value =",p.value);
      if (deed.source != p.value) deed.source = p.value;
    }
  }
  onSourceChanged: if (deed.source != p.value) p.value = deed.source;
  
  Loader {
    id: q
    source: deed.source
    
    onItemChanged: {
      //console.log("item changed=",item);
      if (!item) return;
      var cla = source;
      cla = cla.split("/").pop();
      if (cla.indexOf(".qml") > 0) cla = cla.split(".qml")[0];
      if (cla) item.$class = cla;
      //item.parent = deed;
      // а это надо для рисоваек.. а то создаст, а его не видать
      // если у итемы есть пропертя visual, то пусть она сама ее настраивает тогда..
      if (!item.$properties || !item.$properties["visual"]) item.visual = true;

      item.logicalName = pln.value;

      // Так, дальше. Хак. Надо дернуть за тяпку, а то detector-ы не срабатывают
      // см qmlweb function QMLItem(meta) 
      var event = new CustomEvent('componentOnCompleted', { 'detail': item });
      window.dispatchEvent(event);
    }
  }

  //////////////////////////
  property var names: ["Robot","FileRobot","Array2dRobot","Spheres","Points","Lines","Triangles","PointLight", "TextRobot", "ImageRobot" ]

    Column {
      id: examples
      ExclusiveGroup {
        id: ex
        onCurrentChanged: p.value = ex.current.text; 
      }
      Repeater {
        model: names.length
        RadioButton {
          text: names[index]
          exclusiveGroup: ex
        }
      }
    }

  TextParam {
    id: pln
    guid: "logicalName"
    text: "Логическое имя (необязательно)"

    onValueChanged: q.item.logicalName = pln.value;
  }
}