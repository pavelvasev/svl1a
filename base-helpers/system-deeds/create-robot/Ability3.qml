Ability {
  name: "create-robot"
  title: "Добавить объект"
  
  function feel( target_object_x ) {
    if (target_object_x && target_object_x.$class == "Scene") return 1;
    if (!target_object_x) return 0.9;
    if (target_object_x.iAmRobotGenerator) return 1;    
    return 0.7;
  }

  deedPath: Qt.resolvedUrl("Deed3.qml")
}