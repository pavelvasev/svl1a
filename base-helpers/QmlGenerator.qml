// Генератор объектов. Почти как Loader, только делает не 1 объект а много (сколько надо).
// см метод generate
// см http://doc.qt.io/qt-5/qml-qtquick-loader.html

Item {
    id: loader

    property string source
    property string sourceCode
    property bool   active: true

    signal loaded( object object );

    function respondToSourceChange() {
        //console.log("loader creates component for source=",source);
        // __executionContext
        var context = loader.$properties["source"].componentScope;

        sourceComponent = Qt.createComponent( source.indexOf(".") > 0 ? source : source + ".qml",context );  // e.g. if source contains 'dot', load as is; if not - add .qml to it.

        if (!sourceComponent) {
            debugger;
            console.error("QmlGenerator.qml: failed to load component from source=",source );
        }
    }

    // with timeoutMode = true, Loader performs loading after small timeout
    // with timeoutMode = false, Loader loads (e.g. creates component) immediately
    property bool timeoutMode: false
    
    onSourceChanged: {
        if (loader.timeoutId && timeoutMode) {
            clearTimeout(loader.timeoutId);
            loader.timeoutId=null;
            //console.log("%%%%%%%%%%%%%%%%%%%%%%%% timeout cleared");
        }

        if (source) {
            if (active) {

                if (!timeoutMode)
                    respondToSourceChange();
                else
                    loader.timeoutId=setTimeout( function() {
                        respondToSourceChange();
                        loader.timeoutId=null;
                    }, 1 ); //setTimeout

            }
            else
                sourceComponent = null;
        }
        else
            sourceComponent = null;
    }

    onActiveChanged: {
        if (source)
            sourceChanged();
        else
            sourceComponentChanged();
    }

    property var sourceComponent


    function generate( init_props, newitem_parent ) {
        if (!sourceComponent || !active) {
            console.error("generator not active or no source component, exiting");
            return;
        }

/*
    if (parent && parent.$tidyupList) parent.$tidyupList.push(this);
*/        
        var it = sourceComponent.createObject( loader );

        if (!it) {
            console.error("failed to create object for component source=",source );
            return;
        }

        /*
        { // Важный фикс класса объекта, см doc/shlak/troubles.md
          var cla = source;
          cla = cla.split("/").pop();
          if (cla.indexOf(".qml") > 0) cla = cla.split(".qml")[0];
          if (cla) it.$class = cla;
        } 
        */

        if (!newitem_parent) newitem_parent = loader.parent;
        it.parent = newitem_parent;
        newitem_parent.childrenChanged();

        for (i in init_props) {
          var v = init_props[i];
          // console.log("QmlGenerator assigning property ",i,v);
          it[i] = v; // т.е. значение а не биндинг
        }

        if (engine.operationState !== QMLOperationState.Init && engine.operationState !== QMLOperationState.Idle) {
            engine.$initializePropertyBindings();
            engine.callCompletedSignals();
        }
        else {
        }

        //console.log("generator loaded it=",it);
        loaded( it );

        return it;
    }
    
}
