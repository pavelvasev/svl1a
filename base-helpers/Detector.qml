// При создании объектов в системе набирает их в массив items
// при выполнении условия test

Registrator {
  function test( obj ) {
    return false;
  }

  property var items: []
  property var count: 0

  signal itemAdded( object obj );
  signal itemRemoved( object obj );

  /// борьба с дубликатами
  property var itemsHash: {return {}}

  onCreated: {
    if (test( obj )) {
      if (itemsHash[obj._uniqueId]) {
        return;
      }
      itemsHash[obj._uniqueId] = 1;

      obj.Component.destruction.connect(obj, function() {
        delete itemsHash[obj._uniqueId];
        var index = items.indexOf( obj );
        if (index >= 0) items.splice( index,1 );
        
        count = items.length;
        itemRemoved( obj );
        itemsChanged();
      } );

      items.push( obj );
      count = items.length;
      itemAdded( obj );
      itemsChanged();
    }
  }
}