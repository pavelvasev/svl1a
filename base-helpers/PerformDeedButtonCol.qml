// Кнопочка, вызывающая действие

Column {
    id: co

    property var tag: "right"    
    property var name: "waiting.."

    property alias object: rundlg.object
    property alias input: rundlg.object
    property alias options: rundlg.options
    property alias once: rundlg.once
    property alias activateDlg: rundlg.activateDlg
    property alias parentToObject: rundlg.parentToObject

    property alias text: btn.text

    signal performedGood();
    signal clicked();

    property alias outputDeed: rundlg.outputDeed
    property var record: false

    Button {
      id: btn
      width: 150
      text: name
      onClicked: { co.clicked(); rundlg.perform(); }
    }

    PerformDeed {
      id: rundlg
      name: parent.name
      object: parent.object      
      manual: true
      record: parent.record
      onPerformedGood: parent.performedGood();
    }

    Component.onCompleted: vlwindow.refineAll();
}
