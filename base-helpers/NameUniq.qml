// Такая штука, которая создает метод wp.get_object_uniq_name для присвоения объектам уникальных имен
// Полезно для 1) параметров, хранимых в хеше урлья 2) записи действий

Registrator {
  id: un

  Component.onCompleted: {
    if (!inited) init();
  }

  property bool inited: false

  function init() {
    wp.get_object_user_name = un.get_object_user_name;
    wp.get_object_uniq_name = un.get_object_uniq_name;
    wp.find_by_uniq_name = un.find_by_uniq_name;
    inited = true;
  }

  // сигнал от registrator-а
  onCreated: {
    if (!inited) init();
    wp.get_object_uniq_name( obj ); // фиксируем имена. главным образом потому, что нам надо задать номер, который потом фигурирует в имени
  }

  function get_object_user_name( some_js_or_qml_object ) {
    return get_object_uniq_name( some_js_or_qml_object );
  }

  function get_object_uniq_name( obj ) {
    if (!obj) return "null";

    if (obj.$userForcedName) return obj.$userForcedName;
    //if (obj.$wpAssignedUniqName) return obj.$wpAssignedUniqName; 
    // пробуем без пересчета, т.к. вроде сейчас успешно выславляется parent какой надо..
    // пока еще нельзя. ибо например тип объекта меняется.. а он участвует в вычислении..
    // в общем еще думать будем

    // нам нужен постоянный пересчет т.к. scope может поменяться
    var p = find_nearest_parent_with_scope_name( obj );

    var tt = wp.get_object_wonderful_type( obj );
    if (tt == "Deed") tt = obj.ability.name;

    if (obj.$wpCountedInScope !== p) {
      if (!p.scopeCounter) p.scopeCounter = {};
      var k = tt;
      p.scopeCounter[ k ] = (p.scopeCounter[ k ] || 0) +1;

      obj.$wpCountedInScope = p;
      obj.$wpCount = p.scopeCounter[ k ];
    }

    var sn = p.scopeName || "";
    var cnt = (obj.$wpCount > 1) ? "#" + obj.$wpCount : "";
    //var cnt =  "#" + obj.$wpCount;
    var t = sn + "/" + tt + cnt;
    obj.$wpAssignedUniqName = t;
    //console.log(t);

    return t;
  }

  function find_nearest_parent_with_scope_name( obj ) {
    var p = obj.parent;
    while (p) {
      if (p.scopeName) return p;
      p = p.parent;
    }
    return qmlEngine.rootObject;
  }

  function find_by_uniq_name( name, root ) {
    //if (!root) { console.log( "find_by_uniq_name",name ); console.trace(); }
    if (!root) root = qmlEngine.rootObject;

    if (get_object_uniq_name(root) == name) return root;

    var cc = root.children;
    for (var i=0; i<cc.length; i++) {
      var q = find_by_uniq_name( name, cc[i] );
      if (q) return q;
    }
      
    return null;
  }
}